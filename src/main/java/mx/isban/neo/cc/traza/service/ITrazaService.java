package mx.isban.neo.cc.traza.service;

import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.MessageResp;
import mx.isban.neo.cc.model.response.TrazaResp;

public interface ITrazaService {
	
	
	TrazaResp[] coredmlTraza(DmlBean bean);
	
	MessageResp[] coredmlMotivoTdc(DmlBean bean);

	

}