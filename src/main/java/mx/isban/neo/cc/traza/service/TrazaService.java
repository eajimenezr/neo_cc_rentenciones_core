package mx.isban.neo.cc.traza.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.isban.neo.cc.folio.service.IBuilderDMLService;
import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.MessageResp;
import mx.isban.neo.cc.model.response.TrazaResp;

@Service
public class TrazaService implements ITrazaService {

	@Autowired
	private IBuilderDMLService buildService;
	
	
	@Override
	public TrazaResp[] coredmlTraza(DmlBean bean) {
		return buildService.buildDmlT(bean);
	}


	@Override
	public MessageResp[] coredmlMotivoTdc(DmlBean bean) {
		return buildService.buildDmlMTdc(bean);
	}

	
	
}
