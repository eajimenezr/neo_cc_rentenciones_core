/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * OAuth2SecurityConfiguration.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2020-06-12       Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc.security.cors;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

/**
 * Type class: ____ .<br>
 * Main objective: ___ .<br>
 * Contains gets / sets methods:
 * <ul>
 * <li></li>
 * </ul>
 * Global constants or variables:
 * <ul>
 * <li></li>
 * </ul>
 * @author Isban / iscontreras
 * SecurityConfiguration.java by Z055749
 *
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {


    /**
     * Overriding method:
     * @param http
     * @throws Exception  configure 
     * @see org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter#configure(org.springframework.security.config.annotation.web.builders.HttpSecurity)
     **/
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http              
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("*").permitAll()
                .and().httpBasic();
        
        http
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
}