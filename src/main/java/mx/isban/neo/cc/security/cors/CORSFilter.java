/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * WebConfiguration.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2020-06-11       Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc.security.cors;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Type class: ____ .<br>
 * Main objective: ___ .<br>
 * Contains gets / sets methods:
 * <ul>
 * <li></li>
 * </ul>
 * Global constants or variables:
 * <ul>
 * <li></li>
 * </ul>
 * @author Isban / iscontreras
 * WebConfiguration.java by Z055749
 *
 */
@Component
public class CORSFilter  implements Filter {
	/**
	 * Logger LOG : 
	 **/
	private static final Logger LOG = LoggerFactory.getLogger(CORSFilter.class);
	/**
	 * String maxAge : 
	 **/
	@Value("${access.control.max-age}")
	private String maxAge;
	/**
	 * String allowMethos : 
	 **/
	@Value("${access.control.allow.methods}")
	private String allowMethos;
	/**
	 * String allowHeaders : 
	 **/
	@Value("${access.control.allow.headers}")
	private String  allowHeaders;
	/**
	 * String allowCredentials : 
	 **/
	@Value("${access.control.allow.credentials}")
	private String allowCredentials;
	/**
	 * String allowOrigin : 
	 **/
	@Value("${access.control.allow.origin}")
	private String allowOrigin;
	
    /**
     * Overriding method:
     * @param filterConfig
     * @throws ServletException  init 
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     **/
	@Scheduled
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }
    /**
     * Overriding method:
     * @param servletRequest
     * @param servletResponse
     * @param filterChain
     * @throws IOException
     * @throws ServletException  doFilter 
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
     **/
    @Scheduled
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpServletRequest request= (HttpServletRequest) servletRequest;

        response.setHeader("Access-Control-Allow-Origin", allowOrigin);
        response.setHeader("Access-Control-Allow-Methods", allowMethos);
        response.setHeader("Access-Control-Allow-Headers", allowHeaders);
        response.setHeader("Access-Control-Allow-Credentials", allowCredentials);
        response.setHeader("Access-Control-Max-Age", maxAge);
        
        if ("OPTIONS".equalsIgnoreCase(request.getMethod())) {
        	 LOG.info("doFilter.Request.Option");
			response.setStatus(HttpServletResponse.SC_OK);
			response.setHeader("Access-Control-Max-Age", "3600");
		}else {
			filterChain.doFilter(servletRequest, servletResponse);
		}

        LOG.info("doFilter");

    }

    /**
     * Overriding method:  destroy 
     * @see javax.servlet.Filter#destroy()
     **/
    @Scheduled
    @Override
    public void destroy() {

    }
}
