/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * TenenciasApplication.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2018-09-25                               Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * Clase principal que ejecuta el micro servicio de tenencias.
 * Inicia componentes:
 * <Strong>EnableSwagger2</Strong>
 * <Strong>ComponentSca</Strong>
 * <Strong>EnableAutoConfiguration</Strong>
 * <Strong>SpringBootApplication</Strong>
 * @author Isban / Z046293
 * Class InteraccionesApplication.java 
 */
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
@EnableSwagger2
@ComponentScan(basePackages = { "mx.isban.neo" })
public class FolioApplication {


	/**
	 * Methos main: return type void 
	 * @param args
	 * 	 argumantos opcionales 
	 * 				para inicio de ejecucion
	 */
	public static void main(String[] args) {
		// ejecuta spring boot
		SpringApplication.run(FolioApplication.class, args);
	}

}
