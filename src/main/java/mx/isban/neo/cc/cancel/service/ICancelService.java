package mx.isban.neo.cc.cancel.service;

import java.sql.SQLException;
import java.util.Map;

import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.request.Mpa2Rq;
import mx.isban.neo.cc.model.response.AdicionalResp;
import mx.isban.neo.cc.model.response.CancelAdicResp;
import mx.isban.neo.cc.model.response.CancelTitularResp;
import mx.isban.neo.cc.model.response.MessageRespCancel;

import mx.isban.neo.cc.tenencias.trx.beans.lmwe.rq.LmweRq;
import mx.isban.neo.cc.tenencias.trx.beans.lmwe.rs.TrxRs;

public interface ICancelService {
	
	public MessageRespCancel[] coredmlCancel(DmlBean bean) throws SQLException;
	
	public AdicionalResp[] coredmlAdicionales(DmlBean bean) throws SQLException;
	
	public MessageRespCancel[] validaResponse(MessageRespCancel[] resp) throws SQLException;
	
	public MessageRespCancel[] eliminarObj(MessageRespCancel[] resp) throws SQLException;
	
	public CancelTitularResp cancelaTdcTitular(String titular, String code, String folio) throws SQLException;
	
	public Map<String,String> cancelaTdcAdic(String titular, String code) throws SQLException;
		
	public CancelAdicResp consultaAdicional(String folio, String numTdc) throws SQLException;
	
	/**
	 * Method coreGetCreditCardTrx
	 * Method objective: ___ .<br>
	 * @param resquest mapa de datos apra acceder a la TRX
	 * @param config configuracion requerida para validar el estatus de bloqueo, situacion de la tarjeta y codigos de bloqueo
	 * @return DatosLmweRs   lista de tarjetas que cumplen con las validaciones
	 *
	 */
	public TrxRs coreGetCreditCardTrx(Mpa2Rq resquest);

	/**
	 * Method coreExtraerTrx
	 * Method objective: ___ .<br>
	 * @param endpoint endpoint del servicio
	 * @param json json que se envia
	 * @return String rresultado consultado
	 */
	String coreExtraerTrx(String endpoint, Object json);
	
	

	/**
	 * Method extraerTrx
	 * Method objective: ___ .<br>
	 * @param endpoint endpoint del servicio
	 * @param json json que se envia
	 * @return String rresultado consultado
	 */
	String extraerTrx(String endpoint, Object json);
	
	

}