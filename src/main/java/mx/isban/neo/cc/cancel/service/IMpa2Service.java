package mx.isban.neo.cc.cancel.service;


import mx.isban.neo.cc.model.request.Mpa2Rq;
import mx.isban.neo.cc.tenencias.trx.beans.lmwe.rs.TrxRs;


public interface IMpa2Service {

	/**
	 * Method consumirTrxLmwe
	 * Method objective: ___ .<br>
	 * @param lmweRq mapa de datos apra acceder a la TRX
	 * @param config configuracion requerida para validar el estatus de bloqueo, situacion de la tarjeta y codigos de bloqueo
	 * @return DatosLmweRs   lista de tarjetas que cumplen con las validaciones
	 */
	TrxRs consumirTrxLmwe(Mpa2Rq lmweRq);


}
