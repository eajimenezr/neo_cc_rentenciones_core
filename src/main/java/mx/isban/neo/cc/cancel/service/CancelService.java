package mx.isban.neo.cc.cancel.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import mx.isban.neo.cc.builder.dml.service.IBuildReDMLService;
import mx.isban.neo.cc.dao.IConsumeExternalWsDAO;
import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.request.Mpa2Rq;
import mx.isban.neo.cc.model.response.AdicionalCancelResp;
import mx.isban.neo.cc.model.response.AdicionalResp;
import mx.isban.neo.cc.model.response.CancelAdicResp;
import mx.isban.neo.cc.model.response.CancelTitularResp;
import mx.isban.neo.cc.model.response.MessageRespCancel;
import mx.isban.neo.cc.model.response.RespuestasCancel;
import mx.isban.neo.cc.tenencias.trx.beans.lmwe.rs.TrxRs;
import mx.isban.neo.cc.utils.ConstastCommon;
import mx.isban.neo.cc.utils.MappingUtilTnnc;
import mx.isban.neo.cc.utils.UtilMappingSqlRe;



@Service
public class CancelService implements ICancelService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CancelService.class);

	
	@Autowired	
	public IBuildReDMLService dmlService;

	@Autowired
	public IMpa2Service iTrxService;
	
	@Autowired
	public IConsumeExternalWsDAO iConsumeExternalWsDAO;
	
	@Override
	public MessageRespCancel[] coredmlCancel(DmlBean bean) throws SQLException {
		return dmlService.buildDmlCancel(bean);
	}

	@Override
	public MessageRespCancel[] validaResponse(MessageRespCancel[] resp) throws SQLException {
		ArrayList<RespuestasCancel> listRespuestas = new ArrayList<RespuestasCancel>();//creamos el objeto lista
		
		//extraemos id_respuesta y respuesta de cada iteracion
		for (int i = 0; i < resp.length; i++) {
			RespuestasCancel respuestas = new RespuestasCancel();

			respuestas.setId_respuesta(resp[i].getIDRESPUESTA());
			respuestas.setRespuesta(resp[i].getRESPUESTA());
			listRespuestas.add(respuestas);//almacenamos la respuesta en la lista

		}
		resp[0].setRespuestas(listRespuestas);
		MessageRespCancel[] respFinal = resp.clone();
		this.eliminarObj(respFinal);
		resp= respFinal;
		
		return resp;
	}

	@Override
	public MessageRespCancel[] eliminarObj(MessageRespCancel[] resp) throws SQLException {

		for (int i = 1; i < resp.length; i++) {
			resp[i]= null;
		}
		resp[0].setIDRESPUESTA(null);
		resp[0].setRESPUESTA(null);	
		return resp;
	}

	/**
	 * Method coreGetCreditCardTrx
	 * Method objective: ___ .<br>
	 * @param resquest mapa de datos apra acceder a la TRX
	 * @return TrxRs informacion recuperada de la TRX LMWE
	 */
	@Override
	public TrxRs coreGetCreditCardTrx(Mpa2Rq resquest) {
		return iTrxService.consumirTrxLmwe(resquest);
	}
	
	/**
	 * Method coreExtraerTrx
	 * Method objective: ___ .<br>
	 * @param endpoint endpoint del servicio
	 * @param json json que se envia
	 * @return String rresultado consultado
	 */
	@Override
	public 	String coreExtraerTrx(String endpoint, Object json) {
		return extraerTrx(endpoint, json);
	}
	
	/**
	 * Method extraerTrx
	 * Method objective: ___ .<br>
	 * @param endpoint endpoint del servicio
	 * @param json json que se envia
	 * @return String rresultado consultado
	 */
	@Override
	public String extraerTrx(String endpoint,Object json) {
		String result = ConstastCommon.EMPTY_STRING;
		try {
			//Se manda ejecutar el proceso
		 result = iConsumeExternalWsDAO.consumeWs( endpoint, json , null, null, HttpMethod.POST);
		}catch (RestClientException e) {
			/*RestClientException  **/
			LOGGER.error("consumirTrxLmwe.RestClientException",e);
		}
		
		return result;
	}

	@Override
	public AdicionalResp[] coredmlAdicionales(DmlBean bean) throws SQLException {
		return dmlService.buildDmlAdicionales(bean);

	}

	@Override
	public CancelAdicResp consultaAdicional(String folio, String numTdc ) throws SQLException {
		String consulta= "consulta_adicionales";
		CancelAdicResp response = new CancelAdicResp(); 

		DmlBean bean = UtilMappingSqlRe.mapingQuery(consulta, folio, numTdc, "1", null, null);
		AdicionalResp[] result = this.coredmlAdicionales(bean);
			
		if(result.length >= 1 && result[0].getIDMENSAJE() == 1)  {
			
			response.setId_mensaje(result[0].getIDMENSAJE());
			response.setMensaje("se consultan adicionales con exito");
			
			ArrayList<AdicionalCancelResp> listAdicCanceladas = new ArrayList<AdicionalCancelResp>();//creamos el objeto lista

			for (int i = 0; i < result.length; i++) {
				
				Map<String, String>  respCancel =this.cancelaTdcAdic(result[i].getTDCADICIONAL(), result[i].getCODIGOBLOQUEO());
				 //manda a guardar TDC adicional con accion=2	
				 	DmlBean beanAdic = UtilMappingSqlRe.mapingQuery(consulta, folio, result[i].getTDCADICIONAL(), "2", result[i].getCODIGOBLOQUEO(), null);
					AdicionalResp[] resultAdic = this.coredmlAdicionales(beanAdic);
					LOGGER.info("resultado de guardado de adicional "+" "+ i +" "+ Arrays.toString(resultAdic)  );
			
					AdicionalCancelResp adicionales = new AdicionalCancelResp();

					if (respCancel.get("id_mensaje").equals("1")) {
						adicionales.setId_mensaje(Integer.parseInt(respCancel.get("id_mensaje")));
						adicionales.setMensaje(respCancel.get("mensaje")+ ": "+result[i].getTDCADICIONAL());
					}else {
						adicionales.setId_mensaje(Integer.parseInt(respCancel.get("id_mensaje")));
						adicionales.setMensaje(respCancel.get("mensaje"));
						adicionales.setPasoAdicional("Aplicar bloqueo manual a TDC: "+result[i].getTDCADICIONAL()+" con codigo de bloqueo: " +result[i].getCODIGOBLOQUEO());
					}
					
					listAdicCanceladas.add(adicionales);//almacenamos la respuesta en la lista
		
			}
			
			response.setAdicionalesCanceladas(listAdicCanceladas);
					
		}else {
			response.setId_mensaje(result[0].getIDMENSAJE());
			response.setMensaje(result[0].getMENSAJE());

			
		}
		
    	return response;
    	
    	
    	
	}

	@Override
	public CancelTitularResp cancelaTdcTitular(String titular, String code, String folio) throws SQLException {
		
		CancelTitularResp objTdcCancel = new CancelTitularResp();
		Mpa2Rq resquest = MappingUtilTnnc.getSingletonInstance().mappingMpa2Rq(titular,code);
		TrxRs  trxRs = this.coreGetCreditCardTrx(resquest);
		String trama = trxRs.getResultado().getTramaRespuesta();
		
		if(trxRs != null && trxRs.getResultado().getMensajeError() == null) {
			objTdcCancel.setId_mensaje(1);
			if(code.equals("5")) {
				objTdcCancel.setMensaje(trxRs.getEstatus().getMensaje()+" al cancelar TDC Adicional");	
			}else {
				objTdcCancel.setMensaje(trxRs.getEstatus().getMensaje()+" al cancelar TDC Titular");
			}
			
		}else {
			objTdcCancel.setId_mensaje(0);
			objTdcCancel.setMensaje(trama);
			objTdcCancel.setPasoAdicional("Aplicar bloqueo manual a TDC: "+ titular+ "con codigo de bloqueo: "+ code );
		}

		//se manda a guardar a tdc titular
		DmlBean bean = UtilMappingSqlRe.mapingQuery("consulta_adicionales", folio, titular, "3", code, null);
		AdicionalResp[] result = this.coredmlAdicionales(bean);
		LOGGER.error("se guarda tdc titular "+ Arrays.toString(result));
		
		
		return objTdcCancel;
	}

	@Override
	public Map<String, String> cancelaTdcAdic(String titular, String code) throws SQLException {
		Mpa2Rq resquest = MappingUtilTnnc.getSingletonInstance().mappingMpa2Rq(titular,code);
		TrxRs  trxRs = this.coreGetCreditCardTrx(resquest);
		Map<String, String> respCancel = new HashMap<>();

		
		if(trxRs != null && trxRs.getResultado().getMensajeError() == null) {
			respCancel.put("id_mensaje", "1");
			respCancel.put("mensaje", "TDC Adicional cancelada");
		}else if(trxRs != null && trxRs.getResultado().getMensajeError() != null){
			respCancel.put("id_mensaje", "0");
			respCancel.put("mensaje", trxRs.getResultado().getTramaRespuesta());
		}
		
		return respCancel ;
	}

}
