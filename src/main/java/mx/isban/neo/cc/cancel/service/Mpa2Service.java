package mx.isban.neo.cc.cancel.service;



import org.apache.commons.lang3.StringUtils;
import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import mx.isban.neo.cc.model.request.Mpa2Rq;
import mx.isban.neo.cc.tenencias.trx.beans.lmwe.rs.TrxRs;
import mx.isban.neo.cc.utils.Utils;


@Service
public class Mpa2Service  implements IMpa2Service{
	/**
	 * Constante para trazar la aplicacion.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(Mpa2Service.class);
	
	/**
	 * ICoreTenenciasService iCoreTenenciasService : 
	 **/
	@Autowired
	private ICancelService iCancelService;
	/**
	 * String baseUri : 
	 **/
	@Value("${endPoint.base}")
	private String baseUri;// 
	
	/**
	 * String uriTrxGenerica : 
	 **/
	@Value("${trx.generica}")
	private String uriTrxGenerica;
	
	
	/**
	 * Method consumirTrxLmwe
	 * Method objective: ___ .<br>
	 * @param lmweRq mapa de datos que llena desde el front y que se setan los ids para su busqueda
	 * @param config configuracion requerida para validar el estatus de bloqueo, situacion de la tarjeta y codigos de bloqueo
	 * @return DatosLmweRs lista de tarjetas que cumplen con las validaciones
	 */
	@Override
	public TrxRs consumirTrxLmwe(Mpa2Rq lmweRq) {
		//Inicializamos nuestros objetos
		TrxRs trxRs = new TrxRs();
		//Seteamos los valores de la url que necesitamos
		StringBuilder endpointEjecutaTrx = new StringBuilder();
		endpointEjecutaTrx.append(baseUri);
		endpointEjecutaTrx.append(uriTrxGenerica);
		String result = iCancelService.coreExtraerTrx(endpointEjecutaTrx.toString(), lmweRq.getMpa2());
		//Validamos el resultado
			if(StringUtils.isNotBlank(result)) {
				LOG.info("#consumirTrxLmwe.result#:"+Encode.forJava(result));
				trxRs = (TrxRs) Utils.getSingletonInstance().stringToObjectMapper(result, TrxRs.class);

			}
		
		
		//Regresamos el resultado
		return trxRs;
	}

}
