package mx.isban.neo.cc.catalogos.service;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.isban.neo.cc.builder.dml.service.IBuildDMLService;
import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.CatalogoResp;

@Service
public class CatalogoService implements ICatalogoService {

	
	@Autowired
	public IBuildDMLService dmlService;
	
	
	@Override
	public CatalogoResp[] coredmlCatalogs(DmlBean bean)throws SQLException  {
		return dmlService.buildDmlCatalogos(bean);
	}

}
