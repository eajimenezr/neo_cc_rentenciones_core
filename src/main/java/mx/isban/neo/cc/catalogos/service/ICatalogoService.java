package mx.isban.neo.cc.catalogos.service;

import java.sql.SQLException;

import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.CatalogoResp;

public interface ICatalogoService {
	
	CatalogoResp[] coredmlCatalogs(DmlBean bean )throws SQLException ;

}