package mx.isban.neo.cc.core.controller;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import io.swagger.annotations.ApiOperation;
import mx.isban.neo.cc.model.beans.DmlBean;


import mx.isban.neo.cc.model.response.MessageResp;
import mx.isban.neo.cc.model.rs.RespBean;
import mx.isban.neo.cc.scripts.service.IScriptService;
import mx.isban.neo.cc.utils.ConstastCommon;
import mx.isban.neo.cc.utils.Headers;
import mx.isban.neo.cc.utils.UtilMapping;
import mx.isban.neo.cc.utils.Utils;

/**
 * Clase tipo controller para la gestion de los Scripts en el proceso RTDC
 * Class ScriptController.java
 * @author Z266957 
 */
@RestController
@RequestMapping("/scripts")
public class ScriptController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TrazaController.class);

	
	/**
	 *  inyeccion para el uso del servicio
	 */
	@Autowired
	private  IScriptService coreScriptservice; 
	
	
    /**
     * consulta de scprits en proceso RTDC
     * @param folio numero de folio RTDC
     * @param cve_ejecutivo clave del ejecutido
     * @param name_cliente  nombre del cliente
     * @param num_tdc numero de PAN 
     * @param vigencia_tdc vigencia de la TDC
     * @param script identificador de script
     * @param dataUser hedaer santander
     * @return
     */
    @ApiOperation(value = "consulta scripts en el proceso RTDC")
	@RequestMapping(value = "/consulta", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> getScript(@RequestParam("folio") String folio,
    										  @RequestParam("cve_ejecutivo") String cve_ejecutivo,
    										  @RequestParam("name_cliente") String name_cliente,
    										  @RequestParam("num_tdc") String num_tdc,
    										  @RequestParam("vigencia_tdc") String vigencia_tdc,
    										  @RequestParam("script") String script,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA consulta de Script");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean response = null;
		MessageResp[] result=null;
		String consulta= "consultaScript";
		
    		try {
    			DmlBean bean = UtilMapping.mapingQueryScripts(consulta, folio, cve_ejecutivo,name_cliente,num_tdc,vigencia_tdc,script);
    	    	result = coreScriptservice.coredmlScript(bean);	
    	    	
    	    	if(result != null && result[0].getIDMENSAJE() > 0) {
    	    	httpStatus = HttpStatus.OK;
				response = new RespBean(); 
				response.setResultado(result);
				//setamos el estatus
				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_MSG+"  de scripts para proceso RTDC.", httpStatus));
    	    	}else  {
    	    		httpStatus = HttpStatus.BAD_REQUEST;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en la consulta de scripts para proceso RTDC.", httpStatus));
    	    	} 
    		
    		
    		}catch(HttpClientErrorException ex) {
    				LOGGER.error("Httpexception",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA. consulta de Script");
			return new ResponseEntity<RespBean>(response, httpStatus);
			
	}

}
