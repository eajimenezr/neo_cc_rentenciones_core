package mx.isban.neo.cc.core.controller;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import io.swagger.annotations.ApiOperation;

import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.request.MotivoReq;


import mx.isban.neo.cc.model.response.MotivosCancelResp;
import mx.isban.neo.cc.model.rs.RespBean;
import mx.isban.neo.cc.motivos.cancel.service.IMotivosCancelService;
import mx.isban.neo.cc.nivel.autenticacion.beans.FilterRq;
import mx.isban.neo.cc.utils.ConstastCommon;
import mx.isban.neo.cc.utils.Headers;
import mx.isban.neo.cc.utils.UtilMappingSql;
import mx.isban.neo.cc.utils.Utils;

/**
 * Clase tipo controller para la gestion de los motivos de cancelacion en el proceso RTDC
 * Class MotivosCanController.java
 * @author Z266957 
 */
@RestController
@RequestMapping("/cancel")
public class MotivosCanController {
	 
	/**
	 * instancia para el uso de logger
	 */
		private static final Logger LOGGER = LoggerFactory.getLogger(MotivosCanController.class);

		/**
		 * inyeccion para usar el servicio
		 */
		@Autowired
		private IMotivosCancelService motivosService; 
		    
		/**
		 * consulta el catalogo de motivos de cancelacion
	     * @param dataUser objeto para recibir el request
	     * @param filterRq header santander
	     * @return regresa el catalog de los motivos de cancelacion
	     */
	    @ApiOperation(value = "consulta catalogo de motivos de cancelacion")
		@RequestMapping(value = "/catalogo", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON})
	    public ResponseEntity<RespBean> getCatalogo(@RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser,
											   @RequestBody(required=false) FilterRq filterRq)  {
	    	MotivosCancelResp[] result=null;
	    	LOGGER.info("INICIA consulta de Motivos de Cancelacion");
			String consulta= "consulta_cat";
			HttpStatus httpStatus = null;
			RespBean response = null;
			
			
	    		try {
	    			DmlBean bean = UtilMappingSql.mapingQuery(consulta, null, null, null);
	    	    	result = motivosService.getCatalog(bean);	    
	    	    	
	    	    	if(result != null && result[0].getIDMENSAJE() > 0) {

		    	    	httpStatus = HttpStatus.OK;
						response = new RespBean(); 
						response.setResultado(result);
						//setamos el estatus
						response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_MSG+" del catalogo de motivos de cancelacion.", httpStatus));
	    	    	}else  {
	    	    		httpStatus = HttpStatus.BAD_REQUEST;
	    				response = new RespBean(); 
	    				response.setResultado(result);
	    				//setamos el estatus
	    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en la consulta del catalogo de motivos de cancelacion.", httpStatus));
	    	    	}
	    		} catch (HttpClientErrorException | IllegalArgumentException e) {				
	    			LOGGER.error("Ocurrio un error "+ e);
	    		}
	    			
				
	    	LOGGER.info("Finaliza  consulta de Motivos de Cancelacion");

			return new ResponseEntity<RespBean>(response, httpStatus);
	    }
	    
	    /**
	     * Guarda un nuevo motivo de cancelacion
	     * @param req objeto que recibe el requet
	     * @param dataUser header santander
	     * @return status de la transaccion
	     */
	    @RequestMapping(value = "/newMotivo", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON},consumes = {MediaType.APPLICATION_JSON})
		public ResponseEntity<RespBean> guardaNew(@RequestBody MotivoReq req,
											@RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser) {
			
	    	MotivosCancelResp[] result=null;
	    	LOGGER.info("INICIA add Motivos de Cancelacion");
			String consulta= "new_motivo";
			HttpStatus httpStatus = null;
			RespBean response = null;

	    		try {
	    			
	    			DmlBean bean = UtilMappingSql.mapingQuery(consulta, req.getStatus(), req.getPrioridad(), req.getDesc());
	    	    	result = motivosService.newMotivo(bean);
	    	    	if(result != null && result[0].getIDMENSAJE() > 0) {

		    	    	httpStatus = HttpStatus.OK;
						response = new RespBean(); 
						response.setResultado(result);
						//setamos el estatus
						response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_RGS+" del motivo de cancelacion.", httpStatus));
	    	    	}else  {
	    	    		httpStatus = HttpStatus.BAD_REQUEST;
	    				response = new RespBean(); 
	    				response.setResultado(result);
	    				//setamos el estatus
	    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en el registro del motivo de cancelacion.", httpStatus));
	    	    	}
	    	    	
	    		} catch (HttpClientErrorException | IllegalArgumentException e) {				
	    			LOGGER.error("Ocurrio un error "+ e);
	    		}
	    			
				
	    	LOGGER.info("Finaliza add Motivos de Cancelacion");

			return new ResponseEntity<RespBean>(response, httpStatus);
	    }
}
