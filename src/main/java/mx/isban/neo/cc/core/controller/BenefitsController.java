package mx.isban.neo.cc.core.controller;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import io.swagger.annotations.ApiOperation;
import mx.isban.neo.cc.benefits.service.IBenefitsService;
import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.request.BenefitsReq;
import mx.isban.neo.cc.model.response.BenefitsResp;
import mx.isban.neo.cc.model.rs.RespBean;
import mx.isban.neo.cc.utils.ConstastCommon;
import mx.isban.neo.cc.utils.Headers;
import mx.isban.neo.cc.utils.UtilMappingSql;
import mx.isban.neo.cc.utils.Utils;

/**
 * Clase tipo controller para la gestion de los beneficios por TDC en el proceso RTDC
  * Class BenefitsController.java
  * @author Z266957 
 */
@RestController
@RequestMapping("/benefits")
public class BenefitsController {
	
	/**
	 * inyeccion para usar el servicio
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(TrazaController.class);

	
	/**
	 * inyeccion para usar el servicio
	 */
	@Autowired
	private IBenefitsService iBenefitsService; 
	
	
	/**
     * consulta los beneficios por TDC 
     * @param id_producto identidicador a consultar
     * @param id_subproducto subproducto a consultar
     * @param dataUser header santander
     * @return regresa el catalogo de beneficios por TDC
     */
    @ApiOperation(value = "consulta catalogo de motivos de cancelacion")
	@RequestMapping(value = "/consulta", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> buscarBenefit(@RequestParam("id_producto") String id_producto,
    											@RequestParam("id_subproducto") String id_subproducto,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA consulta beneficios por TDC");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean response = null;
		BenefitsResp[] result=null;
		String consulta= "consulta_benefit";
		
		
    		try {
    			DmlBean bean = UtilMappingSql.mapingQuery(consulta, id_producto, id_subproducto, null);
    	    	result = iBenefitsService.coredmlBeneficios(bean);	
    	    	if(result != null && result[0].getIDMENSAJE() > 0) {

	    	    	httpStatus = HttpStatus.OK;
					response = new RespBean(); 
					response.setResultado(result);
					//setamos el estatus
					response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_MSG+" de la beneficios por TDC.", httpStatus));
    	    	}else {
    	    		httpStatus = HttpStatus.BAD_REQUEST;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en la consulta de beneficios por TDC.", httpStatus));
        	    	  
    	    	}
    		}catch(HttpClientErrorException ex) {
    				LOGGER.error("Ocurrio un error al consular el beneficio",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.OBTENER_LISTA_DE_BENEIFICIOS POR TDC");
			return new ResponseEntity<RespBean>(response, httpStatus);
			
	}
    
	/**
	 * Borra el beneficio
     * @param id_producto identidicador a consultar
     * @param id_subproducto subproducto a consultar
     * @param dataUser header santander 
     * @return regresa el status de la transaccion
     */
    @ApiOperation(value = "consulta catalogo de motivos de cancelacion")
	@RequestMapping(value = "/delete", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> deleteBenefit(@RequestParam("id_producto") String id_producto,
    											@RequestParam("id_subproducto") String id_subproducto,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA .Borrado de beneficios POR TDC");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean response = null;
		BenefitsResp[] result=null;
		String consulta= "delete_benefit";
		
		
    		try {
    			DmlBean bean = UtilMappingSql.mapingQuery(consulta, id_producto, id_subproducto, null);
    	    	result = iBenefitsService.coredmlBeneficios(bean);	
    	    	
    	    	if(result != null && result[0].getIDMENSAJE() > 0) {
	    	    		
	    	    	httpStatus = HttpStatus.OK;
					response = new RespBean(); 
					response.setResultado(result);
					//setamos el estatus
					response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_MSG+" del borrado de beneficio por TDC.", httpStatus));
    	    	}else {
    	    		httpStatus = HttpStatus.BAD_REQUEST;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en el borrado de beneficio por TDC.", httpStatus));
        	    	  
    	    	}
    	    	
    		}catch(HttpClientErrorException ex) {
    				LOGGER.error("Ocurrio un error al consular el beneficio",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.Borrado de beneficios POR TDC");
			return new ResponseEntity<RespBean>(response, httpStatus);
			
	}
    
    
    /**
     * Crea nueva beneficion para TDC 
     * @param req objeto que recibe los datos del request
     * @param dataUser header Santander
     * @return regresa el estatus de la transaccion
     */
    @ApiOperation(value = "crea nuevo beneficio por TDC")
	@RequestMapping(value = "/newBenefit", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON}, consumes = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> newBenefit(@RequestBody BenefitsReq req,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA.alta de nuevo Beneficio por TDC");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean response = null;
		BenefitsResp[] result=null;
		String consulta= "new_benefit";
		
    		try {
    			DmlBean bean = UtilMappingSql.mapingQueryBenef(consulta, req);
    	    	result = iBenefitsService.coredmlBeneficios(bean);	
    	    	if(result != null && result[0].getIDMENSAJE() > 0) {

	    	    	httpStatus = HttpStatus.OK;
					response = new RespBean(); 
					response.setResultado(result);
					//setamos el estatus
					response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_RGS+" del nuevo benefico por TDC.", httpStatus));
    	    	}else {
    	    		httpStatus = HttpStatus.BAD_REQUEST;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error la alta del nuevo beneficio por TDC.", httpStatus));
        	    	    
    	    	}
    	    	
    	    	
    		}catch(HttpClientErrorException ex) {
    				LOGGER.error("Ocurrio un error al crear el beneficio",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.alta de nuevo Beneficio por TDC");
			return new ResponseEntity<RespBean>(response, httpStatus);
			
	}
    
    /**
     * Actuliza beneficio de TDC
     * @param req objeto que recibe el request
     * @param dataUser header santader 
     * @return estatus de la transaccion
     */
    @ApiOperation(value = "Actualiza beneficio de TDC")
	@RequestMapping(value = "/updBenefit", method = RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON}, consumes = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> updBenefit(@RequestBody BenefitsReq req,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA. update de beneficio RTDC");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean response = null;
		BenefitsResp[] result=null;
		String consulta= "upd_benefit";
		
    		try {
    			DmlBean bean = UtilMappingSql.mapingQueryBenef(consulta, req);
    	    	result = iBenefitsService.coredmlBeneficios(bean);	
    	    	
    	    	if(result != null && result[0].getIDMENSAJE() > 0) {
	    	    	httpStatus = HttpStatus.OK;
					response = new RespBean(); 
					response.setResultado(result);
					//setamos el estatus
					response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_MSG+" de actualizacion de beneficio por TDC.", httpStatus));
    	    	}else {
    	    		httpStatus = HttpStatus.BAD_REQUEST;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en la actualizacion de beneficio por TDC.", httpStatus));
        	    	    
    	    	}
    	    	
    		}catch(HttpClientErrorException ex) {
    				LOGGER.error("Ocurrio un error al actualizar el beneficio",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.UPDATE BENEFITS");
			return new ResponseEntity<RespBean>(response, httpStatus);
			
	}
    
	


}
