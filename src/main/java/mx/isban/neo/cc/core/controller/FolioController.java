package mx.isban.neo.cc.core.controller;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import io.swagger.annotations.ApiOperation;
import mx.isban.neo.cc.folio.service.IFunctionsService;
import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.CodesResponse;
import mx.isban.neo.cc.model.response.FolioResponse;
import mx.isban.neo.cc.model.response.TrazaResp;
import mx.isban.neo.cc.model.rs.RespBean;
import mx.isban.neo.cc.nivel.autenticacion.beans.FilterRq;
import mx.isban.neo.cc.utils.ConstastCommon;
import mx.isban.neo.cc.utils.Headers;
import mx.isban.neo.cc.utils.UtilMappingSql;
import mx.isban.neo.cc.utils.Utils;

/**
 * Clase tipo controller para la generacion del Folio en el proceso RTDC
 * Class FolioController.java
 * @author Z266957 
 */
@RestController
@RequestMapping("/rtdc")
public class FolioController {
	 
	
	/**
	 * inyeccion para usar el servicio
	 */
		private static final Logger LOGGER = LoggerFactory.getLogger(FolioController.class);

		
		/**
		 * inyeccion para usar el servicio
		 */
		@Autowired
		private IFunctionsService iService; 
		
    
		/**
	     * Crea Folio para proceso RTDC
	     * @param buc numero de cliente
	     * @param cve_ejecutivo clave del ejecutivo
	     * @param name_cliente nombre del cliente
	     * @param dataUser header santander 
	     * @param filterRq header santander
	     * @return el numero de folio y la cantidad de argumentos que tendra disponibles
	     */
	    @ApiOperation(value = "Crea un FolioService", tags = { "Controlador FolioServices" })
		@RequestMapping(value = "/newFolios", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON})
	    public ResponseEntity<RespBean> create(@RequestParam("buc") String buc,
	    									   @RequestParam("cve_ejecutivo") String cveEjecutivo,
	    									   @RequestParam("name_cliente") String nameCliente,
	    		@RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser,
		@RequestBody(required=false) FilterRq filterRq)  {
	    	
	    	FolioResponse[] result=null;
	    	LOGGER.info("INICIA creacion de folio");
				/**Inicializamos nuestros objetos**/
				HttpStatus httpStatus = null;
				RespBean response = null;
				String consulta= "new_folio";

			try {
    			DmlBean bean = UtilMappingSql.mapingQuery(consulta, buc, cveEjecutivo, nameCliente);
    			result =iService.newFolio(bean);	
    	    	
    	    	if(result != null && result[0].getIDMENSAJE() > 0) {
	    	    	httpStatus = HttpStatus.OK;
					response = new RespBean(); 
					response.setResultado(result);
					//setamos el estatus
					response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_MSG+" del nuevo folio para proceso RTDC.", httpStatus));
    	    	}else {
    	    		httpStatus = HttpStatus.BAD_REQUEST;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en la generacion del nuevo Folio.", httpStatus));
        	    	    	 
    	    	}
    		}catch(HttpClientErrorException ex) {
    				LOGGER.error("creacion de folio, exception: ",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.creacion de folio");
			return new ResponseEntity<RespBean>(response, httpStatus);
				
	    }
	    
	    /**
	     * Consulta los codigos de bloqueo por TDC 
	     * @param codes numero de condigos de bloqueo
	     * @param dataUser header santander para el cliente
	     * @param filterRq header santander
	     * @return
	     */
	    @RequestMapping(value = "/codes", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON})
		public ResponseEntity<RespBean> codes(@RequestParam("codes") String codes,
				@RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser,
				@RequestBody(required=false) FilterRq filterRq) {
			
				CodesResponse[] result = null; 
				LOGGER.info("INICIA creacion de folio");
				/**Inicializamos nuestros objetos**/
				HttpStatus httpStatus = null;
				RespBean response = null;
				
			try {
			/**Se valida path variable de entrada**/
				result = iService.getLockCodes(codes);
				if(result != null && result[0].getIDMENSAJE() > 0) {
					httpStatus = HttpStatus.OK;
					response = new RespBean(); 
					response.setResultado(result);
					//setamos el estatus
					response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_MSG+" de codigos de bloqueo.", httpStatus));
    	    	}else {
    	    		httpStatus = HttpStatus.BAD_REQUEST;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en la consulta de codigos de bloqueo.", httpStatus));
    	    	}
				
			}catch(HttpClientErrorException ex) {
				LOGGER.error("busquedaTenencia.ValidationParamsException",ex);
			}
			
			// devuelve response
		    LOGGER.info("TERMINA obtencion de codigos de bloqueo");
			return new ResponseEntity<RespBean>(response, httpStatus);

		}
	    
}
