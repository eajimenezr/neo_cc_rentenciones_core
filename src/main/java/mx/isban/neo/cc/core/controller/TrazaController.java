package mx.isban.neo.cc.core.controller;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import io.swagger.annotations.ApiOperation;
import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.request.GuardaTdcReq;
import mx.isban.neo.cc.model.request.MotivosTdcReq;
import mx.isban.neo.cc.model.request.TrazaReq;
import mx.isban.neo.cc.model.response.MessageResp;
import mx.isban.neo.cc.model.response.TrazaResp;
import mx.isban.neo.cc.model.rs.RespBean;
import mx.isban.neo.cc.scripts.service.IScriptService;
import mx.isban.neo.cc.traza.service.ITrazaService;
import mx.isban.neo.cc.utils.ConstastCommon;
import mx.isban.neo.cc.utils.Headers;
import mx.isban.neo.cc.utils.UtilMappingSql;
import mx.isban.neo.cc.utils.UtilMapping;
import mx.isban.neo.cc.utils.Utils;

@RestController
@RequestMapping("/trazas")
public class TrazaController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TrazaController.class);

	/**
	 *  inyeccion para el uso del servicio
	 */
	@Autowired
	private ITrazaService iTrazaService; 
	
	/**
	 *  inyeccion para el uso del servicio
	 */
	@Autowired
	private  IScriptService coreScriptservice; 

	/**
	 *  inyeccion para el uso del servicio
	 */
	@Autowired
	private HttpServletRequest servletRequest; 
	
	/**
     * consulta la traza del proceso de RTDC
     * @param folio numero de folio de RTDC
     * @param fecha_tr fecha de la traza
     * @param dataUser header santander
     * @return regresa la traza a consultar
     */
    @ApiOperation(value = "consulta la traza del proceso de RTDC")
	@RequestMapping(value = "/consulta", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> buscarTraza(@RequestParam("folio") String folio,
    											@RequestParam("fecha_tr") String fecha_tr,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA.obtenerTraza proceso RTDC");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean response = null;
		TrazaResp[] result=null;
		String consulta= "consulta_traza";
		
    		try {
    			DmlBean bean = UtilMappingSql.mapingQuery(consulta, folio, fecha_tr, null);
    	    	result = iTrazaService.coredmlTraza(bean);	
    	    	
    	    	if(result != null && result[0].getIDMENSAJE() > 0) {
    	    	httpStatus = HttpStatus.OK;
				response = new RespBean(); 
				response.setResultado(result);
				//setamos el estatus
				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_MSG+" de la consulta de Traza de RTDC.", httpStatus));
    	    	}else{
    	    		httpStatus = HttpStatus.BAD_REQUEST;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en la consulta de la Traza de RTDC.", httpStatus));
    	    	}
    	    	
    		}catch(HttpClientErrorException ex) {
    				LOGGER.error("busquedaTenencia.ValidationParamsException",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.OBTENER_LISTA_DE_TARJETAS_POR_CLIENTE");
			return new ResponseEntity<RespBean>(response, httpStatus);
			
	}
    
    
    /**
     * Crea nueva traza para proceso RTDC
     * @param req objeto que recibe el request 
     * @param dataUser header santander 
     * @return regresa el status de la transaccion
     */
    @ApiOperation(value = "crea nueva traza para el procso RTDC")
	@RequestMapping(value = "/newTraza", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON}, consumes = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> newTraza(@RequestBody TrazaReq req,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA.obtenerTraza proceso RTDC");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean response = null;
		TrazaResp[] result=null;
		String consulta= "new_traza";
        HttpServletRequest request= (HttpServletRequest) servletRequest;
        		
    		try {
    			req.setIp_cliente(request.getRemoteAddr());
    			DmlBean bean = UtilMappingSql.mapingQueryTraza(consulta, req);
    	    	result = iTrazaService.coredmlTraza(bean);	
    	    	
    	    	if(result != null && result[0].getIDMENSAJE() > 0) {
	    	    	httpStatus = HttpStatus.OK;
					response = new RespBean(); 
					response.setResultado(result);
					//setamos el estatus
					response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_RGS+" de la de Traza de RTDC.", httpStatus));
    	    	}else{
    	    		httpStatus = HttpStatus.BAD_REQUEST;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en el registro de la nueva Traza.", httpStatus));
    	    	}
    	    	
    		}catch(HttpClientErrorException ex) {
    				LOGGER.error("busquedaTenencia.ValidationParamsException",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.OBTENER_LISTA_DE_TARJETAS_POR_CLIENTE");
			return new ResponseEntity<RespBean>(response, httpStatus);
			
	}
    
    /**
     * Consulta traza de proceso RTDC
     * @param req objeto que recibe el request
     * @param dataUser header santander
     * @return regresa el estatus de la transaccion
     */
    @ApiOperation(value = "consulta catalogo de motivos de cancelacion")
	@RequestMapping(value = "/updTraza", method = RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON}, consumes = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> updTraza(@RequestBody TrazaReq req,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA.obtenerTraza proceso RTDC");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean response = null;
		TrazaResp[] result=null;
		String consulta= "upd_traza";
        HttpServletRequest request= (HttpServletRequest) servletRequest;

		
    		try {
    			req.setIp_cliente(request.getRemoteAddr());
    			DmlBean bean = UtilMappingSql.mapingQueryTraza(consulta, req);
    	    	result = iTrazaService.coredmlTraza(bean);	
    	    	
    	    	if(result != null && result[0].getIDMENSAJE() > 0) {
	    	    	httpStatus = HttpStatus.OK;
					response = new RespBean(); 
					response.setResultado(result);
					//setamos el estatus
					response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_RGS+" de la actualizacion de Traza de RTDC.", httpStatus));
    	    	}else{
    	    		httpStatus = HttpStatus.BAD_REQUEST;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en la actualizacion de la Traza.", httpStatus));
    	    	}		
    		}catch(HttpClientErrorException ex) {
    				LOGGER.error("busquedaTenencia.ValidationParamsException",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.OBTENER_LISTA_DE_TARJETAS_POR_CLIENTE");
			return new ResponseEntity<RespBean>(response, httpStatus);
			
	}
    
    
    
    /**
     * asocia los motivos de cancelacion a una TDC 
     * @param req objeto que recibe el request
     * @param dataUser header santander 
     * @return
     */
    @ApiOperation(value = "asocia los motivos de cancelacion a una TDC")
	@RequestMapping(value = "/asociaMotivos", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON}, consumes = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> motivoToTdc(@RequestBody MotivosTdcReq req,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA asociar motivos a TDC");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean response = null;
		MessageResp[] result=null;
		String consulta= "motivoTotdc";
		
    		try {
    			DmlBean bean = UtilMapping.mapingQueryMotivoTdc(consulta, req);
    	    	result = iTrazaService.coredmlMotivoTdc(bean);	
    	    	
    	    	if(result != null && result[0].getIDMENSAJE() > 0) {
	    	    	httpStatus = HttpStatus.OK;
					response = new RespBean(); 
					response.setResultado(result);
					//setamos el estatus
					response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_RGS+" para asociar motivos a una TDC.", httpStatus));
    	    	}else{
    	    		httpStatus = HttpStatus.BAD_REQUEST;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en el registro para asociar motivos a una TDC.", httpStatus));
    	    	}		
    		}catch(HttpClientErrorException ex) {
    				LOGGER.error("asociar.ValidationParamsException",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.asociar motivos a TDC");
			return new ResponseEntity<RespBean>(response, httpStatus);
			
	}
	
    
    /**
     * Guarda una TDC 
     * @param req objeto que recibe el request
     * @param dataUser header santander
     * @return regresa el status de la transaccion
     */
    @ApiOperation(value = "guarda TDC")
	@RequestMapping(value = "/guardaTdc", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON}, consumes = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> guardaTdc(@RequestBody GuardaTdcReq req,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA guarda TDC");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean response = null;
		MessageResp[] result=null;
		String consulta= "guardaTdc";
		
    		try {
    			DmlBean bean = UtilMapping.mapingQuery(consulta, req);
    	    	result = iTrazaService.coredmlMotivoTdc(bean);	
    	    	
    	    	if(result != null && result[0].getIDMENSAJE() > 0) {
		
	    	    	httpStatus = HttpStatus.OK;
					response = new RespBean(); 
					response.setResultado(result);
					//setamos el estatus
					response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_RGS+" de guardar TDC.", httpStatus));
    	    	}else{
		    	    		httpStatus = HttpStatus.BAD_REQUEST;
		    				response = new RespBean(); 
		    				response.setResultado(result);
		    				//setamos el estatus
		    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en el registro de la TDC.", httpStatus));
		    	   }		
    		}catch(HttpClientErrorException ex) {
    				LOGGER.error("asociar.ValidationParamsException",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.guarda TDC");
			return new ResponseEntity<RespBean>(response, httpStatus);
			
	}
	
    /**
     * consulta los motivos de llamadas
     * @param id_tipo_motivo identidicador de motivo a consultar
     * @param dataUser header santander
     * @return regresa el motivo de llamada solicitado
     */
    @ApiOperation(value = "consulta motivos de llamada")
	@RequestMapping(value = "/consultaMotivo", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> getMotivoCall(@RequestParam("id_tipo_motivo") String id_tipo_motivo,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA.TERMINA.OBTENER_lista de motivo de llamada");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean response = null;
		MessageResp[] result=null;
		String consulta= "motivo_llamada";
		
		
    		try {
    			DmlBean bean = UtilMappingSql.mapingQuery(consulta, id_tipo_motivo, null, null);
    	    	result = coreScriptservice.coredmlMotivoCall(bean);	
    	    	
    	    	if(result != null && result[0].getIDMENSAJE() > 0) {
	    	    	httpStatus = HttpStatus.OK;
					response = new RespBean(); 
					response.setResultado(result);
					//setamos el estatus
					response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_MSG+" de la consulta de motivos de llamada.", httpStatus));
    	    	} else{
    	    		httpStatus = HttpStatus.BAD_REQUEST;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en la consulta de motivos de llamada.", httpStatus));
    	    	} 		
    		}catch(HttpClientErrorException ex) {
    				LOGGER.error("busquedaTenencia.ValidationParamsException",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.OBTENER_lista de motivo de llamada");
			return new ResponseEntity<RespBean>(response, httpStatus);
			
	}
    
    
}
