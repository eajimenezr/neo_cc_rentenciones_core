package mx.isban.neo.cc.model.request;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JsonRequestMpa implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String textoBloqueo;
	private int codigoBloqueo;
	private String pan;
	public String getTextoBloqueo() {
		return textoBloqueo;
	}
	public void setTextoBloqueo(String textoBloqueo) {
		this.textoBloqueo = textoBloqueo;
	}
	public int getCodigoBloqueo() {
		return codigoBloqueo;
	}
	public void setCodigoBloqueo(int codigoBloqueo) {
		this.codigoBloqueo = codigoBloqueo;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	
	
}
