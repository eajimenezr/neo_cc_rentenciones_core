package mx.isban.neo.cc.model.request;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TrazaReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id_traza;
	
	private String id_canal;
	
	private String id_modulo;
	
	private String id_tipo_operacion;
	
	private String conn_id;
	
	private String fec_tran;
	
	private String ip_cliente;
	
	private String cve_ejecutivo;
	
	private String cve_usuario;
	
	private String cve_usr_local;
	
	private String id_status;
	
	private String cve_folio;
	
	private String cve_buc;

	public String getId_traza() {
		return id_traza;
	}

	public void setId_traza(String id_traza) {
		this.id_traza = id_traza;
	}

	public String getId_canal() {
		return id_canal;
	}

	public void setId_canal(String id_canal) {
		this.id_canal = id_canal;
	}

	public String getId_modulo() {
		return id_modulo;
	}

	public void setId_modulo(String id_modulo) {
		this.id_modulo = id_modulo;
	}

	public String getId_tipo_operacion() {
		return id_tipo_operacion;
	}

	public void setId_tipo_operacion(String id_tipo_operacion) {
		this.id_tipo_operacion = id_tipo_operacion;
	}

	public String getConn_id() {
		return conn_id;
	}

	public void setConn_id(String conn_id) {
		this.conn_id = conn_id;
	}

	public String getFec_tran() {
		return fec_tran;
	}

	public void setFec_tran(String fec_tran) {
		this.fec_tran = fec_tran;
	}

	public String getIp_cliente() {
		return ip_cliente;
	}

	public void setIp_cliente(String ip_cliente) {
		this.ip_cliente = ip_cliente;
	}

	public String getCve_ejecutivo() {
		return cve_ejecutivo;
	}

	public void setCve_ejecutivo(String cve_ejecutivo) {
		this.cve_ejecutivo = cve_ejecutivo;
	}

	public String getCve_usuario() {
		return cve_usuario;
	}

	public void setCve_usuario(String cve_usuario) {
		this.cve_usuario = cve_usuario;
	}

	public String getCve_usr_local() {
		return cve_usr_local;
	}

	public void setCve_usr_local(String cve_usr_local) {
		this.cve_usr_local = cve_usr_local;
	}

	public String getId_status() {
		return id_status;
	}

	public void setId_status(String id_status) {
		this.id_status = id_status;
	}

	public String getCve_folio() {
		return cve_folio;
	}

	public void setCve_folio(String cve_folio) {
		this.cve_folio = cve_folio;
	}

	public String getCve_buc() {
		return cve_buc;
	}

	public void setCve_buc(String cve_buc) {
		this.cve_buc = cve_buc;
	}

	
}
