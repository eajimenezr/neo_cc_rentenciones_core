package mx.isban.neo.cc.model.request;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdArgReq implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UpdArgReq() {}
	
	private String cve_folio;
	private String num_tarjeta;
	private String ide_argumento;
	private String status_argumento;
	private String captura_adicional;
	private String folio_adicional;
	private String statusRetencion;
	private String ids_motivosCancel;

	public String getCve_folio() {
		return cve_folio;
	}
	public void setCve_folio(String cve_folio) {
		this.cve_folio = cve_folio;
	}
	public String getNum_tarjeta() {
		return num_tarjeta;
	}
	public void setNum_tarjeta(String num_tarjeta) {
		this.num_tarjeta = num_tarjeta;
	}
	public String getIde_argumento() {
		return ide_argumento;
	}
	public void setIde_argumento(String ide_argumento) {
		this.ide_argumento = ide_argumento;
	}
	public String getStatus_argumento() {
		return status_argumento;
	}
	public void setStatus_argumento(String status_argumento) {
		this.status_argumento = status_argumento;
	}
	public String getCaptura_adicional() {
		return captura_adicional;
	}
	public void setCaptura_adicional(String captura_adicional) {
		this.captura_adicional = captura_adicional;
	}
	public String getFolio_adicional() {
		return folio_adicional;
	}
	public void setFolio_adicional(String folio_adicional) {
		this.folio_adicional = folio_adicional;
	}
	public String getStatusRetencion() {
		return statusRetencion;
	}
	public void setStatusRetencion(String statusRetencion) {
		this.statusRetencion = statusRetencion;
	}
	public String getIds_motivosCancel() {
		return ids_motivosCancel;
	}
	public void setIds_motivosCancel(String ids_motivosCancel) {
		this.ids_motivosCancel = ids_motivosCancel;
	}

	
}
