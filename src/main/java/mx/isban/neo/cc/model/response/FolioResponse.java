package mx.isban.neo.cc.model.response;

import java.io.Serializable;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;



import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FolioResponse implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	//** constructor por default*/
	public FolioResponse () {
	}
	
	@JsonProperty("id_mensaje")
	private int IDMENSAJE;
	
	@JsonProperty("mensaje")
	private String MENSAJE;
	
	@JsonProperty("folio")
	private String FOLIO;

	@JsonProperty("no_argumentos")
	private String NOARGUMENTOS;

	@JsonProperty("no_argumentos_fusion_lineas")
	private int NOARGUMENTOSFUSIONDELINEAS;


	public int getIDMENSAJE() {
		// TODO Auto-generated method stub
		return 0;
	}


	

}
