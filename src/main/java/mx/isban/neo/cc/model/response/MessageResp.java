package mx.isban.neo.cc.model.response;

import java.io.Serializable;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MessageResp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MessageResp() {
	}
	
	@JsonProperty("id_mensaje")
	private int  IDMENSAJE;
	
	@JsonProperty("mensaje")
	private String MENSAJE;
	
	@JsonProperty("script")
	private String SCRIPT;
	
	
	@JsonProperty("id_motivo")
	private String IDMOTIVO;
	
	@JsonProperty("desc_motivo")
	private String DESCMOTIVO;
	
	@JsonProperty("mensaje_pop_up")
	private String MENSAJEPOPUP;

	public int getIDMENSAJE() {
		// TODO Auto-generated method stub
		return IDMENSAJE;
	}
	
	
}
