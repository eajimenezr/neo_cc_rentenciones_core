package mx.isban.neo.cc.model.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TrazaResp implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@JsonProperty("id_mensaje")
	private int IDMENSAJE;
	
	@JsonProperty("mensaje")
	private String MENSAJE;
	
	
	@JsonProperty("ide_traza")
	private	int IDE_TRAZA;

	@JsonProperty("ide_canal")
	private	int IDE_CANAL;

	@JsonProperty("ide_modulo")
	private	int IDE_MODULO;

	@JsonProperty("ide_tipo_operacion")
	private	int IDE_TIPO_OPERACION;

	@JsonProperty("conn_id")
	private	String CONN_ID;

	@JsonProperty("fec_trn")
	private	String FEC_TRN;

	@JsonProperty("txt_ip_cliente")
	private	String TXT_IP_CLIENTE;

	@JsonProperty("cve_ejecutivo")
	private	String CVE_EJECUTIVO;

	@JsonProperty("cve_usuario")
	private	String CVE_USUARIO;

	@JsonProperty("cve_usr_local")
	private	String CVE_USR_LOCAL;

	@JsonProperty("ide_status")
	private	int IDE_STATUS;

	@JsonProperty("cve_folio")
	private	String CVE_FOLIO;

	@JsonProperty("cve_buc")
	private	String CVE_BUC;

	public int getIDMENSAJE() {
		// TODO Auto-generated method stub
		return IDMENSAJE;
	}



}
