package mx.isban.neo.cc.model.response;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RespuestasCancel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	 private String id_respuesta;
     private String respuesta;
	public String getId_respuesta() {
		return id_respuesta;
	}
	public void setId_respuesta(String id_respuesta) {
		this.id_respuesta = id_respuesta;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	
     
	     
}
