package mx.isban.neo.cc.model.response;

import java.io.Serializable;




import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MessageRespCancel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MessageRespCancel() {
//        this.respuestas = new ArrayList<Respuestas>();
	}
	
	@JsonProperty("id_mensaje")
	private int IDMENSAJE;
	
	@JsonProperty("mensaje")
	private String MENSAJE;
	
	@JsonProperty("id_pantalla")
	private int IDPANTALLA;
	
	@JsonProperty("validacion")
	private String VALIDACION;

	@JsonProperty("id_pantalla_paso_adicional")
	private int IDPANTALLAPASOADICIONAL;
	
	@JsonProperty("pasos_adicionales")
	private String PASOSADICIONALES;
	
	@JsonProperty("id_pantalla_paso_adicional2")
	private int IDPANTALLAPASOADICIONAL2;
	
	@JsonProperty("pasos_adicionales2")
	private String PASOSADICIONALES2;
	
	@JsonProperty("codigo_bloqueo")
	private int CODIGOBLOQUEO;
	
	@JsonProperty("id_sig_pregunta")
	private int IDSIGUIENTEPREGUNTA;
	
	@JsonProperty("id_respuesta")
	private String IDRESPUESTA;
	
	@JsonProperty("respuesta")
	private String RESPUESTA;
	
	@JsonProperty("respuestas")
	private List<RespuestasCancel> respuestas;
	
	@JsonProperty("tdc_cancelada")
	private TdcCanceladasResp tdc_cancelada;

	public int getIDMENSAJE() {
		return IDMENSAJE;
	}

	public void setIDMENSAJE(int iDMENSAJE) {
		IDMENSAJE = iDMENSAJE;
	}

	public String getMENSAJE() {
		return MENSAJE;
	}

	public void setMENSAJE(String mENSAJE) {
		MENSAJE = mENSAJE;
	}

	public int getIDPANTALLA() {
		return IDPANTALLA;
	}

	public void setIDPANTALLA(int iDPANTALLA) {
		IDPANTALLA = iDPANTALLA;
	}

	public String getVALIDACION() {
		return VALIDACION;
	}

	public void setVALIDACION(String vALIDACION) {
		VALIDACION = vALIDACION;
	}

	public int getIDPANTALLAPASOADICIONAL() {
		return IDPANTALLAPASOADICIONAL;
	}

	public void setIDPANTALLAPASOADICIONAL(int iDPANTALLAPASOADICIONAL) {
		IDPANTALLAPASOADICIONAL = iDPANTALLAPASOADICIONAL;
	}

	public String getPASOSADICIONALES() {
		return PASOSADICIONALES;
	}

	public void setPASOSADICIONALES(String pASOSADICIONALES) {
		PASOSADICIONALES = pASOSADICIONALES;
	}

	public int getIDPANTALLAPASOADICIONAL2() {
		return IDPANTALLAPASOADICIONAL2;
	}

	public void setIDPANTALLAPASOADICIONAL2(int iDPANTALLAPASOADICIONAL2) {
		IDPANTALLAPASOADICIONAL2 = iDPANTALLAPASOADICIONAL2;
	}

	public String getPASOSADICIONALES2() {
		return PASOSADICIONALES2;
	}

	public void setPASOSADICIONALES2(String pASOSADICIONALES2) {
		PASOSADICIONALES2 = pASOSADICIONALES2;
	}

	public int getCODIGOBLOQUEO() {
		return CODIGOBLOQUEO;
	}

	public void setCODIGOBLOQUEO(int cODIGOBLOQUEO) {
		CODIGOBLOQUEO = cODIGOBLOQUEO;
	}

	public int getIDSIGUIENTEPREGUNTA() {
		return IDSIGUIENTEPREGUNTA;
	}

	public void setIDSIGUIENTEPREGUNTA(int iDSIGUIENTEPREGUNTA) {
		IDSIGUIENTEPREGUNTA = iDSIGUIENTEPREGUNTA;
	}

	public String getIDRESPUESTA() {
		return IDRESPUESTA;
	}

	public void setIDRESPUESTA(String iDRESPUESTA) {
		IDRESPUESTA = iDRESPUESTA;
	}

	public String getRESPUESTA() {
		return RESPUESTA;
	}

	public void setRESPUESTA(String rESPUESTA) {
		RESPUESTA = rESPUESTA;
	}

	public List<RespuestasCancel> getRespuestas() {
		return respuestas;
	}

	public void setRespuestas(List<RespuestasCancel> respuestas) {
		this.respuestas = respuestas;
	}

	public TdcCanceladasResp getTdc_cancelada() {
		return tdc_cancelada;
	}

	public void setTdc_cancelada(TdcCanceladasResp tdc_cancelada) {
		this.tdc_cancelada = tdc_cancelada;
	}

	
}
