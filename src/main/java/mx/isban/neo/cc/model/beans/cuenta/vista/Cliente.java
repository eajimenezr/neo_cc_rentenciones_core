/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * Cliente.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2019-03-28                               Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc.model.beans.cuenta.vista;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import mx.isban.neo.cc.utils.ConstastCommon;

/**
 * Clase de tipo dto para la informacion del campo que se buscara referente a cuenta vista.<br>
 * Contiene Get/Sets de los componentes:<br>
 * <li>numClie</li>
 * <li>excluido</li>
 * <li>listaProductos</li>
 * @author Isban / iscontreras
 * Class Cliente.java 
 */
@ApiModel
@JsonIgnoreProperties(ignoreUnknown = true )
@JsonInclude(com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL)
public class Cliente implements Serializable {

	/**
	 *  long serialVersionUID: id de transporte para las diferentes capas.
	 */
	private static final long serialVersionUID = 7331822828632550664L;

	/**
	 *  String numClie : numero de cliente o buc
	 */
	@ApiModelProperty(value="numClie")
	@Size(max = 8, message="numClie tiene un maximo de 8 caracteres")
	@NotNull(message = "numClie" + ConstastCommon.NOT_NULL_MSG)
	@NotEmpty(message = "numClie" + ConstastCommon.NOT_EMPTY_MSG)
	private String numClie;
	
	/**
	 *  String excluido : estatus del cliente si es apto o no para cuenta vista
	 */
	@ApiModelProperty(value="excluido")
	private String excluido;
	
	/**
	 *  ProductoCliente productoCliente : atributo de tipo objeto que contiene los productos contratados
	 */
	@Valid
	@ApiModelProperty(value="listaProductos")
	@NotNull(message = "listaProductos" + ConstastCommon.NOT_NULL_MSG)
	private List<ProductoCliente> listaProductos;
	
	/**
	 *  se iniciliza los datos
	 */
	public Cliente() {
		this.listaProductos = new ArrayList<>();
	}

	/**
	 * Gets the numClie
	 * @return the numClie
	 */
	public String getNumClie() {
		return numClie;
	}

	/**
	 * Sets the new numClie 
	 * @param numClie the numClie to set
	 */
	public void setNumClie(String numClie) {
		this.numClie = numClie;
	}

	/**
	 * Gets the excluido
	 * @return the excluido
	 */
	public String getExcluido() {
		return excluido;
	}

	/**
	 * Sets the new excluido 
	 * @param excluido the excluido to set
	 */
	public void setExcluido(String excluido) {
		this.excluido = excluido;
	}

	/**
	 * Gets the listaProductos
	 * @return the listaProductos
	 */
	public List<ProductoCliente> getListaProductos() {
		return new ArrayList<>(this.listaProductos);
	}

	/**
	 * Sets the new listaProductos 
	 * @param listaProductos the listaProductos to set
	 */
	public void setListaProductos(List<ProductoCliente> listaProductos) {
		if(listaProductos==null) {
			listaProductos = new ArrayList<>();
		}else {
			this.listaProductos = new ArrayList<>(listaProductos);
		}
	}
	

}
