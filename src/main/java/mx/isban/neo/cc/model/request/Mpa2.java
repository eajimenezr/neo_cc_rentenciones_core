package mx.isban.neo.cc.model.request;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Mpa2 implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	 private String idTrx;
	 private String jsonRequest;
	public String getIdTrx() {
		return idTrx;
	}
	public void setIdTrx(String idTrx) {
		this.idTrx = idTrx;
	}
	public String getJsonRequest() {
		return jsonRequest;
	}
	public void setJsonRequest(String jsonRequest) {
		this.jsonRequest = jsonRequest;
	}


	 
	
}
