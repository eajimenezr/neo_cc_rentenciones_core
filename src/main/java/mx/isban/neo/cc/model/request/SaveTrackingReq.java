package mx.isban.neo.cc.model.request;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SaveTrackingReq implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SaveTrackingReq() {}
	
	private String cve_folio;
	private String num_tarjeta;
	private String txt_comentario;
	private String ide_argumento;

	public String getCve_folio() {
		return cve_folio;
	}
	public void setCve_folio(String cve_folio) {
		this.cve_folio = cve_folio;
	}
	public String getNum_tarjeta() {
		return num_tarjeta;
	}
	public void setNum_tarjeta(String num_tarjeta) {
		this.num_tarjeta = num_tarjeta;
	}
	public String getTxt_comentario() {
		return txt_comentario;
	}
	public void setTxt_comentario(String txt_comentario) {
		this.txt_comentario = txt_comentario;
	}
	public String getIde_argumento() {
		return ide_argumento;
	}
	public void setIde_argumento(String ide_argumento) {
		this.ide_argumento = ide_argumento;
	}

	
	
}
