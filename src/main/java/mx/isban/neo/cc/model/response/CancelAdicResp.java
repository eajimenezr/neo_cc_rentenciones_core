package mx.isban.neo.cc.model.response;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CancelAdicResp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private int id_mensaje;
	
	private String mensaje;
	
	private List<AdicionalCancelResp> adicionalesCanceladas;

	public int getId_mensaje() {
		return id_mensaje;
	}

	public void setId_mensaje(int id_mensaje) {
		this.id_mensaje = id_mensaje;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public List<AdicionalCancelResp> getAdicionalesCanceladas() {
		return adicionalesCanceladas;
	}

	public void setAdicionalesCanceladas(List<AdicionalCancelResp> adicionalesCanceladas) {
		this.adicionalesCanceladas = adicionalesCanceladas;
	}


	
	
}
