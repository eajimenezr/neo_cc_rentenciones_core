package mx.isban.neo.cc.model.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TrackingArguResp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@JsonProperty("id_mensaje")
	private int IDMENSAJE;
	
	@JsonProperty("mensaje")
	private String MENSAJE;
	
	@JsonProperty("fec_alta_comentario")
	private String FEC_ALTA_COMENTARIO;
	
	@JsonProperty("ide_argumento")
	private String IDE_ARGUMENTO;
	
	@JsonProperty("nom_argumento")
	private String NOM_ARGUMENTO;
	
	@JsonProperty("herramienta")
	private String HERRAMIENA;
	
	@JsonProperty("txt_comentario")
	private String TXT_COMENTARIO;

	public int getIDMENSAJE() {
		// TODO Auto-generated method stub
		return IDMENSAJE;
	}


}
