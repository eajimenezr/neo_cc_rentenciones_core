package mx.isban.neo.cc.model.request;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Mpa2Rq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	 private Mpa2 mpa2;
	 private String numTdc;
	 
	 public Mpa2Rq() {
		 this.mpa2= new Mpa2();
	 }

	public Mpa2 getMpa2() {
		return mpa2;
	}

	public void setMpa2(Mpa2 mpa2) {
		this.mpa2 = mpa2;
	}

	public String getNumTdc() {
		return numTdc;
	}

	public void setNumTdc(String numTdc) {
		this.numTdc = numTdc;
	}
	  
	
	 	
}
