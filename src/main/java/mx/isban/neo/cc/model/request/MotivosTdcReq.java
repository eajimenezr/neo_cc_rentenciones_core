package mx.isban.neo.cc.model.request;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class MotivosTdcReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String folio_rtdc;
	
	private String num_tdc;
	
	private String ids_motivos;
		
	private String accion;

	public String getFolio_rtdc() {
		return folio_rtdc;
	}

	public void setFolio_rtdc(String folio_rtdc) {
		this.folio_rtdc = folio_rtdc;
	}

	public String getNum_tdc() {
		return num_tdc;
	}

	public void setNum_tdc(String num_tdc) {
		this.num_tdc = num_tdc;
	}

	public String getIds_motivos() {
		return ids_motivos;
	}

	public void setIds_motivos(String ids_motivos) {
		this.ids_motivos = ids_motivos;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	
}
