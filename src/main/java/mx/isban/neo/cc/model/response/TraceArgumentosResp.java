package mx.isban.neo.cc.model.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TraceArgumentosResp implements Serializable{
	

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("idmensaje")
	private int IDMENSAJE;
	
	@JsonProperty("mensaje")
	private String MENSAJE;
	
	@JsonProperty("ide_argumento")
	private String IDE_ARGUMENTO;
	
	@JsonProperty("nom_argumento")
	private String NOM_ARGUMENTO;
	

}
