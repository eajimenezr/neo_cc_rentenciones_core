package mx.isban.neo.cc.model.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CodesPendingResp implements Serializable {

	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("id_mensaje")
	private int IDMENSAJE;
	
	@JsonProperty("mensaje")
	private String MENSAJE;
	
	@JsonProperty("buc")
	private String BUC;
	
	@JsonProperty("pan")
	private String PAN;
	
	@JsonProperty("ide_tarjeta")
	private String IDE_TARJETA;

	public int getIDMENSAJE() {
		return IDMENSAJE;
	}

	public void setIDMENSAJE(int iDMENSAJE) {
		IDMENSAJE = iDMENSAJE;
	}

	public String getMENSAJE() {
		return MENSAJE;
	}

	public void setMENSAJE(String mENSAJE) {
		MENSAJE = mENSAJE;
	}

	public String getBUC() {
		return BUC;
	}

	public void setBUC(String bUC) {
		BUC = bUC;
	}

	public String getPAN() {
		return PAN;
	}

	public void setPAN(String pAN) {
		PAN = pAN;
	}

	public String getIDE_TARJETA() {
		return IDE_TARJETA;
	}

	public void setIDE_TARJETA(String iDE_TARJETA) {
		IDE_TARJETA = iDE_TARJETA;
	}


	
}
