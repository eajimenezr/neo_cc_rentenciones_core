package mx.isban.neo.cc.model.beans;


import java.io.Serializable;
import java.util.Map;

public class DmlBean implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String pkg;
	
	private String funcion;
	
	private Map<String,String> paramsIn;
	
	private Map<String,String> paramsOut;

	public String getPkg() {
		return pkg;
	}

	public void setPkg(String pkg) {
		this.pkg = pkg;
	}

	public String getFuncion() {
		return funcion;
	}

	public void setFuncion(String funcion) {
		this.funcion = funcion;
	}

	public Map<String, String> getParamsIn() {
		return paramsIn;
	}

	public void setParamsIn(Map<String, String> paramsIn) {
		this.paramsIn = paramsIn;
	}

	public Map<String, String> getParamsOut() {
		return paramsOut;
	}

	public void setParamsOut(Map<String, String> paramsOut) {
		this.paramsOut = paramsOut;
	}
	
	


}
