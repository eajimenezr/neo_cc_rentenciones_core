/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ProductoCliente.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2019-03-28                               Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc.model.beans.cuenta.vista;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import mx.isban.neo.cc.utils.ConstastCommon;

/**
 * Clase de tipo dto para la informacion detalle del producto contratado y su estatus.<br>
 * Contiene Get/Sets de los componentes:<br>
 * <li>producto</li>
 * <li>subProducto</li>
 * <li>estatusProducto</li>
 * @author Isban / iscontreras
 * Class ProductoCliente.java 
 */
@ApiModel
@JsonIgnoreProperties(ignoreUnknown = true )
@JsonInclude(com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL)
public class ProductoCliente implements Serializable {

	/**
	 *  long serialVersionUID : id de transporte para las diferentes capas.
	 */
	private static final long serialVersionUID = 8088235306830455018L;

	/**
	 *  String producto : codigo del producto contratado por el cliente
	 */
	@ApiModelProperty(value="producto")
	@NotNull(message = "producto" + ConstastCommon.NOT_NULL_MSG)
	@NotEmpty(message = "producto" + ConstastCommon.NOT_EMPTY_MSG)
	private String producto;
	
	/**
	 *  String subProducto : codigo del subproducto
	 */
	@ApiModelProperty(value="subProducto")
	@NotNull(message = "subProducto" + ConstastCommon.NOT_NULL_MSG)
	@NotEmpty(message = "subProducto" + ConstastCommon.NOT_EMPTY_MSG)
	private String subProducto;
	
	/**
	 *  String estatusProducto
	 */
	@ApiModelProperty(value="estatusProducto")
	private String estatusProducto;
	
	/**
	 * Gets the estatusProducto
	 * @return the estatusProducto
	 */
	public String getEstatusProducto() {
		return estatusProducto;
	}

	/**
	 * Sets the new estatusProducto 
	 * @param estatusProducto the estatusProducto to set
	 */
	public void setEstatusProducto(String estatusProducto) {
		this.estatusProducto = estatusProducto;
	}

	/**
	 * Gets the producto
	 * @return the producto
	 */
	public String getProducto() {
		return producto;
	}

	/**
	 * Sets the new producto 
	 * @param producto the producto to set
	 */
	public void setProducto(String producto) {
		this.producto = producto;
	}

	/**
	 * Gets the subProducto
	 * @return the subProducto
	 */
	public String getSubProducto() {
		return subProducto;
	}

	/**
	 * Sets the new subProducto 
	 * @param subProducto the subProducto to set
	 */
	public void setSubProducto(String subProducto) {
		this.subProducto = subProducto;
	}
	

}
