package mx.isban.neo.cc.model.response;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class TdcCanceladasResp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private CancelTitularResp titular;
	
	private CancelAdicResp adicionales;

	public CancelTitularResp getTitular() {
		return titular;
	}

	public void setTitular(CancelTitularResp titular) {
		this.titular = titular;
	}

	public CancelAdicResp getAdicionales() {
		return adicionales;
	}

	public void setAdicionales(CancelAdicResp adicionales) {
		this.adicionales = adicionales;
	}

	
}
