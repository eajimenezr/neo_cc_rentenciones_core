package mx.isban.neo.cc.model.response;

import java.io.Serializable;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CatalogoResp implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CatalogoResp() {
	}
	
	@JsonProperty("id_mensaje")
	private int IDMENSAJE;
	
	@JsonProperty("mensaje")
	private String MENSAJE;
	
	@JsonProperty("id_Item")
	private String IDITEM;
	

	@JsonProperty("desc_Item")
	private String DESCITEM;

	public int getIDMENSAJE() {
		// TODO Auto-generated method stub
		return IDMENSAJE;
	}

	
}
