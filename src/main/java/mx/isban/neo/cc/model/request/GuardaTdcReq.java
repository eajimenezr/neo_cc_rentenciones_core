package mx.isban.neo.cc.model.request;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GuardaTdcReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String cve_folio;
	private String num_tarjeta;
	private double imp_saldo;
	private double imp_lim_credito;
	private String fec_vigencia;
	private String fec_antiguedad;
	private String ind_status_tarjeta;
	private String ind_tipo_tarjeta;
	private String ide_producto;
	private String ide_sub_producto;
	private String ide_codigo_bloqueo;
	private String ide_mot_llamada;
	private String ind_status_retencion;
	private double mto_recibido;
	private String fecha_alta_TDC;
	private String fecha_limite_pago;
	private String numero_contrato;
	private String num_tarjeta_origen;
	public String getCve_folio() {
		return cve_folio;
	}
	public void setCve_folio(String cve_folio) {
		this.cve_folio = cve_folio;
	}
	public String getNum_tarjeta() {
		return num_tarjeta;
	}
	public void setNum_tarjeta(String num_tarjeta) {
		this.num_tarjeta = num_tarjeta;
	}
	public double getImp_saldo() {
		return imp_saldo;
	}
	public void setImp_saldo(double imp_saldo) {
		this.imp_saldo = imp_saldo;
	}
	public double getImp_lim_credito() {
		return imp_lim_credito;
	}
	public void setImp_lim_credito(double imp_lim_credito) {
		this.imp_lim_credito = imp_lim_credito;
	}
	public String getFec_vigencia() {
		return fec_vigencia;
	}
	public void setFec_vigencia(String fec_vigencia) {
		this.fec_vigencia = fec_vigencia;
	}
	public String getFec_antiguedad() {
		return fec_antiguedad;
	}
	public void setFec_antiguedad(String fec_antiguedad) {
		this.fec_antiguedad = fec_antiguedad;
	}
	public String getInd_status_tarjeta() {
		return ind_status_tarjeta;
	}
	public void setInd_status_tarjeta(String ind_status_tarjeta) {
		this.ind_status_tarjeta = ind_status_tarjeta;
	}
	public String getInd_tipo_tarjeta() {
		return ind_tipo_tarjeta;
	}
	public void setInd_tipo_tarjeta(String ind_tipo_tarjeta) {
		this.ind_tipo_tarjeta = ind_tipo_tarjeta;
	}
	public String getIde_producto() {
		return ide_producto;
	}
	public void setIde_producto(String ide_producto) {
		this.ide_producto = ide_producto;
	}
	public String getIde_sub_producto() {
		return ide_sub_producto;
	}
	public void setIde_sub_producto(String ide_sub_producto) {
		this.ide_sub_producto = ide_sub_producto;
	}
	public String getIde_codigo_bloqueo() {
		return ide_codigo_bloqueo;
	}
	public void setIde_codigo_bloqueo(String ide_codigo_bloqueo) {
		this.ide_codigo_bloqueo = ide_codigo_bloqueo;
	}
	public String getIde_mot_llamada() {
		return ide_mot_llamada;
	}
	public void setIde_mot_llamada(String ide_mot_llamada) {
		this.ide_mot_llamada = ide_mot_llamada;
	}
	public String getInd_status_retencion() {
		return ind_status_retencion;
	}
	public void setInd_status_retencion(String ind_status_retencion) {
		this.ind_status_retencion = ind_status_retencion;
	}
	public double getMto_recibido() {
		return mto_recibido;
	}
	public void setMto_recibido(double mto_recibido) {
		this.mto_recibido = mto_recibido;
	}
	public String getFecha_alta_TDC() {
		return fecha_alta_TDC;
	}
	public void setFecha_alta_TDC(String fecha_alta_TDC) {
		this.fecha_alta_TDC = fecha_alta_TDC;
	}
	public String getFecha_limite_pago() {
		return fecha_limite_pago;
	}
	public void setFecha_limite_pago(String fecha_limite_pago) {
		this.fecha_limite_pago = fecha_limite_pago;
	}
	public String getNumero_contrato() {
		return numero_contrato;
	}
	public void setNumero_contrato(String numero_contrato) {
		this.numero_contrato = numero_contrato;
	}
	public String getNum_tarjeta_origen() {
		return num_tarjeta_origen;
	}
	public void setNum_tarjeta_origen(String num_tarjeta_origen) {
		this.num_tarjeta_origen = num_tarjeta_origen;
	}
	
	
	
}
