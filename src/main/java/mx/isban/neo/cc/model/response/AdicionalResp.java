package mx.isban.neo.cc.model.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdicionalResp implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	
	@JsonProperty("id_mensaje")
	private int IDMENSAJE;
	
	@JsonProperty("mensaje")
	private String MENSAJE;
	
	@JsonProperty("tdc_adicional")
	private String TDCADICIONAL;
	
	@JsonProperty("codigo_bloqueo")
    private String CODIGOBLOQUEO;

	public int getIDMENSAJE() {
		return IDMENSAJE;
	}

	public void setIDMENSAJE(int iDMENSAJE) {
		IDMENSAJE = iDMENSAJE;
	}

	public String getMENSAJE() {
		return MENSAJE;
	}

	public void setMENSAJE(String mENSAJE) {
		MENSAJE = mENSAJE;
	}

	public String getTDCADICIONAL() {
		return TDCADICIONAL;
	}

	public void setTDCADICIONAL(String tDCADICIONAL) {
		TDCADICIONAL = tDCADICIONAL;
	}

	public String getCODIGOBLOQUEO() {
		return CODIGOBLOQUEO;
	}

	public void setCODIGOBLOQUEO(String cODIGOBLOQUEO) {
		CODIGOBLOQUEO = cODIGOBLOQUEO;
	}

	
	
}
