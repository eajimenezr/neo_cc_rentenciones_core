package mx.isban.neo.cc.model.request;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CancelRq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String cve_folio;
	private String num_tdc;
	private String id_validacion;
	private String id_respuesta;
	private String txt_folioCapturado;
	private String txt_folioCapturado2;
	private String tipoOperacion;
	public String getCve_folio() {
		return cve_folio;
	}
	public void setCve_folio(String cve_folio) {
		this.cve_folio = cve_folio;
	}
	public String getNum_tdc() {
		return num_tdc;
	}
	public void setNum_tdc(String num_tdc) {
		this.num_tdc = num_tdc;
	}
	public String getId_validacion() {
		return id_validacion;
	}
	public void setId_validacion(String id_validacion) {
		this.id_validacion = id_validacion;
	}
	public String getId_respuesta() {
		return id_respuesta;
	}
	public void setId_respuesta(String id_respuesta) {
		this.id_respuesta = id_respuesta;
	}
	public String getTxt_folioCapturado() {
		return txt_folioCapturado;
	}
	public void setTxt_folioCapturado(String txt_folioCapturado) {
		this.txt_folioCapturado = txt_folioCapturado;
	}
	public String getTxt_folioCapturado2() {
		return txt_folioCapturado2;
	}
	public void setTxt_folioCapturado2(String txt_folioCapturado2) {
		this.txt_folioCapturado2 = txt_folioCapturado2;
	}
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	
	
	
}
