/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * CuentaVista.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2019-03-28                               Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc.model.beans.cuenta.vista;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import mx.isban.neo.cc.utils.ConstastCommon;

/**
 * Clase de tipo dto para la informacion del cliente en cuenta vista.<br>
 * Contiene Get/Sets de los componentes:<br>
 * <li>cliente</li>
 * @author Isban / iscontreras
 * Class CuentaVista.java 
 */
@ApiModel
@JsonIgnoreProperties(ignoreUnknown = true )
@JsonInclude(com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL)
public class CuentaVista  implements Serializable {

	/**
	 *  long serialVersionUID: id de transporte para las diferentes capas.
	 */
	private static final long serialVersionUID = -4202117444900211001L;
	/**
	 *  Cliente cliente : informacion del cliente a buscar en cuenta vista
	 */
	@Valid
	@ApiModelProperty(value="cliente")
	@NotNull(message = "cliente" + ConstastCommon.NOT_NULL_MSG)
	private Cliente cliente;
	/**
	 * Gets the cliente
	 * @return the cliente
	 */
	public Cliente getCliente() {
		return cliente;
	}
	/**
	 * Sets the new cliente 
	 * @param cliente the cliente to set
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
}
