package mx.isban.neo.cc.model.request;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BenefitsReq implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String id_producto;
	private String id_subproducto;
	private String desc_producto;
	private String desc_producto_red;
	private String desc_beneficio;
	public String getId_producto() {
		return id_producto;
	}
	public void setId_producto(String id_producto) {
		this.id_producto = id_producto;
	}
	public String getId_subproducto() {
		return id_subproducto;
	}
	public void setId_subproducto(String id_subproducto) {
		this.id_subproducto = id_subproducto;
	}
	public String getDesc_producto() {
		return desc_producto;
	}
	public void setDesc_producto(String desc_producto) {
		this.desc_producto = desc_producto;
	}
	public String getDesc_producto_red() {
		return desc_producto_red;
	}
	public void setDesc_producto_red(String desc_producto_red) {
		this.desc_producto_red = desc_producto_red;
	}
	public String getDesc_beneficio() {
		return desc_beneficio;
	}
	public void setDesc_beneficio(String desc_beneficio) {
		this.desc_beneficio = desc_beneficio;
	}
	
	
}
