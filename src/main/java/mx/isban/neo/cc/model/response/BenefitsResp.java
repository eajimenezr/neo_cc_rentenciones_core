package mx.isban.neo.cc.model.response;

import java.io.Serializable;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BenefitsResp implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public  BenefitsResp() {
		// TODO Auto-generated constructor stub
	}
	
	@JsonProperty("id_mensaje")
	private int IDMENSAJE;

	@JsonProperty("mensaje")
	private String MENSAJE;

	@JsonProperty("id_producto")
	private int IDE_PRODUCTO;

	@JsonProperty("id_sub_producto")
	private int IDE_SUB_PRODUCTO;

	@JsonProperty("des_producto")
	private String DES_PRODUCTO; 

	@JsonProperty("des_producto_red")
	private String DES_PRODUCTO_RED;

	@JsonProperty("des_beneficios")
	private String DES_BENEFICIOS;

	public int getIDMENSAJE() {
		// TODO Auto-generated method stub
		return 0;
	}

	
}
