package mx.isban.neo.cc.model.request;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScriptsReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String cve_folio;
	private String nombre_Ejecutivo;
	private String nombre_Cliente;
	private String num_Tarjeta;
	private String vigencia_Tarjeta;
	private String script;
	
	
	
}
