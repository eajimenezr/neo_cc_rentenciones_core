package mx.isban.neo.cc.model.request;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FusionReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String cve_folio;
	private String num_tarjeta;
	private String idValidacion;
	private String ide_respuesta;
	public String getCve_folio() {
		return cve_folio;
	}
	public void setCve_folio(String cve_folio) {
		this.cve_folio = cve_folio;
	}
	public String getNum_tarjeta() {
		return num_tarjeta;
	}
	public void setNum_tarjeta(String num_tarjeta) {
		this.num_tarjeta = num_tarjeta;
	}
	public String getIdValidacion() {
		return idValidacion;
	}
	public void setIdValidacion(String idValidacion) {
		this.idValidacion = idValidacion;
	}
	public String getIde_respuesta() {
		return ide_respuesta;
	}
	public void setIde_respuesta(String ide_respuesta) {
		this.ide_respuesta = ide_respuesta;
	}
		
	
}
