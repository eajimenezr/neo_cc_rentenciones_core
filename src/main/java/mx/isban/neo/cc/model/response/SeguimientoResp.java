package mx.isban.neo.cc.model.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;



@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SeguimientoResp implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public SeguimientoResp() {
//        this.motivos_cancelacion = new List<MotivosCancelResp>();
//        this.argumentos = new ArrayList<TraceArgumentosResp>();

	}
	
	@JsonProperty("idmensaje")
	private int IDMENSAJE;
	
	@JsonProperty("mensaje")
	private String MENSAJE;
	
	@JsonProperty("cve_folio")
	private String CVE_FOLIO;
	
	@JsonProperty("num_tarjeta")
	private String NUM_TARJETA;
	
	@JsonProperty("ind_status_ret")
	private String IND_STATUS_RETENCION;
	
	@JsonProperty("desc_status")
	private String DESCESTATUS;
	
	@JsonProperty("fecha_llamada")
	private String FECHALLAMADA;
	
	@JsonProperty("cve_ejecutivo")
	private String CVE_EJECUTIVO;
	
	@JsonProperty("nombreEjecutivo")
	private String NOMBREEJECUTIVO;

	@JsonProperty("cve_Jefe")
	private String CVE_JEFE;
	
	@JsonProperty("nombre_Supervisor")
	private String NOMBRESUPERVISOR;
	
	@JsonProperty("motivos_cancelacion")
	private MotivosCancelaResp[] motivos_cancelacion;
	
	@JsonProperty("argumentos")
	private TraceArgumentosResp[] argumentos;

	public int getIDMENSAJE() {
		// TODO Auto-generated method stub
		return IDMENSAJE;
	}

	public String getMENSAJE() {
		return MENSAJE;
	}

	public void setMENSAJE(String mENSAJE) {
		MENSAJE = mENSAJE;
	}

	public String getCVE_FOLIO() {
		return CVE_FOLIO;
	}

	public void setCVE_FOLIO(String cVE_FOLIO) {
		CVE_FOLIO = cVE_FOLIO;
	}

	public String getNUM_TARJETA() {
		return NUM_TARJETA;
	}

	public void setNUM_TARJETA(String nUM_TARJETA) {
		NUM_TARJETA = nUM_TARJETA;
	}

	public String getIND_STATUS_RETENCION() {
		return IND_STATUS_RETENCION;
	}

	public void setIND_STATUS_RETENCION(String iND_STATUS_RETENCION) {
		IND_STATUS_RETENCION = iND_STATUS_RETENCION;
	}

	public String getDESCESTATUS() {
		return DESCESTATUS;
	}

	public void setDESCESTATUS(String dESCESTATUS) {
		DESCESTATUS = dESCESTATUS;
	}

	public String getFECHALLAMADA() {
		return FECHALLAMADA;
	}

	public void setFECHALLAMADA(String fECHALLAMADA) {
		FECHALLAMADA = fECHALLAMADA;
	}

	public String getCVE_EJECUTIVO() {
		return CVE_EJECUTIVO;
	}

	public void setCVE_EJECUTIVO(String cVE_EJECUTIVO) {
		CVE_EJECUTIVO = cVE_EJECUTIVO;
	}

	public String getNOMBREEJECUTIVO() {
		return NOMBREEJECUTIVO;
	}

	public void setNOMBREEJECUTIVO(String nOMBREEJECUTIVO) {
		NOMBREEJECUTIVO = nOMBREEJECUTIVO;
	}

	public String getCVE_JEFE() {
		return CVE_JEFE;
	}

	public void setCVE_JEFE(String cVE_JEFE) {
		CVE_JEFE = cVE_JEFE;
	}

	public String getNOMBRESUPERVISOR() {
		return NOMBRESUPERVISOR;
	}

	public void setNOMBRESUPERVISOR(String nOMBRESUPERVISOR) {
		NOMBRESUPERVISOR = nOMBRESUPERVISOR;
	}

	public MotivosCancelaResp[] getMotivos_cancelacion() {
		return motivos_cancelacion;
	}

	public void setMotivos_cancelacion(MotivosCancelaResp[] motivos_cancelacion) {
		this.motivos_cancelacion = motivos_cancelacion;
	}

	public TraceArgumentosResp[] getArgumentos() {
		return argumentos;
	}

	public void setArgumentos(TraceArgumentosResp[] argumentos) {
		this.argumentos = argumentos;
	}

	public void setIDMENSAJE(int iDMENSAJE) {
		IDMENSAJE = iDMENSAJE;
	}

	
	
}
