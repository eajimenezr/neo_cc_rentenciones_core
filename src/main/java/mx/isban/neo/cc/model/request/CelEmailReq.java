package mx.isban.neo.cc.model.request;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CelEmailReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String cve_folio;
	private String num_tdc;
	private String cel;
	private String email;
	public String getCve_folio() {
		return cve_folio;
	}
	public void setCve_folio(String cve_folio) {
		this.cve_folio = cve_folio;
	}
	public String getNum_tdc() {
		return num_tdc;
	}
	public void setNum_tdc(String num_tdc) {
		this.num_tdc = num_tdc;
	}
	public String getCel() {
		return cel;
	}
	public void setCel(String cel) {
		this.cel = cel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
}
