package mx.isban.neo.cc.model.response;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FusionResp implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("idmensaje")
	private int IDMENSAJE;
	
	@JsonProperty("mensaje")
	private String MENSAJE;
	
	@JsonProperty("id_pantalla")
	private int IDPANTALLA;
	
	@JsonProperty("txt_pregunta")
	private String TXT_PREGUNTA;
	
	@JsonProperty("id_validacion")
	private int IDE_VALIDACION;
	
	@JsonProperty("id_respuesta")
	private String IDE_RESPUESTA;
	
	@JsonProperty("validacion")
	private String TXT_RESPUESTA;
	
	@JsonProperty("ofertable")
	private String OFERTABLE;
	
	@JsonProperty("respuestas")
	private List<Respuestas> respuestas;

	public int getIDMENSAJE() {
		// TODO Auto-generated method stub
		return IDMENSAJE;
	}

	public String getMENSAJE() {
		return MENSAJE;
	}

	public void setMENSAJE(String mENSAJE) {
		MENSAJE = mENSAJE;
	}

	public int getIDPANTALLA() {
		return IDPANTALLA;
	}

	public void setIDPANTALLA(int iDPANTALLA) {
		IDPANTALLA = iDPANTALLA;
	}

	public String getTXT_PREGUNTA() {
		return TXT_PREGUNTA;
	}

	public void setTXT_PREGUNTA(String tXT_PREGUNTA) {
		TXT_PREGUNTA = tXT_PREGUNTA;
	}

	public int getIDE_VALIDACION() {
		return IDE_VALIDACION;
	}

	public void setIDE_VALIDACION(int iDE_VALIDACION) {
		IDE_VALIDACION = iDE_VALIDACION;
	}

	public String getIDE_RESPUESTA() {
		return IDE_RESPUESTA;
	}

	public void setIDE_RESPUESTA(String iDE_RESPUESTA) {
		IDE_RESPUESTA = iDE_RESPUESTA;
	}

	public String getTXT_RESPUESTA() {
		return TXT_RESPUESTA;
	}

	public void setTXT_RESPUESTA(String tXT_RESPUESTA) {
		TXT_RESPUESTA = tXT_RESPUESTA;
	}

	public String getOFERTABLE() {
		return OFERTABLE;
	}

	public void setOFERTABLE(String oFERTABLE) {
		OFERTABLE = oFERTABLE;
	}

	public List<Respuestas> getRespuestas() {
		return respuestas;
	}

	public void setRespuestas(List<Respuestas> respuestas) {
		this.respuestas = respuestas;
	}

	public void setIDMENSAJE(int iDMENSAJE) {
		IDMENSAJE = iDMENSAJE;
	}

	
	
}
