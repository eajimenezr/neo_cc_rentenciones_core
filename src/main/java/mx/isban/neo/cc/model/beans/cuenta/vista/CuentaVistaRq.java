/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * CuentaVistaRq.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2019-03-28                               Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc.model.beans.cuenta.vista;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import mx.isban.neo.cc.utils.ConstastCommon;

/**
 * Clase de tipo dto para la informacion de cuenta vista de entrada en el servicio.<br>
 * Contiene Get/Sets de los componentes:<br>
 * <li>cuentaVista</li>
 * @author Isban / iscontreras
 * Class CuentaVistaRq.java 
 */
@ApiModel
@JsonIgnoreProperties(ignoreUnknown = true )
@JsonInclude(com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL)
public class CuentaVistaRq  implements Serializable {

	/**
	 *  long serialVersionUID: id de transporte para las diferentes capas.
	 */
	private static final long serialVersionUID = -4594683265254354887L;
	/**
	 *  CuentaVista cuentaVista : atributo de tipo objeto con la informacion de entrada para cuenta vista.
	 */
	@Valid
	@ApiModelProperty(value="cuentaVista")
	@NotNull(message = "cuentaVista" + ConstastCommon.NOT_NULL_MSG)
	private CuentaVista cuentaVista;

	/**
	 * Gets the cuentaVista
	 * @return the cuentaVista
	 */
	public CuentaVista getCuentaVista() {
		return cuentaVista;
	}

	/**
	 * Sets the new cuentaVista 
	 * @param cuentaVista the cuentaVista to set
	 */
	public void setCuentaVista(CuentaVista cuentaVista) {
		this.cuentaVista = cuentaVista;
	}
	
}
