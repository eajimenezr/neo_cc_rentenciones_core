package mx.isban.neo.cc.model.response;

import java.io.Serializable;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class updArgResp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public updArgResp() {
	}
	
	@JsonProperty("idmensaje")
	private int IDMENSAJE;
	
	@JsonProperty("mensaje")
	private String MENSAJE;
	
	@JsonProperty("id_ComboAdicional")
	private String IDCOMBOADICIONAL;
	
	
	@JsonProperty("id_Pantalla_Captura")
	private String IDPANTALLACAPTURA;

	@JsonProperty("pasos_Adicionales")
	private String PASOSADICIONALES;
	
	@JsonProperty("id_Pantalla_Pasos")
	private String IDPANTALLAPASOS;

	public int getIDMENSAJE() {
		// TODO Auto-generated method stub
		return IDMENSAJE;
	}

	
}
