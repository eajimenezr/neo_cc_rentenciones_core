package mx.isban.neo.cc.model.response;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CancelTitularResp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private int id_mensaje;
	
	private String mensaje;
	
	private String pasoAdicional;

	public int getId_mensaje() {
		return id_mensaje;
	}

	public void setId_mensaje(int id_mensaje) {
		this.id_mensaje = id_mensaje;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getPasoAdicional() {
		return pasoAdicional;
	}

	public void setPasoAdicional(String pasoAdicional) {
		this.pasoAdicional = pasoAdicional;
	}

	
	
}
