package mx.isban.neo.cc.model.response;

import java.io.Serializable;




import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MessageResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MessageResponse() {
	}
	
	@JsonProperty("idmensaje")
	private int IDMENSAJE;
	
	@JsonProperty("mensaje")
	private String MENSAJE;
	
	@JsonProperty("id_argumento")
	private String IDARGUMENTO;
	
	
	@JsonProperty("nombre_argumento")
	private String NOMBREARGUMENTO;

	@JsonProperty("des_argumento")
	private String DESCARGUMENTO;
	
	@JsonProperty("validaciones")
	private String VALIDACIONES;
	
	@JsonProperty("id_pantalla")
	private String IDPANTALLA;
	
	@JsonProperty("id_status")
	private String IDSTATUS;
	
	@JsonProperty("desc_status")
	private String DESCSTATUS;
	
	@JsonProperty("modificable")
	private int MODIFICABLE;
	
	@JsonProperty("script")
	private String SCRIPT;

	public int getIDMENSAJE() {
		// TODO Auto-generated method stub
		return IDMENSAJE;
	}

	
	
}
