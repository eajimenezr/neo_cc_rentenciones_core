package mx.isban.neo.cc.model.response;

import java.io.Serializable;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MotivosCancelResp implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MotivosCancelResp() {
		
	}
	@JsonProperty("id_mensaje")
	private int IDMENSAJE;	
	
	@JsonProperty("mensaje")
	private String MENSAJE;
	
	@JsonProperty("id_motivo")
	private int IDMOTIVO;
		
	@JsonProperty("desc_motivo")
	private String DESCRIPCIONMOTIVO;
	
	@JsonProperty("prioridad")
	private String PRIORIDAD;

	public int getIDMENSAJE() {
		// TODO Auto-generated method stub
		return 0;
	}


}
