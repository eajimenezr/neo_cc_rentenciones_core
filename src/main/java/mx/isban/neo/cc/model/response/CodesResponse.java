
package mx.isban.neo.cc.model.response;


import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;

/**
 * Clase TO que contiene los datos de respuesta hacia las transacciones
 * reutilizables de NEO
 * 
 * @author Z266957
 *
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"ID_MENSAJE",
"MENSAJE",
"CODIGO",
"LABOR_RETENCION",
"DESCRIPTION_BLOQUEO"
})
public class CodesResponse implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CodesResponse() {
		// TODO Auto-generated constructor stub
	}

	@JsonProperty("id_mensaje")
	private int IDMENSAJE;
	
	@JsonProperty("mensaje")
	private String MENSAJE;
	
	@JsonProperty("codigo")
	private BigDecimal CODIGO;
	
	@JsonProperty("labor_retencion")
	private BigDecimal LABOR_RETENCION;
	
	@JsonProperty("description_bloqueo")
	private String DESCRIPTION_BLOQUEO;

	public int getIDMENSAJE() {
		// TODO Auto-generated method stub
		return 0;
	}


}
