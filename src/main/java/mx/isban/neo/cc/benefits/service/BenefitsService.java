package mx.isban.neo.cc.benefits.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.isban.neo.cc.folio.service.IBuilderDMLService;
import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.BenefitsResp;

@Service
public class BenefitsService implements IBenefitsService {

	@Autowired
	private IBuilderDMLService buildService;
	
	@Override
	public BenefitsResp[] coredmlBeneficios(DmlBean bean) {
		 return buildService.buildDmlBnft(bean);
	}
	
	
	
}
