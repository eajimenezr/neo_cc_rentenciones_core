package mx.isban.neo.cc.benefits.service;

import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.BenefitsResp;


public interface IBenefitsService {

	BenefitsResp[] coredmlBeneficios(DmlBean bean);
	
}