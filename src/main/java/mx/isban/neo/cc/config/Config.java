/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * Config.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2018-10-16                               Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc.config;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.client.RestTemplate;

import mx.isban.neo.cc.model.rs.CodError;

/**
 * clase de tipo configuracion.
 * Contiene los metodos:<br>
 * <ul><li>dataSource   </li>
 * <li>jdbcTemplate </li>
 * <li>validator    </li>
 * <li>jmsTemplate </li>
 * <li>restTemplate</li>
 * <li>bufferingClientHttpRequestFactory</li>
 * 
 * </ul>
 * <p>Variables globales</p>
 * <ul>
 * <li> dbUrl      </li>
 * <li> user       </li>
 * <li> pass       </li>
 * <li> dbTimeOut       </li>
 * <li> timeOutRead       </li>
 * <li> timeOutConnection       </li>
 * </ul>
 * @author Isban / Z046293
 * Class Config.java 
 */
@Configuration
public class Config {
	/**
	 * Constante para trazar la aplicacion.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(Config.class);

	/**
	 *  Integer dbTimeOut : tiempo de expiracion en la conexin a la BD
	 */
	@Value("${bd.time.out}")
	private Integer dbTimeOut;

	
	/** 
	 *  String timeOutConnection : tiempo de expiracion de la conexion
	 */
	@Value("${resttemplate.time.out.connection}")
	private Integer timeOutConnection;
	
	/**
	 * Method jdbcTemplate
	 * Method objective: Metodo de inyeccion para el jdbcTemplate, se le agrega el datasource que contiene la conexion a la BD
	 * Se agregan porpiedades de la  conexion .<br>
	 * @param dataSource DataSource con la informacion para realizar la conexion a oracle
	 * @return JdbcTemplate con la configuracion del dataSource
	 */
	@Bean
	public JdbcTemplate jdbcTemplate(DataSource dataSource) {
		LOG.info("CARGANDO JDBC TEMPLATE...");
		// se devuelve el bean con el data source
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource,false);
		jdbcTemplate.setQueryTimeout(3000);
		return jdbcTemplate;
	}
	
	/**
	 * Bean para inicializar el restTemplate
	 * Methos restTemplate: return type RestTemplate 
	 * @param restBuilder inicializacion del rest template builder
	 * @return restTemplate bean de tipo restTemplateP
	 */
	 @Primary
	 @Bean(name = "restTemplateP")
	public RestTemplate restTemplateP(BufferingClientHttpRequestFactory bufferingClientHttpRequestFactory) {
		RestTemplate restTemplateP = new RestTemplate(bufferingClientHttpRequestFactory);
		//se agrega listado de interceptor para agregar encabezados
		List<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
		// se agrega el interceptor para logear peticiones
		interceptors.add(new RequestInterceptor());
		restTemplateP.setInterceptors(interceptors);
		return restTemplateP;
	}
	 
	/**
	 * Method restTemplateS
	 * Method objective: Bean para inicializar el restTemplate .<br>
	 * @param restBuilder inicializacion del rest template builder
	 * @return RestTemplate bean de tipo restTemplateS
	 */
	@Bean(name = "restTemplateS")
	public RestTemplate restTemplateS(RestTemplateBuilder restBuilder) {
		return restBuilder.build();
	}
	
	/**
	 * Bean para incializar el BufferingClientHttpRequestFactory que requiere de un simpleClientHttpRequestFactory
	 * Methos bufferingClientHttpRequestFactory: return type BufferingClientHttpRequestFactory 
	 * @return bufferingClientHttpRequestFactory
	 */
	@Bean
	public BufferingClientHttpRequestFactory bufferingClientHttpRequestFactory() {
		SimpleClientHttpRequestFactory simpleClientHttpRequestFactory = new SimpleClientHttpRequestFactory();
		return new BufferingClientHttpRequestFactory(simpleClientHttpRequestFactory);
	}	
	
	/**
	 * Metodo para generar el bean que transportara los mensajes de error
	 * Methos CodError: return type ErrorMessage 
	 * @return bean CodError
	 */
	@Bean
	public CodError codError() {
		return new CodError();
	}
}
