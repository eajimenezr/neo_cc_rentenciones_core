package mx.isban.neo.cc.config;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

/**
 * Clase de tipo interceptor para registrar respuestas o peticiones de solicitudes https .
 * Contiene los metodos: <br>
 * <Strong>intercept</Strong>
 * @author Isban / Z046293
 * Class RequestInterceptor.java 
 */
public class RequestInterceptor implements ClientHttpRequestInterceptor {
	/**
	 * Variable para el logeo en consola
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(RequestInterceptor.class);

	/**
	 * muestra el log de la ejecucion
	 */
	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {
		// Ejecuta el request
		ClientHttpResponse response = execution.execute(request, body);
		// logea la url a invocar
		LOGGER.info("URL " + request.getURI());
		// ejecuta el metodo a invocar
		LOGGER.info("metodo " + request.getMethod());
		// se devuelve la respuesta
		return response;
	}

}
