/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ConstantsSql.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2018-10-22                               Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc.utils;

/**
 * Clase final para definir constantes de tipo sql que se reutilizan en el proyecto.<br>
 * Contiene las constantes:<br>
 * <li>WARNING                       </li>
 * <li>P_S_WARNING                   </li>
 * <li>DML_ROLLBACK_MSG              </li>
 * <li>DML_ROLLBACK                  </li>
 * <li>STR_ERRSQL001                 </li>
 * <li>STR_ERRSQL001_MSG             </li>
 * <li>STR_ERRSQL002                 </li>
 * <li>STR_ERRSQL002_MSG             </li>
 * <li>STR_ERRSQL003                 </li>
 * <li>STR_ERRSQL003_MSG             </li>
 * <li>RES_DML                       </li>
 * <li>V_S_PROCEDIMIENTO             </li>
 * <li>OPERATION_TYPE_INSERT_PARAMS  </li>
 * <li>OPERATION_TYPE_UPDATE_PARAMS  </li>
 * <li>OPERATION_TYPE_DELETE_PARAMS  </li>
 * <li>OPERATION_TYPE_QUERY_PARAMS   </li>
 * <li>OPERATION_TYPE_QUERY          </li>
 * @author Isban / iscontreras
 * Class ConstantsSql.java 
 */
public final class ConstantsSql {

	/**
	 * Custom builder with the elements:
	 */
	private ConstantsSql() {
		
	}
}
