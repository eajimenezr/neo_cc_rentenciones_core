/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * UtilMappingPosicionConsolidada.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2019-12-23       Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc.utils;

import java.math.BigDecimal;

import javax.xml.bind.JAXBElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.isban.neo.cc.posicion.consolidada.wsdl.BuscarPosicionConsolidadaRequest;
import mx.isban.neo.cc.posicion.consolidada.wsdl.ObjectFactory;

/**
 * Type class: ____ .<br>
 * Main objective: ___ .<br>
 * Contains gets / sets methods:
 * <ul>
 * <li></li>
 * </ul>
 * Global constants or variables:
 * <ul>
 * <li></li>
 * </ul>
 * @author Isban / iscontreras
 * UtilMappingPosicionConsolidada.java by Z055749
 *
 */
public final class UtilMappingPosicionConsolidada {
	
	/**
	 * Variable para logeo en consola
	 */
	private static final Logger LOG = LoggerFactory.getLogger(UtilMappingPosicionConsolidada.class);

	/**
	 *  LoginUtileria instance: intancia de la clase
	 */
	private static UtilMappingPosicionConsolidada  instance = new UtilMappingPosicionConsolidada();
	/**
	 * Custom builder with the elements:
	 */
	private UtilMappingPosicionConsolidada() {
		
	}
	
	/**
     * devuelve instancia de la clase en tipo singleton
     *
     * @return instance instancia de LoginUtileria;
     */
    public static UtilMappingPosicionConsolidada getSingletonInstance() {
        return instance;
    }
	
	/**
	 * Method mappingSoapDataPC
	 * Method objective: ___ .<br>
	 * @param numClie numero de cliente del cual buscaremos su datos en posicion consolidada
	 * @return JAXBElement<BuscarPosicionConsolidadaRequest> mapa de datos recuperados del servicio
	 */
	public JAXBElement<BuscarPosicionConsolidadaRequest> mappingSoapDataPC(String numClie){
		LOG.info("inicia.mappingSoapDataPC");
		
		// se obtiene el factory generado por el cliente
		ObjectFactory objectFactory = new ObjectFactory();
		// se instancea el objeto con el factory
		BuscarPosicionConsolidadaRequest buscarPosicionConsolidadaRequest = objectFactory
				.createBuscarPosicionConsolidadaRequest();
		// se asigna el valor del request soap
		buscarPosicionConsolidadaRequest.setBuc(numClie);
		LOG.info("termina.mappingSoapDataPC");
		return objectFactory.createBuscarPosicionConsolidadaRequest(buscarPosicionConsolidadaRequest);
	}

	/**
	 * Method validaSaldoFavor
	 * Method objective: Formatear el salod; remover zeros a la izquierda, si se detecta el signo de menos al final de la cadena, se envia al principio .<br>
	 * @param saldo saldo que hay que removerle zeros y validar su signo 
	 * @return String valor formateado
	 */
	public String validaSaldoFavor(String saldo) {
		StringBuilder valorSalida = new StringBuilder();
		LOG.info("saldo_sin_formateo:"+saldo);
		saldo = saldo.replaceFirst(ConstastCommon.REGEX_ZERO_LEFT, ConstastCommon.EMPTY_STRING);
		valorSalida.append(saldo);
		//Validamos si existe el signo de menos
		if(saldo.indexOf(ConstastCommon.LESS_STRING)!=-1) {
			 valorSalida = new StringBuilder();
			//remplazamos el signo de menos que se encuentra del lado derecho
			 saldo = saldo.replaceAll(ConstastCommon.LESS_STRING, ConstastCommon.EMPTY_STRING);
			 //agregamos el signo de menos al lado izquierdo
			valorSalida.append(ConstastCommon.LESS_STRING);
			valorSalida.append(saldo);
		}
		LOG.info("saldo_formateado:"+valorSalida.toString());
		saldo = null;
		//regresamos el saldo formateado
		return valorSalida.toString();
	}
	/**
	 * Method addDecimalPoint
	 * Method objective: ___ .<br>
	 * @param value cadena valor que le agregaremos el punto decimal
	 * @param posicion posicion en la que se la pondremos
	 * @return valorSalida valor con el punto decimal agregado
	 */
	public String addDecimalPoint(String value,int posicion) {
		value = validaSaldoFavor(value);
		LOG.info("length:"+value.length());
		LOG.info("posicion:"+posicion);
		//Convertimos el string a caracter
		char[] charList= value.toCharArray();
		StringBuilder valorSalida = new StringBuilder();
		int x = 0;
		//indicamos en que posicion vamos a querer el punto decimal
		int aux = value.length() - posicion;
		//Recorremos el listado de caracteres
		for(char posicionLetra :charList) {
			//Los agregamos a un nuevo stringbuilder
			valorSalida.append(posicionLetra);
			if(x==aux) {
				//Cuando sea la posicion en la que hay que poner el punto decimal se agrega
				valorSalida.append(ConstastCommon.DOT);
			}
			x=x+1;
		}
		//Vaciamos las variables que no se utilizan
		charList = null;
		value = null;
		LOG.info("valorSalida:"+valorSalida);
		return valorSalida.toString();
	}
	
	/**
	 * Method saldoLinea
	 * Method objective: ___ .<br>
	 * @param limite limite de credito
	 * @param importeSaldo importe del saldo gastado
	 * @return result resultado de la resta(saldo en linea)
	 */
	public String saldoLinea(String limite,String importeSaldo) {
		LOG.debug("inicia.saldoLinea");
		String result = ConstastCommon.ZERO_STRING;
		//Inicializamos nuestros bigDeciaml
		BigDecimal limitBD = new BigDecimal(limite);
		BigDecimal importeSaldoBD = new BigDecimal(importeSaldo);
		
		//Retsamos los bigDecimal
		result = limitBD.subtract(importeSaldoBD).toString();
		LOG.info("result:"+result);
		LOG.debug("termina.saldoLinea");
		//Regresamos el resultado
		return result;
	}
}
