package mx.isban.neo.cc.utils;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.request.BenefitsReq;
import mx.isban.neo.cc.model.request.TrazaReq;

public final class UtilMappingSql {

	/**
	 * Variable para logeo en consola
	 */
	private static final Logger LOG = LoggerFactory.getLogger(UtilMappingSql.class);
	
	
	/**
	 * String LMWE : 
	 **/
	private static final String functionGetCatalog = "FNC_CONSULTA_MOTIVOS";
	
	private static final String functionNewMotivo = "FNC_GUARDA_MOTIVO";
	
	private static final String functionGetTraza = "FNC_CONSULTA_TRAZA";
	
	private static final String functionNewTraza = "FNC_GUARDA_TRAZA";
	
	private static final String functionUpdTraza = "FNC_ACTUALIZA_TRAZA";
	
	private static final String pkgGetTraza = "PKG_RET_TRAZAS";
	
	private static final String pkgGetCatalogo = "PKG_RET_MOTIVOS_CANCELA";
	
	private static final String functionNewBenefit = "FNC_GUARDA_BENEFICIO";
	
	private static final String functionUpdBenefit = "FNC_ACTUALIZA_BENEFICIO";
	
	private static final String functionGetBenefit = "FNC_CONSULTA_BENEFICIOS";
	
	private static final String functiondDeleteBenefit = "FNC_ELIMINA_BENEFICIO";
	
	private static final String functiongetMotivoCall = "FNC_RET_MOTIVOS_LLAMADA";
	
	private static final String pkgBenefits = "PKG_RET_TDC_BENEFICIOS";
	
	private static final String paramOut = "SYS_REFCURSOR";
	
	private static final String typecursor = "CURSOR";
	
	/**
	 *  LoginUtileria instance: intancia de la clase
	 */
	private static UtilMappingSql instance;
	
	/**
	 * Custom builder with the elements:
	 */
	private UtilMappingSql() {
		
	}
	
	/**
     * devuelve instancia de la clase en tipo singleton
     *
     * @return instance instancia de LoginUtileria;
     */
    public static UtilMappingSql getSingletonInstance() {
        if (null == instance) {
            instance = new UtilMappingSql();
        }
        return instance;
    }
	
	
		public static DmlBean mapingQuery(String funcName, String pa1, String pa2, String pa3) {
		
		
		DmlBean beanFill = new DmlBean();
		
		switch (funcName) {
		case "consulta_cat":
			beanFill.setFuncion(functionGetCatalog);
			beanFill.setPkg(pkgGetCatalogo);
			
			Map<String, String> paramsOut = new HashMap<>();
			paramsOut.put(paramOut,typecursor);
			beanFill.setParamsOut(paramsOut);
			beanFill.setParamsIn(null);
			break;
		case "new_motivo":
			beanFill.setFuncion(functionNewMotivo);
			beanFill.setPkg(pkgGetCatalogo);
			
			Map<String, String> paraOut = new HashMap<>();
			paraOut.put(paramOut,typecursor);
			beanFill.setParamsOut(paraOut);
				
			
			Map<String, String> paramsIn = new HashMap<>();
			paramsIn.put("status",pa1);
			paramsIn.put("prioridad",pa2);
			paramsIn.put("desc",pa3);
			beanFill.setParamsIn(paramsIn);
		
			break;
		case "consulta_traza":
			beanFill.setFuncion(functionGetTraza);
			beanFill.setPkg(pkgGetTraza);
			
			Map<String, String> parOut = new HashMap<>();
			parOut.put(paramOut, typecursor);
			beanFill.setParamsOut(parOut);
			
			Map<String, String> paramIn = new HashMap<>();
			paramIn.put("pa_cve_folio",pa1);
			paramIn.put("pa_fec_trn",pa2);
			beanFill.setParamsIn(paramIn);

			break;
		case "consulta_benefit":
			beanFill.setFuncion(functionGetBenefit);
			beanFill.setPkg(pkgBenefits);
			
			Map<String, String> paramouts = new HashMap<>();
				paramouts.put(paramOut, typecursor);
			beanFill.setParamsOut(paramouts);
			
			Map<String, String> paraIn = new HashMap<>();
				paraIn.put("pa_ide_producto",pa1);
				paraIn.put("pa_ide_sub_producto",pa2);
			beanFill.setParamsIn(paraIn);

			break;
			
		case "delete_benefit":
			beanFill.setFuncion(functiondDeleteBenefit);
			beanFill.setPkg(pkgBenefits);
			
			Map<String, String> pSalida = new HashMap<>();
				pSalida.put(paramOut, typecursor);
			beanFill.setParamsOut(pSalida);
			
			Map<String, String> paIn = new HashMap<>();
				paIn.put("PA_IDE_PRODUCTO",pa1);
				paIn.put("PA_IDE_SUB_PRODUCTO",pa2);
			beanFill.setParamsIn(paIn);

			break;
			
		case "new_folio":
			beanFill.setFuncion("");
			beanFill.setPkg("");
			
			Map<String, String> pOuts = new HashMap<>();
				pOuts.put(paramOut, typecursor);
			beanFill.setParamsOut(pOuts);
			
			Map<String, String> pIns = new HashMap<>();
				pIns.put("pa_cve_buc",pa1);
				pIns.put("pa_cve_ejecutivo",pa2);
				pIns.put("pa_desc_nombre_cliente",pa3);
			beanFill.setParamsIn(pIns);

			break;
			
		case "motivo_llamada":
			beanFill.setFuncion(functiongetMotivoCall);
			beanFill.setPkg(null);
			
			Map<String, String> parametroSal = new HashMap<>();
			parametroSal.put(paramOut,typecursor);
			beanFill.setParamsOut(parametroSal);
			
			Map<String, String> parametroEnt = new HashMap<>();
			parametroEnt.put("pa_ind_tipo_motivo",pa1);
			beanFill.setParamsIn(parametroEnt);

			
			break;
			
		default:
			break;

		}
		
		return beanFill;
		
	}
		
		public static DmlBean mapingQueryTraza(String function,TrazaReq bean) {
			
			DmlBean beanfill = new DmlBean();
			
			switch (function) {
			case "new_traza":
				
				beanfill.setFuncion(functionNewTraza);
				beanfill.setPkg(pkgGetTraza);
				
				Map<String, String> parOut = new HashMap<>();
				parOut.put(paramOut, typecursor);
				beanfill.setParamsOut(parOut);
				
				Map<String, String> paramSalida = new HashMap<>();
					paramSalida.put("IDE_CANAL",bean.getId_canal());
					paramSalida.put("IDE_MODULO",bean.getId_modulo());
					paramSalida.put("IDE_TIPO_OPERACION",bean.getId_tipo_operacion());
					paramSalida.put("CONN_ID",bean.getConn_id());
					paramSalida.put("TXT_IP_CLIENTE",bean.getIp_cliente());
					paramSalida.put("CVE_EJECUTIVO",bean.getCve_ejecutivo());
					paramSalida.put("CVE_USUARIO",bean.getCve_usuario());
					paramSalida.put("CVE_USR_LOCAL",bean.getCve_usr_local());
					paramSalida.put("IDE_STATUS",bean.getId_status());
					paramSalida.put("CVE_FOLIO",bean.getCve_folio());
					paramSalida.put("CVE_BUC",bean.getCve_buc());
				beanfill.setParamsIn(paramSalida);

				
				
				break;
				
			case "upd_traza":
				
				beanfill.setFuncion(functionUpdTraza);
				beanfill.setPkg(pkgGetTraza);
				
				Map<String, String> paramEntr = new HashMap<>();
				paramEntr.put(paramOut, typecursor);
				beanfill.setParamsOut(paramEntr);
				
				Map<String, String> prOut = new HashMap<>();
				prOut.put("IDE_TRAZA",bean.getId_traza());
				prOut.put("IDE_CANAL",bean.getId_canal());
				prOut.put("IDE_MODULO",bean.getId_modulo());
				prOut.put("IDE_TIPO_OPERACION",bean.getId_tipo_operacion());
				prOut.put("CONN_ID",bean.getConn_id());
				prOut.put("FEC_TRAN",bean.getFec_tran());
				prOut.put("TXT_IP_CLIENTE",bean.getIp_cliente());
				prOut.put("CVE_EJECUTIVO",bean.getCve_ejecutivo());
				prOut.put("CVE_USUARIO",bean.getCve_usuario());
				prOut.put("CVE_USR_LOCAL",bean.getCve_usr_local());
				prOut.put("IDE_STATUS",bean.getId_status());
				prOut.put("CVE_FOLIO",bean.getCve_folio());
				prOut.put("CVE_BUC",bean.getCve_buc());
				beanfill.setParamsIn(prOut);
				
				break;

			default:
				break;
			}
			
			return beanfill;
			
		}
	
		public static DmlBean mapingQueryBenef(String function,BenefitsReq bean) {
			DmlBean beanfill = new DmlBean();
			
			switch (function) {
			case "new_benefit":
				beanfill.setFuncion(functionNewBenefit);
				beanfill.setPkg(pkgBenefits);
				
				Map<String, String> pIn = new HashMap<>();
				pIn.put(paramOut, typecursor);
				beanfill.setParamsOut(pIn);
				
				Map<String, String> pas = new HashMap<>();
					pas.put("id_prod", bean.getId_producto());
					pas.put("id_subprod", bean.getId_subproducto());
					pas.put("desc_prod", bean.getDesc_producto());
					pas.put("des_prod_red", bean.getDesc_producto_red());
					pas.put("desc_benefit", bean.getDesc_beneficio());

				beanfill.setParamsIn(pas);
				
				
				
				break;
				
			case "upd_benefit":
				
				beanfill.setFuncion(functionUpdBenefit);
				beanfill.setPkg(pkgBenefits);
				
				Map<String, String> pOut = new HashMap<>();
				pOut.put(paramOut, typecursor);
				beanfill.setParamsOut(pOut);
				
				Map<String, String> paIn = new HashMap<>();
					paIn.put("id_prod", bean.getId_producto());
					paIn.put("id_subprod", bean.getId_subproducto());
					paIn.put("desc_prod", bean.getDesc_producto());
					paIn.put("des_prod_red", bean.getDesc_producto_red());
					paIn.put("desc_benefit", bean.getDesc_beneficio());

				beanfill.setParamsIn(paIn);
				
				break;

			default:
				break;
			}
			
			
			return beanfill;
			
			}
			
		
	
}
