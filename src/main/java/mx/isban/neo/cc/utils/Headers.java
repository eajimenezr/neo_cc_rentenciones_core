/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * Headers.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2019-10-25       Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc.utils;

/**
 * Type class: ____ .<br>
 * Main objective: ___ .<br>
 * Contains gets / sets methods:
 * <ul>
 * <li></li>
 * </ul>
 * Global constants or variables:
 * <ul>
 * <li></li>
 * </ul>
 * @author Isban / iscontreras
 * Headers.java by Z055749
 *
 */
public enum Headers {
	
	/**
	 * Headers AUTHORIZATION : 
	 **/
	AUTHORIZATION("Authorization"),
    /** 
     * X-Santander-Global-Id (mandatory) - Header for traceability, 
     * this value must be get from API - Global Security Identification. 
     */
     HEADER_SANTANDER_GLOBAL_ID ("X-Santander-Global-Id"),
    /** 
     * X-Santander-Client-Id - (mandatory) Consumer application identifier
     */
    HEADER_SANTANDER_CLIENT_ID ("X-Santander-Client-Id"),
    
    /**
     * Headers LOCALUSER : 
     **/
    LOCALUSER ("localUser");
    /**
     * String HEADER_SANTANDER_DATA_ID :  corresponde a la cookie 
     **/
    public static final String HEADER_SANTANDER_DATA_ID = "X-Santander-Data-Id";
	/**
	 * String name : 
	 **/
	private String name;

	/**
	 * Constructos to implement default: @param name
	 * Constructos to implement default: @param ordinal 
	 * Headers
	 */
	Headers(final String name) {
		this.name = name;
		//Contructor default 
	}

	/**
	 * Gets the value of String name 
	 * @return the value name
	 */
	public String getName() {
		return name;
	}

}
