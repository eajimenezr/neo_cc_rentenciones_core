package mx.isban.neo.cc.utils;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.request.FusionReq;
import mx.isban.neo.cc.model.request.UpdArgReq;

public final class UtilMappingArgmsSql {

	/**
	 * Variable para logeo en consola
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(UtilMappingArgmsSql.class);
	
	
	/**
	 * String LMWE : 
	 **/
	private static final String functionGetArguments = "FNC_RET_VALIDA_ARGU";
	
	private static final String functionUpdArguments = "FNC_RET_ACTUALIZA_STATARGU";
	
	private static final String functionFusionLine = "FNC_RET_FUSIONLINEAS";
	
	private static final String functionAdcArguments = "FNC_RET_GUARDA_ADICIONALES";
	
	private static final String functionConfirmRtdc = "FNC_RET_CONFIRMA_RETEN";
	
	private static final String functionGetCatalogos = "FNC_RET_CONSULTA_CAT";
	
	private static final String functionGetQuery = "FNC_CONSULTA_PROCESO";
	
	private static final String functionGetTrackingArg = "FNC_CONSULTA_TRACKING";

	private static final String functionSaveComment = "FNC_GUARDA_COMENTARIO";
	
	private static final String pkgGetTracing = "PKG_RET_CONSULTAS";
	
	private static final String pkgGetTrackingArg = "PKG_RET_SEGUIMIENTO_ARGU";
	
	private static final String paramOut = "SYS_REFCURSOR";
	
	private static final String typecursor = "CURSOR";
	
	/**
	 *  LoginUtileria instance: intancia de la clase
	 */
	private static UtilMappingArgmsSql instance;
	
	/**
	 * Custom builder with the elements:
	 */
	private UtilMappingArgmsSql() {
		
	}
	
	/**
     * devuelve instancia de la clase en tipo singleton
     *
     * @return instance instancia de LoginUtileria;
     */
    public static UtilMappingArgmsSql getSingletonInstance() {
        if (null == instance) {
            instance = new UtilMappingArgmsSql();
        }
        return instance;
    }
	
	
		public static DmlBean mapingQuery(String funcName, String pa1, String pa2, String pa3, String pa4,String pa5,String pa6) {
		
		
		DmlBean beanFill = new DmlBean();
		
		switch (funcName) {
		case "consulta_Arguments":
			beanFill.setFuncion(functionGetArguments);
			beanFill.setPkg(null);
			
			Map<String, String> paraOut = new HashMap<>();
			paraOut.put(paramOut,typecursor);
			beanFill.setParamsOut(paraOut);
			
			
			Map<String, String> paramsIn = new HashMap<>();
			paramsIn.put("pa_cve_folio",pa1);
			paramsIn.put("pa_num_Tarjeta",pa2);
			paramsIn.put("pa_ide_argumento",pa3);
			paramsIn.put("pa_iteracion",pa4);
			paramsIn.put("pa_resultadoValidacion",pa5);
			paramsIn.put("pa_ides_motivosCanelacion",pa6);
			beanFill.setParamsIn(paramsIn);
			
			
			break;
		
		case "consulta_catalogo":
			beanFill.setFuncion(functionGetCatalogos);
			beanFill.setPkg(null);
			
			Map<String, String> p1Out = new HashMap<>();
			p1Out.put(paramOut,typecursor);
			beanFill.setParamsOut(p1Out);
			
			
			Map<String, String> pa1In = new HashMap<>();
			pa1In.put("pa_idcatalogo",pa1);
			pa1In.put("pa_subitemcat",pa2);
			beanFill.setParamsIn(pa1In);
			
			break;
			
		case "consulta_tracing":
			beanFill.setFuncion(functionGetQuery);
			beanFill.setPkg(pkgGetTracing);
			
			Map<String, String> par1Out = new HashMap<>();
			par1Out.put(paramOut,typecursor);
			beanFill.setParamsOut(par1Out);
			
			
			Map<String, String> pasIn = new HashMap<>();
			pasIn.put("pa_cve_folio",pa1);
			pasIn.put("pa_num_Tarjeta",pa2.replace(" ",""));
			beanFill.setParamsIn(pasIn);
			
			break;
			
		case "consulta_trkArgument":
			beanFill.setFuncion(functionGetTrackingArg);
			beanFill.setPkg(pkgGetTrackingArg);
			
			Map<String, String> outPa = new HashMap<>();
			outPa.put(paramOut,typecursor);
			beanFill.setParamsOut(outPa);
			
			
			Map<String, String> inPa = new HashMap<>();
			inPa.put("pa_cve_folio",pa1);
			inPa.put("pa_num_Tarjeta",pa2.replace(" ",""));
			beanFill.setParamsIn(inPa);
			
			break;
		
			
			default:
				break;
			}	
			
			return beanFill;
			
		}
		
		public static DmlBean mapingQueryObj(String funcName, UpdArgReq request) {
			DmlBean beanFill = new DmlBean();

		
			switch (funcName) {
			case "upd_argument":
				beanFill.setFuncion(functionUpdArguments);
				beanFill.setPkg(null);
				
				Map<String, String> paraOut = new HashMap<>();
				paraOut.put(paramOut,typecursor);
				beanFill.setParamsOut(paraOut);
				
				
				Map<String, String> paramsIn = new HashMap<>();
				paramsIn.put("pa_cve_folio",request.getCve_folio());
				paramsIn.put("pa_num_Tarjeta",request.getNum_tarjeta().replace(" ",""));
				paramsIn.put("pa_ide_argumento",request.getIde_argumento());
				paramsIn.put("pa_status_argumento",request.getStatus_argumento());
				paramsIn.put("pa_ides_motivosCanelacion",request.getIds_motivosCancel());
				beanFill.setParamsIn(paramsIn);
				
				
				break;
			case "adicional_argument":
				beanFill.setFuncion(functionAdcArguments);
				beanFill.setPkg(null);
				
				Map<String, String> parOut = new HashMap<>();
				parOut.put(paramOut,typecursor);
				beanFill.setParamsOut(parOut);
				
				
				Map<String, String> paraIn = new HashMap<>();
				paraIn.put("pa_cve_folio",request.getCve_folio());
				paraIn.put("pa_num_Tarjeta",request.getNum_tarjeta().replace(" ",""));
				paraIn.put("pa_ide_argumento",request.getIde_argumento());
				paraIn.put("pa_captura_adicional",request.getCaptura_adicional());
				paraIn.put("pa_folio_adicional",request.getFolio_adicional());
				beanFill.setParamsIn(paraIn);
				
				break;
				
			case "confirma_rtdc":
				beanFill.setFuncion(functionConfirmRtdc);
				beanFill.setPkg(null);
				
				Map<String, String> pOut = new HashMap<>();
				pOut.put(paramOut,typecursor);
				beanFill.setParamsOut(pOut);
				
				
				Map<String, String> pIn = new HashMap<>();
				pIn.put("pa_cve_folio",request.getCve_folio());
				pIn.put("pa_num_Tarjeta",request.getNum_tarjeta().replace(" ",""));
				pIn.put("pa_statusRetencion",request.getStatusRetencion());	
				beanFill.setParamsIn(pIn);
				
				break;
				
				
				
				default:
					break;
				}
		
		
			return beanFill;
			
		}

		public static DmlBean mapingQueryObj(String funcName, FusionReq request) {
			DmlBean beanFill = new DmlBean();

		
			switch (funcName) {
			
			case "fusion_lineas":
				beanFill.setFuncion(functionFusionLine);
				beanFill.setPkg(null);
				
				Map<String, String> paraOut = new HashMap<>();
				paraOut.put(paramOut,typecursor);
				beanFill.setParamsOut(paraOut);
				
				
				Map<String, String> paramsIn = new HashMap<>();
				paramsIn.put("pa_cve_folio",request.getCve_folio());
				paramsIn.put("pa_num_Tarjeta",request.getNum_tarjeta().replace(" ",""));
				paramsIn.put("pa_idValidacion",request.getIdValidacion());
				paramsIn.put("pa_ide_respuesta",request.getIde_respuesta());
				beanFill.setParamsIn(paramsIn);
				
				
				break;
	
			default:
				break;
			}
	
	
		return beanFill;
		
	}

		public static DmlBean mapingQueryObjGeneric(String funcName, String pa1,String pa2,String pa3,String pa4,String pa5, String pa6) {
			DmlBean beanFill = new DmlBean();
			
			switch (funcName) {
			case "guarda_coment":
				
				beanFill.setFuncion(functionSaveComment);
				beanFill.setPkg(pkgGetTrackingArg);

				Map<String, String> par1Out = new HashMap<>();
				par1Out.put(paramOut,typecursor);
				beanFill.setParamsOut(par1Out);
				
				Map<String, String> pasIn = new HashMap<>();
				pasIn.put("pa_cve_folio",pa1);
				pasIn.put("pa_num_Tarjeta",pa2.replace(" ",""));
				pasIn.put("pa_txt_comentario",pa3);
				pasIn.put("pa_ide_argumento",pa4);		
				beanFill.setParamsIn(pasIn);
				
				break;
			default:
				break;
			}
			
			
			return beanFill;
			
		}
		
}
