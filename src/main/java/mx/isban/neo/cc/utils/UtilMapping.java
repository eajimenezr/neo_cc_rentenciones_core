package mx.isban.neo.cc.utils;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.isban.neo.cc.model.beans.DmlBean;

import mx.isban.neo.cc.model.request.GuardaTdcReq;
import mx.isban.neo.cc.model.request.MotivosTdcReq;



public final class UtilMapping {

	/**
	 * Variable para logeo en consola
	 */
	private static final Logger LOG = LoggerFactory.getLogger(UtilMapping.class);
	
	
	/**
	 * String LMWE : 
	 **/
	private static final String functionMotivoToTdc = "FNC_RET_AGREGA_MOTIVOS";
	
	private static final String functionSaveTDCtoFolio = "FNC_RET_GUARDA_TDC";
	
	private static final String functionGetScript = "FNC_RET_CONSULTA_SCRIPT";
	
	private static final String pkgGetTraza = "PKG_RET_TRAZAS";
			
	private static final String paramOut = "SYS_REFCURSOR";
	
	private static final String typecursor = "CURSOR";
	
	
	/**
	 *  LoginUtileria instance: intancia de la clase
	 */
	private static UtilMapping instance;
	
	/**
	 * Custom builder with the elements:
	 */
	private UtilMapping() {
		
	}
	
	/**
     * devuelve instancia de la clase en tipo singleton
     *
     * @return instance instancia de LoginUtileria;
     */
    public static UtilMapping getSingletonInstance() {
        if (null == instance) {
            instance = new UtilMapping();
        }
        return instance;
    }
	
	
    public static DmlBean mapingQuery(String consulta, GuardaTdcReq req) {
		
		
		DmlBean beanFill = new DmlBean();
		
		switch (consulta) {
		case "guardaTdc":
			beanFill.setFuncion(functionSaveTDCtoFolio);
			beanFill.setPkg("");
			
			Map<String, String> paramsOut = new HashMap<>();
			paramsOut.put(paramOut,typecursor);
			
			Map<String, String> paramsIn = new HashMap<>();
			paramsIn.put("pa_cve_folio", req.getCve_folio());
			paramsIn.put("pa_num_tarjeta", req.getNum_tarjeta());
			paramsIn.put("pa_imp_saldo",  String.valueOf(req.getImp_saldo())); 
			paramsIn.put("pa_imp_limite_credito", String.valueOf(req.getImp_lim_credito()));
			paramsIn.put("pa_fec_vigencia", req.getFec_vigencia());
			paramsIn.put("pa_fec_antiguedad", req.getFec_antiguedad());
			paramsIn.put("pa_ind_status_tarjeta", req.getInd_status_tarjeta());
			paramsIn.put("pa_ind_tipo_tarjeta", req.getInd_tipo_tarjeta());
			paramsIn.put("pa_ide_producto", req.getIde_producto());
			paramsIn.put("pa_ide_sub_producto", req.getIde_sub_producto());
			paramsIn.put("pa_ide_codigo_bloqueo", req.getIde_codigo_bloqueo());
			paramsIn.put("pa_ide_motLlamada", req.getIde_mot_llamada());
			paramsIn.put("pa_ind_status_retencion", req.getInd_status_retencion());
			paramsIn.put("pa_mto_recibido", String.valueOf(req.getMto_recibido()));
			paramsIn.put("pa_fecha_alta_TDC", req.getFecha_alta_TDC());
			paramsIn.put("pa_fecha_limite_pago", req.getFecha_limite_pago());
			paramsIn.put("pa_numero_contrato", req.getNumero_contrato());
			paramsIn.put("pa_num_tarjeta_origen", req.getNum_tarjeta_origen());
			
			
			
			beanFill.setParamsIn(paramsIn);
			
			break;

		}
		
		return beanFill;
		
	}
		
		public static DmlBean mapingQueryMotivoTdc(String function,MotivosTdcReq bean) {
			
			DmlBean beanfill = new DmlBean();
			
			switch (function) {
			case "motivoTotdc":
				
				beanfill.setFuncion(functionMotivoToTdc);
				beanfill.setPkg("");
				
				Map<String, String> parOut = new HashMap<>();
				parOut.put(paramOut, typecursor);
				beanfill.setParamsOut(parOut);
				
				Map<String, String> paraIn = new HashMap<>();
					paraIn.put("pa_cve_folio",bean.getFolio_rtdc());
					paraIn.put("pa_num_tarjeta",bean.getNum_tdc());
					paraIn.put("pa_idsMotivos",bean.getIds_motivos());
					paraIn.put("pa_accion",bean.getAccion());
					
				beanfill.setParamsIn(paraIn);

				
				
				break;

			default:
				break;
			}
			
			return beanfill;
			
		}
		
		public static DmlBean mapingQueryScripts(String function,String pa1,String pa2,String pa3,String pa4, String pa5,String pa6) {
			
			DmlBean beanfill = new DmlBean();
			
			switch (function) {
			case "consultaScript":
				
				beanfill.setFuncion(functionGetScript);
				beanfill.setPkg("");
				
				Map<String, String> parOut = new HashMap<>();
				parOut.put(paramOut, typecursor);
				beanfill.setParamsOut(parOut);
				
				Map<String, String> paraIn = new HashMap<>();
					paraIn.put("cve_folio",pa1);
					paraIn.put("cve_Ejecutivo",pa2);
					paraIn.put("nombre_Cliente",pa3);
					paraIn.put("num_Tarjeta",pa4.replace(" ",""));
					paraIn.put("vigencia_Tarjeta",pa5);
					paraIn.put("script",pa6);
					
				beanfill.setParamsIn(paraIn);

				
				
				break;

			default:
				break;
			}
			
			return beanfill;
			
		}

			
		
	
}
