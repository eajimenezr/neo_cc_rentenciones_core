/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * MappingUtilTnnc.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2019-03-28                               Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.IllegalFormatConversionException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import mx.isban.neo.cc.excepcion.ValidationParamsException;
import mx.isban.neo.cc.excepcion.ValidationParamsFault;
import mx.isban.neo.cc.model.beans.cuenta.vista.CuentaVistaRq;
import mx.isban.neo.cc.model.request.JsonRequestMpa;
import mx.isban.neo.cc.model.request.Mpa2Rq;
import mx.isban.neo.cc.tenencias.model.DatosLmwe;
import mx.isban.neo.cc.tenencias.model.DatosLmweRs;
import mx.isban.neo.cc.tenencias.model.SimpleDataLmwe;
import mx.isban.neo.cc.tenencias.tarjetas.beans.card.rq.CardRq;
import mx.isban.neo.cc.tenencias.tarjetas.beans.typecard.TypesCards;
import mx.isban.neo.cc.tenencias.trx.beans.lmwe.rq.JsonRequest;
import mx.isban.neo.cc.tenencias.trx.beans.lmwe.rq.LmweRq;

import mx.isban.neo.cc.tenencias.trx.beans.lmwe.rs.TrxRs;


/**
 * Type class: Utilitaria .<br>
 * Main objective: Contener ciertos metodos que pueden llegar a ser reutilizados a lo largo de uno o varios flujos, estos metodos estan bajo singleton para ser instanciados unicamente una vez cuando se llaman .<br>
 * Contains methods:
 * <ul>
 * <li>mappingValidacionBls</li>
 * <li>mappingCardRq</li>
 * <li>mappingLmweRq</li>
 * <li>mappingStringToTrxRs</li>
 * <li>mappingDataLmweRs</li>
 * <li>mappingSimpleDataLmwe</li>
 * </ul>
 * Global constants or variables:
 * <ul>
 * <li>LMWE</li>
 * </ul>
 * @author Isban / iscontreras
 * MappingUtilTnnc.java by Z055749
 *
 */
public final class MappingUtilTnnc {

	/**
	 * Variable para logeo en consola
	 */
	private static final Logger LOG = LoggerFactory.getLogger(MappingUtilTnnc.class);
	
	/**
	 * String LMWE : 
	 **/
	private static final String LMWE = "LMWE";
	/**
	 *  LoginUtileria instance: intancia de la clase
	 */
	private static MappingUtilTnnc instance;
	/**
	 * Custom builder with the elements:
	 */
	private MappingUtilTnnc() {
		
	}
	
	/**
     * devuelve instancia de la clase en tipo singleton
     *
     * @return instance instancia de LoginUtileria;
     */
    public static MappingUtilTnnc getSingletonInstance() {
        if (null == instance) {
            instance = new MappingUtilTnnc();
        }
        return instance;
    }

//    /**
//     * Methos mappingValidacionBls: return type CuentaVistaRq 
//     * @param numClie numero de cliente
//     * @param request mapa de entrada con la lista de productos
//     * @return  CuentaVistaRq con el mapa completo 
//     * @throws ValidationParamsException validacion de tipo excepcion si el numero de cliente viene vacio o es null
//     */
//    public CuentaVistaRq mappingValidacionBls(String numClie,CuentaVistaRq request) throws ValidationParamsException {
//    	LOG.info("INICIA mappingValidacionBls");
//    	if(!StringUtils.isNotBlank(numClie)) {
//    		LOG.info("ERROR en mappingValidacionBls Fallo el mapeo de numClie");
//    		/**ejecutamos excepcion de parametros**/
//			 throw new ValidationParamsException("Fallo el mapeo de numClie", new ValidationParamsFault("Fallo el mapeo de numClie",ConstastCommon.NULL_OR_EMPTY_MSG+" numClie"));
//		}
//    	/**Validamos el tipo de dato que sea numerico**/
//    	UtilsCommon.utilValidation(numClie);
//    	
//    	 request.getCuentaVista().getCliente().setNumClie(numClie);
//     	LOG.info("TERMINA mappingValidacionBls");
//    	return request;
//    }
    
    
    /**
     * Method mappingLmweRq
     * Method objective: ___ .<br>
     * @param numClie numero de cliente que se mapea para la busqueda
     * @return LmweRq mapa de datos para la trx LMWE
     * @throws ValidationParamsException excepcion de tipo validacion 
     */
    public LmweRq mappingLmweRq(String numClie) throws ValidationParamsException {
    	//Inicializamos nuestros objetos
    	LmweRq resquest = new LmweRq();
    	String numClienteFormat = numClie.trim();
    	int buc = Integer.parseInt(numClienteFormat);
    	//Validamos que sea numerico 
    	UtilsCommon.utilValidation(numClie);
    	JsonRequest jsonRq = new JsonRequest();
    	Formatter fmt = null;
    	try {
    	 fmt = new Formatter();
    	numClienteFormat = fmt.format("%08d",buc).toString();
    	
    	jsonRq.setBuc(numClienteFormat);
    	
    	}catch (IllegalFormatConversionException e) {
    		LOG.error("Formatter.IllegalFormatConversionException",e);
    		jsonRq.setBuc(numClienteFormat);
		}finally {
			if(fmt!=null) {
				fmt.close();
		    	fmt = null;
			}
		}
    	//seteamos los valores si pasa la validacion
    	resquest.getLmwe().setJsonRequest(Utils.getSingletonInstance().objectToJsonString(jsonRq));
    	resquest.getLmwe().setIdTrx(LMWE);
    	resquest.setNumClie(numClie);
    	//regresamos ,los datos
    	return resquest;
    }
    
    /**
     * Method mappingStringToTrxRs
     * Method objective: ___ .<br>
     * @param json json en formato string
     * @return TrxRs json mapeado al objeto
     */
    public TrxRs mappingStringToTrxRs(String json) {
    	ObjectMapper mapper = new ObjectMapper();
    	TrxRs trxRs = null;
    	mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
    	try {
    		trxRs = new TrxRs();
    		trxRs =	mapper.reader().forType(TrxRs.class).readValue(json);
		} catch (IOException e) {
			/** Excepcion de tipo: e **/
		LOG.error("mappingStringToTrxRs.IOException",e);
		}
    	return trxRs;
    }
    
   
	/**
	 * Method mappingDataLmweRs
	 * Method objective: ___ .<br>
	 * @param trxRs  datos que regresa la transaccion
	 * @return DatosLmweRs mapa de datos de la trx ya formateado
	 */
	public  DatosLmweRs mappingDataLmweRs (TrxRs trxRs) {
		LOG.debug("inicia.mappingDataLmweRs");
		//Inicializamios nuestros objetos
		DatosLmweRs datosLmweRs = new DatosLmweRs();
		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
		try {
			//realizamos el cast del json
			datosLmweRs = mapper.reader().forType(DatosLmweRs.class).readValue(trxRs.getResultado().getResponseJSON().toString());
		} catch (JsonProcessingException e) {
			/** Excepcion de tipo: e JsonProcessingException **/
			LOG.error("mappingDataLmweRs.JsonProcessingException",e);
			//Si se detecta un error lo mandamos castear en formato simple
			datosLmweRs = new DatosLmweRs();
			
			List<DatosLmwe> datosLMWE = new ArrayList<>();
			SimpleDataLmwe simpleDataLmwe = mappingSimpleDataLmwe( trxRs);
			if(simpleDataLmwe!=null) {
				datosLMWE.add(simpleDataLmwe.getDatosLMWE());
				datosLmweRs.setDatosLMWE(datosLMWE);
			}
		} catch (IOException e) {
			/** Excepcion de tipo: e IOException **/
			LOG.error("mappingDataLmweRs.IOException",e);
		}
		//se manda a avalidar que no existan caracteres en los importes de limite de credito y saldo
		datosLmweRs=this.validateData(datosLmweRs);
		
		LOG.debug("termina.mappingDataLmweRs");
		//Regresamos los datos
		return datosLmweRs;
	}
	
	/**
	 * Method mappingSimpleDataLmwe
	 * Method objective: ___ .<br>
	 * @param trxRs datos que regresa la transaccion
	 * @return SimpleDataLmwe formato simple del objeto de la transaccion
	 */
	public SimpleDataLmwe mappingSimpleDataLmwe(TrxRs trxRs) {
		LOG.debug("inicia.mappingSimpleDataLmwe");
		//Inicializamios nuestros objetos
		SimpleDataLmwe simpleDataLmwe = null;
		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);		
		try {
			//realizamos el cast del json
			simpleDataLmwe = new SimpleDataLmwe();
			simpleDataLmwe = mapper.reader().forType(SimpleDataLmwe.class).readValue(trxRs.getResultado().getResponseJSON().toString());
		} catch (JsonProcessingException e) {
			/** Excepcion de tipo: e JsonProcessingException **/
			LOG.error("mappingSimpleDataLmwe.JsonProcessingException",e);
		} catch (IOException e) {
			/** Excepcion de tipo: e IOException **/
			LOG.error("mappingSimpleDataLmwe.IOException",e);
		}
		LOG.debug("termina.mappingSimpleDataLmwe");
		//Regresamos los datos
		return simpleDataLmwe;
	}
	
	
	/**
	 * Method mappingSimpleDataLmwe
	 * Method objective: ___ .<br>
	 * @param trxRs datos que regresa la transaccion
	 * @return SimpleDataLmwe formato simple del objeto de la transaccion
	 */
	public DatosLmweRs validateData(DatosLmweRs data) {
		LOG.debug("inicia.validar limiteCredito-ImporteSaldo");
		
		// se valida que no tengan un caracter "-" los importes
		for (int x = 0; x < data.getDatosLMWE().size(); x++) {
				//eliminar signo menos a cadena
				String importeOrg = data.getDatosLMWE().get(x).getImporteSaldo();
				String limiteOrg = data.getDatosLMWE().get(x).getLimiteCredito();
				
				data.getDatosLMWE().get(x).setImporteSaldo(addDecimalPoint(importeOrg, 3));
				data.getDatosLMWE().get(x).setLimiteCredito(addDecimalPoint(limiteOrg, 3));
		
		}

		return data;
	}

	/**
	 * Method validaSaldoFavor
	 * Method objective: Formatear el salod; remover zeros a la izquierda, si se detecta el signo de menos al final de la cadena, se envia al principio .<br>
	 * @param saldo saldo que hay que removerle zeros y validar su signo 
	 * @return String valor formateado
	 */
	public String validaSaldoFavor(String saldo) {
		StringBuilder valorSalida = new StringBuilder();
		LOG.info("saldo_sin_formateo:"+saldo);
		saldo = saldo.replaceFirst(ConstastCommon.REGEX_ZERO_LEFT, ConstastCommon.EMPTY_STRING);
		valorSalida.append(saldo);
		//Validamos si existe el signo de menos
		if(saldo.indexOf(ConstastCommon.LESS_STRING)!=-1) {
			 valorSalida = new StringBuilder();
			//remplazamos el signo de menos que se encuentra del lado derecho
			 saldo = saldo.replaceAll(ConstastCommon.LESS_STRING, ConstastCommon.EMPTY_STRING);
			 //agregamos el signo de menos al lado izquierdo
			valorSalida.append(ConstastCommon.LESS_STRING);
			valorSalida.append(saldo);
		}
		LOG.info("saldo_formateado:"+valorSalida.toString());
	
		saldo = null;
		//regresamos el saldo formateado
		return valorSalida.toString();
	}
	/**
	 * Method addDecimalPoint
	 * Method objective: ___ .<br>
	 * @param value cadena valor que le agregaremos el punto decimal
	 * @param posicion posicion en la que se la pondremos
	 * @return valorSalida valor con el punto decimal agregado
	 */
	public String addDecimalPoint(String value,int posicion) {
		value = validaSaldoFavor(value);
		
		if (value.length() ==2) {
			value= "0"+value;
			
		}
		LOG.info("length:"+value.length());
		LOG.info("posicion:"+posicion);
		//Convertimos el string a caracter
		char[] charList= value.toCharArray();
		StringBuilder valorSalida = new StringBuilder();
		int x = 0;
		//indicamos en que posicion vamos a querer el punto decimal
		int aux = value.length() - posicion;
		//Recorremos el listado de caracteres
		for(char posicionLetra :charList) {
			//Los agregamos a un nuevo stringbuilder
			valorSalida.append(posicionLetra);
			if(x==aux) {
				//Cuando sea la posicion en la que hay que poner el punto decimal se agrega
				valorSalida.append(ConstastCommon.DOT);
			}
			x=x+1;
		}
		//Vaciamos las variables que no se utilizan
		charList = null;
		value = null;
		LOG.info("valorSalida:"+valorSalida);
		return valorSalida.toString();
	}
	
    /**
     * Method mappingLmweRq
     * Method objective: ___ .<br>
     * @param numClie numero de cliente que se mapea para la busqueda
     * @return LmweRq mapa de datos para la trx LMWE
     * @throws ValidationParamsException excepcion de tipo validacion 
     */
    public Mpa2Rq mappingMpa2Rq(String pan,String code){
    	//Inicializamos nuestros objetos
//    	LmweRq resquest = new LmweRq();
    	Mpa2Rq rquest = new Mpa2Rq(); 
    	String panFormat = pan.trim();
    	try {
			UtilsCommon.utilValidation(code);
		} catch (ValidationParamsException e) {
			LOG.error("busquedaTenencia.ValidationParamsException",e);
		}

    	
    	JsonRequestMpa jsonrq = new JsonRequestMpa();
    	
    	jsonrq.setPan(panFormat);
    	jsonrq.setTextoBloqueo("NeoContactCenter");
    	jsonrq.setCodigoBloqueo(Integer.parseInt(code));
    	
    	
    	rquest.getMpa2().setJsonRequest(Utils.getSingletonInstance().objectToJsonString(jsonrq));
    	rquest.getMpa2().setIdTrx("MPA2");
    	rquest.setNumTdc(panFormat);

    	return rquest;
    }
	
}
