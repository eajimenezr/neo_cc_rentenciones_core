package mx.isban.neo.cc.utils;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.request.CancelRq;
import mx.isban.neo.cc.model.request.CartaCancelReq;
import mx.isban.neo.cc.model.request.CelEmailReq;

public final class UtilMappingSqlRe {

	/**
	 * Variable para logeo en consola
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(UtilMappingSqlRe.class);
	
	
	/**
	 * String LMWE : 
	 **/
	private static final String functionTdcCodes = "FNC_RET_ACTUALIZA_CODIGO";
	
	private static final String functionGetLimite = "FNC_RET_LIMITECREDITO";
	
	private static final String functionSaveCelEmail = "FNC_RET_GUARDA_CELMAIL";
	
	private static final String functionGetPuntos = "FNC_RET_PUNTOSRECOMPENSA";
	
	private static final String functionRetCancel = "FNC_RET_CANCELACIONES";
		
	private static final String functionGetAdicionales = "FNC_RET_TDC_ADICIONAL";
		
	private static final String paramOut = "SYS_REFCURSOR";
	
	private static final String typecursor = "CURSOR";
	
	/**
	 *  LoginUtileria instance: intancia de la clase
	 */
	private static UtilMappingSqlRe instance;
	
	/**
	 * Custom builder with the elements:
	 */
	private UtilMappingSqlRe() {
		
	}
	
	/**
     * devuelve instancia de la clase en tipo singleton
     *
     * @return instance instancia de LoginUtileria;
     */
    public static UtilMappingSqlRe getSingletonInstance() {
        if (null == instance) {
            instance = new UtilMappingSqlRe();
        }
        return instance;
    }
	
	
		public static DmlBean mapingQuery(String funcName, String pa1, String pa2, String pa3, String pa4,String pa5) {
		
		
		DmlBean beanFill = new DmlBean();
		
		switch (funcName) {
		case "TDC_codes":
			beanFill.setFuncion(functionTdcCodes);
			beanFill.setPkg(null);
			
			Map<String, String> paraOut = new HashMap<>();
			paraOut.put(paramOut,typecursor);
			beanFill.setParamsOut(paraOut);
			
			
			Map<String, String> paramsIn = new HashMap<>();
			paramsIn.put("pa_ide_tarjeta",pa1);
			paramsIn.put("pa_accion",pa2);

			beanFill.setParamsIn(paramsIn);
			
			
			break;
		case "consulta_limite":
			beanFill.setFuncion(functionGetLimite);
			beanFill.setPkg(null);
			
			Map<String, String> parOut = new HashMap<>();
			parOut.put(paramOut,typecursor);
			beanFill.setParamsOut(parOut);
			
			
			Map<String, String> parIn = new HashMap<>();
			parIn.put("pa_num_tarjeta",pa1);
			beanFill.setParamsIn(parIn);
			
			
			break;
			
		case "consulta_puntos":
			beanFill.setFuncion(functionGetPuntos);
			beanFill.setPkg(null);
			
			Map<String, String> paOut = new HashMap<>();
			paOut.put(paramOut,typecursor);
			beanFill.setParamsOut(paOut);
			
			
			Map<String, String> paIn = new HashMap<>();
			paIn.put("pa_num_tarjeta",pa1);
			beanFill.setParamsIn(paIn);
			
			
			break;
			
		case "consulta_adicionales":
			beanFill.setFuncion(functionGetAdicionales);
			beanFill.setPkg(null);
			
			Map<String, String> pramOut = new HashMap<>();
			pramOut.put(paramOut,typecursor);
			beanFill.setParamsOut(pramOut);
			
			Map<String, String> pIn = new HashMap<>();
			pIn.put("pa_cve_folio",pa1);
			pIn.put("pa_num_tarjeta",pa2.replace(" ",""));
			pIn.put("pa_accion",pa3);
			pIn.put("pa_codigoAplicado",pa4);
			
			beanFill.setParamsIn(pIn);
			
			
			break;
		
				
			
			default:
				break;
			}
			
			return beanFill;
			
		}

		
		public static DmlBean mapingQueryObj(String funcName, CancelRq request) {
			DmlBean beanFill = new DmlBean();

		
			switch (funcName) {
			case "valida_cancel":
				beanFill.setFuncion(functionRetCancel);
				beanFill.setPkg(null);
				
				Map<String, String> paraOut = new HashMap<>();
				paraOut.put(paramOut,typecursor);
				beanFill.setParamsOut(paraOut);
				
				
				Map<String, String> paramsIn = new HashMap<>();
				paramsIn.put("pa_cve_folio",request.getCve_folio());
				paramsIn.put("pa_num_tarjeta",request.getNum_tdc().replace(" ",""));
				paramsIn.put("pa_idValidacion",request.getId_validacion());
				paramsIn.put("pa_ide_respuesta",request.getId_respuesta());
				paramsIn.put("pa_txt_folioCapturado",request.getTxt_folioCapturado());
				paramsIn.put("pa_txt_folioCapturado2",request.getTxt_folioCapturado2());
				paramsIn.put("pa_tipoOperacion",request.getTipoOperacion());
				
				beanFill.setParamsIn(paramsIn);
				
				
				break;
				
				
				default:
					break;
				}
		
		
			return beanFill;
			
		}
		
		public static DmlBean mapingQueryObjCel(String funcName, CelEmailReq request) {
			DmlBean beanFill = new DmlBean();

		
			switch (funcName) {
			case "guarda_cel":
				beanFill.setFuncion(functionSaveCelEmail);
				beanFill.setPkg(null);
				
				Map<String, String> paraOut = new HashMap<>();
				paraOut.put(paramOut,typecursor);
				beanFill.setParamsOut(paraOut);
				
				
				Map<String, String> paramsIn = new HashMap<>();
				paramsIn.put("pa_cve_folio",request.getCve_folio());
				paramsIn.put("pa_num_tarjeta",request.getNum_tdc().replace(" ",""));
				paramsIn.put("pa_cel",request.getCel());
				paramsIn.put("pa_mail",request.getEmail());
	
				beanFill.setParamsIn(paramsIn);
				
				
				break;
				
				
				default:
					break;
				}
		
		
			return beanFill;
			
		}
		
	
}
