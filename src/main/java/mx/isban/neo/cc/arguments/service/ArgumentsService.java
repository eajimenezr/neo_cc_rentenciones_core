package mx.isban.neo.cc.arguments.service;

import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.isban.neo.cc.builder.dml.service.IBuildDMLService;
import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.FusionResp;
import mx.isban.neo.cc.model.response.MessageResponse;
import mx.isban.neo.cc.model.response.Respuestas;
import mx.isban.neo.cc.model.response.SeguimientoResp;
import mx.isban.neo.cc.model.response.TrackingArguResp;
import mx.isban.neo.cc.model.response.updArgResp;

@Service
public class ArgumentsService implements IArgumentsService {

	
	@Autowired
	public IBuildDMLService dmlService;
	
	@Override
	public MessageResponse[] coredmlArguments(DmlBean bean) throws SQLException {
		return dmlService.buildDmlInArgs(bean);
	}
	
	@Override
	public SeguimientoResp[] coredmlTracing(DmlBean bean) throws SQLException {
		return dmlService.buildDmlSeguimiento(bean);
	}
	
	@Override
	public updArgResp[] coredmlUpdArguments(DmlBean bean) throws SQLException {
		return dmlService.buildDmlUpd(bean);
	}

	@Override
	public FusionResp[] coredmlUpdArgumFusion(DmlBean bean) throws SQLException {
		return dmlService.buildDmlFusion(bean);
	}

	@Override
	public TrackingArguResp[] coredmlTrackArguments(DmlBean bean) throws SQLException {
		return dmlService.buildDmlSeguimientoArgum(bean);
	}

	@Override
	public FusionResp[] validaResponse(FusionResp[] resp) throws SQLException {
		ArrayList<Respuestas> listRespuestas = new ArrayList<Respuestas>();//creamos el objeto lista
//		
//		//extraemos id_respuesta y respuesta de cada iteracion
		for (int i = 0; i < resp.length; i++) {
			Respuestas respuestas = new Respuestas();

			respuestas.setId_respuesta(resp[i].getIDE_RESPUESTA());
			respuestas.setRespuesta(resp[i].getTXT_RESPUESTA());
			listRespuestas.add(respuestas);//almacenamos la respuesta en la lista

		}
		resp[0].setRespuestas(listRespuestas);
		FusionResp[] respFinal = resp.clone();
		this.eliminarObj(respFinal);
		resp= respFinal;
		
		return resp;
	}

	@Override
	public FusionResp[] eliminarObj(FusionResp[] resp) throws SQLException {

		for (int i = 1; i < resp.length; i++) {
			resp[i]= null;
		}
		resp[0].setIDE_RESPUESTA(null);
		resp[0].setTXT_RESPUESTA(null);	
		return resp;
	}

}
