package mx.isban.neo.cc.arguments.controller;

import java.sql.SQLException;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import io.swagger.annotations.ApiOperation;
import mx.isban.neo.cc.arguments.service.IArgumentsService;
import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.request.FusionReq;
import mx.isban.neo.cc.model.request.UpdArgReq;
import mx.isban.neo.cc.model.response.FusionResp;
import mx.isban.neo.cc.model.response.MessageResponse;
import mx.isban.neo.cc.model.response.SeguimientoResp;
import mx.isban.neo.cc.model.response.updArgResp;
import mx.isban.neo.cc.model.rs.RespBean;
import mx.isban.neo.cc.utils.ConstastCommon;
import mx.isban.neo.cc.utils.Headers;
import mx.isban.neo.cc.utils.UtilMappingArgmsSql;
import mx.isban.neo.cc.utils.Utils;

/**
 * Clase tipo controller para la gestion de los argumentos en el proceso RTDC
  * Class ArgumentsController.java
  * @author Z266957 
 */
@RestController
@RequestMapping("/argumentos")
public class ArgumentsController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ArgumentsController.class);

	
	/**
	 * iyeccion para usar el servicio
	 */
	@Autowired
	private IArgumentsService iArgsService; 
	
	
	/**
     * consulta la traza del proceso de RTDC
     *
     * @param SYS_REFCURSOR Response de la consulta(function) a BD 
     * @return FolioService
     * @param cveFolio numero de folio
     * @param numFdc TDC a retener/cancelar
     * @param idArgumento identificador del argumento
     * @param idIteracion identificador de la iteracion 
     * @param resultValidacion resultado de la validacion a hacer 
     * @param idsMotivosCancel identificadores de motivos de cancelacion seleccionados
     * @param dataUser Header Santander
     */
    @ApiOperation(value = "consulta argumentos iniciales del proceso RTDC")
	@RequestMapping(value = "/initial", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> getInitialArguments(@RequestParam("cve_folio") String cveFolio,
    											@RequestParam("num_tdc") String numTdc,
    											@RequestParam("id_argumento") String idArgumento,
    											@RequestParam("id_iteracion") String idIteracion,
    											@RequestParam("result_validacion") String resultValidacion,
    											@RequestParam(value = "ids_motivosCancel", required = false) String idsMotivosCancel,
    											@RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA consulta inicial de argumentos");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean responseArgs = null;
		MessageResponse[] resultArguments=null;
		String consulta= "consulta_Arguments";
		
    		try {
    			DmlBean bean = UtilMappingArgmsSql.mapingQuery(consulta, cveFolio, numTdc, idArgumento, idIteracion,resultValidacion, idsMotivosCancel);
    			resultArguments = iArgsService.coredmlArguments(bean);
    	  
    	    	if(resultArguments != null && resultArguments[0].getIDMENSAJE() > 0) {
    	    		httpStatus = HttpStatus.OK;
    	    		responseArgs = new RespBean(); 
    	    		responseArgs.setResultado(resultArguments);
    				//setamos el estatus
    	    		responseArgs.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_MSG+" de argumentos iniciales.", httpStatus));
    	    	}else {
    	        	httpStatus = HttpStatus.BAD_REQUEST;
    	        	responseArgs = new RespBean(); 
    	        	responseArgs.setResultado(resultArguments);
    				//setamos el estatus
    	        	responseArgs.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en la consulta de argumentos iniciales.", httpStatus));
        	    	    	    
    	    	}	

    		}catch(HttpClientErrorException | SQLException ex) {
    				LOGGER.error("Ocurrio un error al consular los argumentos",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.consulta inicial de argumentos");
			return new ResponseEntity<RespBean>(responseArgs, httpStatus);
			
	}

    /**
     *  Actualiza el status de argumento
     *     * @param req Objeto que ser recibe con los parametros de entrada
     * @return respuesta del proceso de actualizacion del argumento
     *      * @param dataUser Header Santander
     **/
    @ApiOperation(value = "Actualiza el status de argumento")
	@RequestMapping(value = "/updArguments", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON}, consumes = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> updateArguments(@RequestBody UpdArgReq req,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA.actualizacion de status de arguments");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean responseUpd = null;
		updArgResp[] resultUpd=null;
    		try {
    			DmlBean bean = UtilMappingArgmsSql.mapingQueryObj("upd_argument", req);
    			resultUpd = iArgsService.coredmlUpdArguments(bean);  
    			
    			if(resultUpd != null && resultUpd[0].getIDMENSAJE() > 0) {
    	    		httpStatus = HttpStatus.OK;
    	    		responseUpd = new RespBean(); 
    	    		responseUpd.setResultado(resultUpd);
    				//setamos el estatus
    	    		responseUpd.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_RGS+" de la actualizacion del status del argumento.", httpStatus));
    	    	}else {
    	        	httpStatus = HttpStatus.BAD_REQUEST;
    	        	responseUpd = new RespBean(); 
    	        	responseUpd.setResultado(resultUpd);
    				//setamos el estatus
    	        	responseUpd.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en la actualización del argumento.", httpStatus));

    	    	}	    		
    			
    			
    		}catch(HttpClientErrorException|SQLException ex) {
    				LOGGER.error("Ocurrio un error al actualizar el argumento",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.actualizacion de status de arguments");
			return new ResponseEntity<RespBean>(responseUpd, httpStatus);
			
	}

    /**
     *  Actualiza el status de argumento a aceptado y guarda adicionales
     *
     * @param req Objeto que ser recibe con los parametros de entrada
     * @return respuesta del proceso de actualizacion del argumento
     * @param dataUser Header Santander
     */
    @ApiOperation(value = "Actualiza el status de argumento a aceptado y guarda adicionales")
	@RequestMapping(value = "/adicional", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON}, consumes = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> adicionalArguments(@RequestBody UpdArgReq req,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA.alta de parametros adicionales");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean responseAdic = null;
		updArgResp[] resultAdic=null;
		   		try {
    			DmlBean beanAdic = UtilMappingArgmsSql.mapingQueryObj("adicional_argument", req);
    			resultAdic = iArgsService.coredmlUpdArguments(beanAdic);  
    			
    			if(resultAdic != null && resultAdic[0].getIDMENSAJE() > 0) {
    	    		httpStatus = HttpStatus.OK;
    	    		responseAdic = new RespBean(); 
    	    		responseAdic.setResultado(resultAdic);
    				//setamos el estatus
    	    		responseAdic.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_RGS+" de los folios adicionales.", httpStatus));
    	    	}else {
    	        	httpStatus = HttpStatus.BAD_REQUEST;
    	        	responseAdic = new RespBean(); 
    	        	responseAdic.setResultado(resultAdic);
    				//setamos el estatus
    	        	responseAdic.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error al agregar un paso adicional.", httpStatus));

    	    	}
    		}catch(HttpClientErrorException|SQLException ex) {
    				LOGGER.error("Ocurrio un error al crear un argumento adicional",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.alta de parametros adicionales");
			return new ResponseEntity<RespBean>(responseAdic, httpStatus);
			
	}
    
    /**
     *  confirma si la TDC fue retenida
     *
     * @param req Objeto que ser recibe con los parametros de entrada
     * @return respuesta del proceso de confirmacion de la RTDC
     * @param dataUser Header Santander
     */
    @ApiOperation(value = "confirma si la TDC fue retenida")
	@RequestMapping(value = "/confirmRtdc", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON}, consumes = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> confirmRTDC(@RequestBody UpdArgReq req,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA.confirmar la retencion de TDC");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean responseConfirm = null;
		updArgResp[] resultC=null;
		String consulta= "confirma_rtdc";
		
    		try {
    			DmlBean bean = UtilMappingArgmsSql.mapingQueryObj(consulta, req);
    			resultC = iArgsService.coredmlUpdArguments(bean);  
	    	    		
    			if(resultC != null && resultC[0].getIDMENSAJE() > 0) {
    	    		httpStatus = HttpStatus.OK;
    	    		responseConfirm = new RespBean(); 
    	    		responseConfirm.setResultado(resultC);
    				//setamos el estatus
    	    		responseConfirm.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_RGS+" de la TDC a retener.", httpStatus));
    	    	}else {
    	        	httpStatus = HttpStatus.BAD_REQUEST;
    	        	responseConfirm = new RespBean(); 
    	        	responseConfirm.setResultado(resultC);
    				//setamos el estatus
    	        	responseConfirm.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error al confirmar una TDC.", httpStatus));

    	    	}
    		}catch(HttpClientErrorException|SQLException ex) {
    				LOGGER.error("Ocurrio un error al confirmar la retencion",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA. confirmar la retencion de TDC");
			return new ResponseEntity<RespBean>(responseConfirm, httpStatus);
			
	}
    
    /**
     *  realiza validaciones para la fusion de lineas
     *
     * @param req Objeto que ser recibe con los parametros de entrada
     * @return respuesta del proceso de fusion de lineas
     * @param dataUser Header Santander
     */
    @ApiOperation(value = "realiza la fusion de lineas de credito")
	@RequestMapping(value = "/fusion", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON}, consumes = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> fusion(@RequestBody FusionReq req,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA.Fusion de lineas");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean responseF = null;
		FusionResp[] resultFs=null;
		String consulta= "fusion_lineas";
		
    		try {
    			DmlBean bean = UtilMappingArgmsSql.mapingQueryObj(consulta, req);
    			resultFs = iArgsService.coredmlUpdArgumFusion(bean);
    					
    			if(resultFs != null && resultFs[0].getIDMENSAJE() > 0) {
    				LOGGER.info("Respuesta:" +resultFs);
    	    		httpStatus = HttpStatus.OK;
    	    		responseF = new RespBean(); 
    	    		responseF.setResultado(resultFs);
    				//setamos el estatus
    	    		responseF.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_MSG+" de validaciones para fusion de lineas.", httpStatus));
    	    	}else {
    	        	httpStatus = HttpStatus.BAD_REQUEST;
    	        	responseF = new RespBean(); 
    	        	responseF.setResultado(resultFs);
    				//setamos el estatus
    	        	responseF.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en la consulta de validaciones para fusion de lineas.", httpStatus));

    	    	}
    		}catch(HttpClientErrorException|SQLException ex) {
    				LOGGER.error("Ocurrio un error en la fusion de lineas",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA. Fusion de lineas");
			return new ResponseEntity<RespBean>(responseF, httpStatus);
			
	}

	/**
     * consulta el seguimiento del proceso de RTDC
     *
     * @param dataUser Header Santander 
     * @param cve_folio numero de folio de proceso RTDC
     * @param num_tdc numero de PAN
     * @param dataUser header Santander
     * @return regresa la consulta del seguimiento
     */
    @ApiOperation(value = "consulta seguimiento")
	@RequestMapping(value = "/tracing", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> getTracing(@RequestParam("cve_folio") String cveFolio,
    											@RequestParam("num_tdc") String numTdc,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA consulta de seguimientos");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean responseSeg = null;
		SeguimientoResp[] resultSeg=null;
		String consulta= "consulta_tracing";
		
		
    		try {
    			DmlBean bean = UtilMappingArgmsSql.mapingQuery(consulta, cveFolio, numTdc, null, null,null,null);
    			resultSeg = iArgsService.coredmlTracing(bean);
    	  
    	    	if(resultSeg != null && resultSeg[0].getIDMENSAJE() > 0) {
    	    		httpStatus = HttpStatus.OK;
    	    		responseSeg = new RespBean(); 
    	    		responseSeg.setResultado(resultSeg);
    				//setamos el estatus
    	    		responseSeg.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_MSG+" de seguimientos del Folio/NumTDC.", httpStatus));
    	    	}else {
    	        	httpStatus = HttpStatus.BAD_REQUEST;
    	        	responseSeg = new RespBean(); 
    	        	responseSeg.setResultado(resultSeg);
    				//setamos el estatus
    	        	responseSeg.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en la consulta de argumentos iniciales.", httpStatus));
        	    	    	    
    	    	}	

    		}catch(HttpClientErrorException | SQLException ex) {
    				LOGGER.error("Ocurrio un error al consular los seguimientos",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.consulta de seguimientos");
			return new ResponseEntity<RespBean>(responseSeg, httpStatus);
			
	}
    
}
