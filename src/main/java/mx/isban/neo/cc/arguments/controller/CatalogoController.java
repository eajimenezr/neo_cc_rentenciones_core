package mx.isban.neo.cc.arguments.controller;

import java.sql.SQLException;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import io.swagger.annotations.ApiOperation;
import mx.isban.neo.cc.catalogos.service.ICatalogoService;
import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.CatalogoResp;
import mx.isban.neo.cc.model.rs.RespBean;
import mx.isban.neo.cc.utils.ConstastCommon;
import mx.isban.neo.cc.utils.Headers;
import mx.isban.neo.cc.utils.UtilMappingArgmsSql;
import mx.isban.neo.cc.utils.Utils;

/**
 * Clase tipo controller para la gestion de los catalogos proceso RTDC
  * Class CatalogoController.java
  * @author Z266957 
 */
@RestController
@RequestMapping("/catalogos")
public class CatalogoController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CatalogoController.class);

	
	@Autowired
	private ICatalogoService iCatalogService; 
	
	/**
     * consulta los catalogos disponibles
     * @param id_catalogo identificador de catalogo a consultar
     * @param subItemCat subidentificador a consultar
     * @param dataUser header Santander
     * @return catalogos de TDCs
     */
    @ApiOperation(value = "consulta de catalogos")
	@RequestMapping(value = "/consulta", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> getCatalogs(@RequestParam("id_catalogo") String id_catalogo,
    									   @RequestParam("subItemCat") String subItemCat,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA consulta inicial de argumentos");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean response = null;
		CatalogoResp[] result=null;
		   		try {
    			DmlBean bean = UtilMappingArgmsSql.mapingQuery("consulta_catalogo", id_catalogo, subItemCat, null, null,null,null);
    	    	result = iCatalogService.coredmlCatalogs(bean);	
    	    	    	    		
    	    	if(result != null && result[0].getIDMENSAJE() > 0) {
    	    		httpStatus = HttpStatus.OK;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_MSG+" de catalogos.", httpStatus));
    	    	}else {
    	        	httpStatus = HttpStatus.BAD_REQUEST;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en la consulta de catalogos.", httpStatus));
        	    	    	    
    	    	}	
    	    	
    		}catch(HttpClientErrorException | SQLException ex) {
    				LOGGER.error("Ocurrio un error al consular los argumentos",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.consulta inicial de argumentos");
			return new ResponseEntity<RespBean>(response, httpStatus);
			
	}
	
	
	
}
