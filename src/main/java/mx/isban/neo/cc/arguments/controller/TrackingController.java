package mx.isban.neo.cc.arguments.controller;

import java.sql.SQLException;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import io.swagger.annotations.ApiOperation;
import mx.isban.neo.cc.arguments.service.IArgumentsService;
import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.request.SaveTrackingReq;
import mx.isban.neo.cc.model.response.TrackingArguResp;
import mx.isban.neo.cc.model.rs.RespBean;
import mx.isban.neo.cc.utils.ConstastCommon;
import mx.isban.neo.cc.utils.Headers;
import mx.isban.neo.cc.utils.UtilMappingArgmsSql;
import mx.isban.neo.cc.utils.Utils;

/**
 * Clase tipo controller para la gestion de seguimiento de argumentos
  * Class TrackingController.java
  * @author Z266957 
 */
@RestController
@RequestMapping("/tracking")
public class TrackingController {

	private static final Logger LOGGER = LoggerFactory.getLogger(TrackingController.class);

	
	/**
	 * inyeccion para usar el servicios
	 */
	@Autowired
	private IArgumentsService iArgumentsService; 
	
	/**
     * consulta los el seguimiento de argumentos
     * @param cve_folio folio de RTDC
     * @param num_tdc numero de PAN
     * @param dataUser header Santander
     * @return seguimiento de argumento
     */
    @ApiOperation(value = "consulta de traking de argumentos")
	@RequestMapping(value = "/consulta", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> getTrackingArguments(@RequestParam("cve_folio") String cve_folio,
    									   @RequestParam("num_tdc") String num_tdc,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA consulta de tracking de argumentos");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean response = null;
		TrackingArguResp[] result=null;
		String consulta= "consulta_trkArgument";
		
		
    		try {
    			DmlBean bean = UtilMappingArgmsSql.mapingQuery(consulta, cve_folio, num_tdc, null, null,null,null);
    	    	result = iArgumentsService.coredmlTrackArguments(bean);	
    	    	    	    		
    	    	if(result != null && result[0].getIDMENSAJE() != 2) {
    	    		httpStatus = HttpStatus.OK;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_MSG+" de tracking de argumentos.", httpStatus));
    	    	}else {
    	        	httpStatus = HttpStatus.BAD_REQUEST;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en la consulta de tracking de argumentos.", httpStatus));
        	    	    	    
    	    	}	
    	    	
    		}catch(HttpClientErrorException | SQLException ex) {
    				LOGGER.error("Ocurrio un error al consular el seguimiento de argumentos",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.consulta de tracking de argumentos");
			return new ResponseEntity<RespBean>(response, httpStatus);
			
	}
	
    /**
     *  confirma si la TDC fue retenida
     * @param req objeto que recibe el rquest 
     * @param dataUser header de santander
     * @return resultado de la transaccion
     */
    @ApiOperation(value = "guarda tracking")
	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON}, consumes = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> saveComents(@RequestBody SaveTrackingReq req,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA. guardar Tracking");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean response = null;
		TrackingArguResp[] result=null;
		String consulta= "guarda_coment";
		
    		try {
    			DmlBean bean = UtilMappingArgmsSql.mapingQueryObjGeneric(consulta, req.getCve_folio(),req.getNum_tarjeta(),req.getTxt_comentario(),req.getIde_argumento(),null,null);
    	    	result = iArgumentsService.coredmlTrackArguments(bean);	
	    	    		
    			if(result != null && result[0].getIDMENSAJE() > 0) {
    	    		httpStatus = HttpStatus.OK;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_RGS+" del comentario en el tracking.", httpStatus));
    	    	}else {
    	        	httpStatus = HttpStatus.BAD_REQUEST;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error al guardar el comentario en el tracking.", httpStatus));

    	    	}
    		}catch(HttpClientErrorException|SQLException ex) {
    				LOGGER.error("TrackingController",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.guardar Tracking");
			return new ResponseEntity<RespBean>(response, httpStatus);
			
	}    
	
	
}
