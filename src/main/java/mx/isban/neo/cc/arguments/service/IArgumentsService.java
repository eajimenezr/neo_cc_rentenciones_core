package mx.isban.neo.cc.arguments.service;

import java.sql.SQLException;

import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.FusionResp;
import mx.isban.neo.cc.model.response.MessageResponse;
import mx.isban.neo.cc.model.response.SeguimientoResp;
import mx.isban.neo.cc.model.response.TrackingArguResp;
import mx.isban.neo.cc.model.response.updArgResp;

 public interface IArgumentsService {
	
	 MessageResponse[] coredmlArguments(DmlBean bean) throws SQLException;
	
	 SeguimientoResp[] coredmlTracing(DmlBean bean) throws SQLException;
	
	 updArgResp[] coredmlUpdArguments(DmlBean bean) throws SQLException;
	
	 FusionResp[] coredmlUpdArgumFusion(DmlBean bean) throws SQLException;
	
	 TrackingArguResp[] coredmlTrackArguments(DmlBean bean) throws SQLException;
	
	 FusionResp[] validaResponse(FusionResp[] resp) throws SQLException;
	
	 FusionResp[] eliminarObj(FusionResp[] resp) throws SQLException;



}