package mx.isban.neo.cc.builder.dml.service;

import java.sql.SQLException;

import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.CatalogoResp;
import mx.isban.neo.cc.model.response.FusionResp;
import mx.isban.neo.cc.model.response.MessageResponse;
import mx.isban.neo.cc.model.response.SeguimientoResp;
import mx.isban.neo.cc.model.response.TrackingArguResp;
import mx.isban.neo.cc.model.response.updArgResp;


public interface IBuildDMLService {
	
	 MessageResponse[] buildDmlInArgs(DmlBean bean) throws SQLException;
	
	 updArgResp[] buildDmlUpd(DmlBean bean) throws SQLException;
	
	 CatalogoResp[] buildDmlCatalogos(DmlBean bean) throws SQLException;

	 FusionResp[] buildDmlFusion(DmlBean bean) throws SQLException;
	
	 SeguimientoResp[] buildDmlSeguimiento(DmlBean bean) throws SQLException;
	
	 TrackingArguResp[] buildDmlSeguimientoArgum(DmlBean bean) throws SQLException;




}