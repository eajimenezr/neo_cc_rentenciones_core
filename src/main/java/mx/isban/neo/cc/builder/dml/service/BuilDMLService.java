package mx.isban.neo.cc.builder.dml.service;

import java.sql.SQLException;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.isban.neo.cc.bd.dao.IEjecDmlArgsDAO;

import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.CatalogoResp;
import mx.isban.neo.cc.model.response.FusionResp;
import mx.isban.neo.cc.model.response.MessageResponse;
import mx.isban.neo.cc.model.response.SeguimientoResp;
import mx.isban.neo.cc.model.response.TrackingArguResp;
import mx.isban.neo.cc.model.response.updArgResp;

@Service
public class BuilDMLService implements IBuildDMLService {
	
	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(BuilDMLService.class);

	@Autowired
	private IEjecDmlArgsDAO executesql;

	@Override
	public MessageResponse[] buildDmlInArgs(DmlBean bean) throws SQLException {
		
		MessageResponse[] result= null;
		if(bean != null) {
				
			try {
					result = executesql.dmlArguments(bean);
				
			} catch (SQLException e) {
				LOGGER.error("Ocurrio una excepcion en la ejecucion SQL"+e);
			}
		}

		return result;
	}

	@Override
	public updArgResp[] buildDmlUpd(DmlBean bean) throws SQLException {
		updArgResp[] result= null;
		if(bean != null) {
			try {
				result = executesql.dmlUpdStatArguments(bean);

			} catch (SQLException e) {
				LOGGER.error("Ocurrio una excepcion en la ejecucion SQL"+e);
			}
		}
		return result;
	}

	@Override
	public CatalogoResp[] buildDmlCatalogos(DmlBean bean) throws SQLException {
		CatalogoResp[] result= null;
		if(bean != null) {
//			String funcion = bean.getFuncion();	
			try {
				result = executesql.dmlCatalogos(bean);

			} catch (SQLException e) {
				LOGGER.error("Ocurrio una excepcion en la ejecucion SQL"+e);
			}
		}
		return result;
	}

	@Override
	public FusionResp[] buildDmlFusion(DmlBean bean) throws SQLException {
		FusionResp[] result= null;
		if(bean != null) {
//			String funcion = bean.getFuncion();	
			try {
				result = executesql.dmlFusion(bean);

			} catch (SQLException e) {
				LOGGER.error("Ocurrio una excepcion en la ejecucion SQL"+e);
			}
		}
		return result;
	}

	@Override
	public SeguimientoResp[] buildDmlSeguimiento(DmlBean bean) throws SQLException {
		SeguimientoResp[] result= null;
		if(bean != null) {
			try {
				result = executesql.dmlSeguimiento(bean);

			} catch (SQLException e) {
				LOGGER.error("Ocurrio una excepcion en la ejecucion SQL"+e);
			}
		}
		return result;
	}

	@Override
	public TrackingArguResp[] buildDmlSeguimientoArgum(DmlBean bean) throws SQLException {
		TrackingArguResp[] result= null;
		if(bean != null) {
			try {
				result = executesql.dmlTrackArguments(bean);

			} catch (SQLException e) {
				LOGGER.error("Ocurrio una excepcion en la ejecucion SQL"+e);
			}
		}
		return result;
	}




	
	
}
 