package mx.isban.neo.cc.builder.dml.service;

import java.sql.SQLException;

import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.AdicionalResp;
import mx.isban.neo.cc.model.response.CodesPendingResp;
import mx.isban.neo.cc.model.response.MessageRespCancel;
import mx.isban.neo.cc.tenencias.model.LimiteResp;



public interface IBuildReDMLService {
	
	public CodesPendingResp[] buildDmlCodesPending(DmlBean bean) throws SQLException;
	
	public LimiteResp[] buildDml(DmlBean bean) throws SQLException;
	
	public MessageRespCancel[] buildDmlCancel(DmlBean bean) throws SQLException;
	
	public AdicionalResp[] buildDmlAdicionales(DmlBean bean) throws SQLException;
	

}