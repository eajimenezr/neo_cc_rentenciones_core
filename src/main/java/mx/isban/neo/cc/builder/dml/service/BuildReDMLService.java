package mx.isban.neo.cc.builder.dml.service;

import java.sql.SQLException;


import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.isban.neo.cc.bd.dao.IEjecReDmlDAO;
import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.AdicionalResp;
import mx.isban.neo.cc.model.response.CodesPendingResp;
import mx.isban.neo.cc.model.response.MessageRespCancel;
import mx.isban.neo.cc.tenencias.model.LimiteResp;


@Service
public class BuildReDMLService implements IBuildReDMLService {
	
	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(BuildReDMLService.class);
	
	@Autowired
	private IEjecReDmlDAO executesql;


	
	@Override
	public CodesPendingResp[] buildDmlCodesPending(DmlBean bean) throws SQLException {
			
		CodesPendingResp[] result= null;
		if(bean != null) {
			try {
					result = executesql.dmlSegArguments(bean);
					
			} catch (SQLException e) {
				LOGGER.error("Ocurrio una excepcion en la ejecucion SQL"+e);
			}
		}

		return result;
	}

	@Override
	public LimiteResp[] buildDml(DmlBean bean) throws SQLException{
		
		LimiteResp[] result= null;

			try {
				result = executesql.dmlLimite(bean);
				//respbean se hara dependiendo al bean correspondiente
			} catch (SQLException e) {
				LOGGER.error("Ocurrio una excepcion en la ejecucion SQL"+e);
			}
		

		return result;
	}
	
	@Override
	public MessageRespCancel[] buildDmlCancel(DmlBean bean) throws SQLException {
		
		MessageRespCancel[] result= null;
		if(bean != null) {
			try {
					result = executesql.dmlCancel(bean);
					
			} catch (SQLException e) {
				LOGGER.error("Ocurrio una excepcion en la ejecucion SQL"+e);
			}
		}

		return result;
	}

	@Override
	public AdicionalResp[] buildDmlAdicionales(DmlBean bean) throws SQLException {
		AdicionalResp[] result= null;
		if(bean != null) {
			try {
				if (bean.getFuncion().equals("FNC_RET_GUARDA_CELMAIL")) {
					result = executesql.dmlCelEmail(bean);

				}else {
					result = executesql.dmlExeAdic(bean);
				}
			} catch (SQLException e) {
				LOGGER.error("Ocurrio una excepcion en la ejecucion SQL"+e);
			}
		}

		return result;
	}

		
}
 