package mx.isban.neo.cc.scripts.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.isban.neo.cc.folio.service.IBuilderDMLService;
import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.MessageResp;

@Service
public class ScriptService implements IScriptService {

	
	@Autowired
	private IBuilderDMLService buildService;
	
	
	@Override
	public MessageResp[] coredmlScript(DmlBean bean) {
		return buildService.buildDmlScript(bean);
	}
	
	@Override
	public MessageResp[] coredmlMotivoCall(DmlBean bean) {
		return buildService.buildDmlScript(bean);
	}
	
	

}
