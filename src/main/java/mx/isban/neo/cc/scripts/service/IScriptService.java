package mx.isban.neo.cc.scripts.service;

import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.MessageResp;

public interface IScriptService {
	
	MessageResp[] coredmlScript(DmlBean bean);

	MessageResp[] coredmlMotivoCall(DmlBean bean);

}