/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * IEjecucionDmlDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2019-06-25                               Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc.bd.dao;


import java.sql.SQLException;

import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.CatalogoResp;
import mx.isban.neo.cc.model.response.FusionResp;
import mx.isban.neo.cc.model.response.MessageResponse;
import mx.isban.neo.cc.model.response.MotivosCancelaResp;
import mx.isban.neo.cc.model.response.SeguimientoResp;
import mx.isban.neo.cc.model.response.TraceArgumentosResp;
import mx.isban.neo.cc.model.response.TrackingArguResp;
import mx.isban.neo.cc.model.response.updArgResp;


/**
 * Type class Interfaz .<br>
 * Main objective: Definir la listado de los metodos de la clase de tipo DAO de EjecucionDmlDAO <br>
 * Contains methods:
 * <ul>
 * <li>accessDml</li>
 * </ul>
 * @author Isban / 
 * IEjecucionDmlDAO.java by Z266957
 *
 */
 public interface IEjecDmlArgsDAO {

	/**
	 * Method dmlUpdate
	 * Method objective: ___ .<br>
	 * @param nameQuery query a ejecutar
	 * @param paramsQuery parametros de entrada para el insert, update o delete
	 * @return List<Map<String,Object>> listado de mapa de datos
	 */
		
     MessageResponse[] dmlArguments(DmlBean bean) throws SQLException;
    
     updArgResp[] dmlUpdStatArguments(DmlBean bean) throws SQLException;
       
     CatalogoResp[] dmlCatalogos(DmlBean bean) throws SQLException;
    
     FusionResp[] dmlFusion(DmlBean bean) throws SQLException;
    
     SeguimientoResp[] dmlSeguimiento(DmlBean bean) throws SQLException;
    
     MotivosCancelaResp[] dmlSegMotivos(DmlBean bean) throws SQLException;
    
     TraceArgumentosResp[] dmlSegArguments(DmlBean bean) throws SQLException;
    
     TrackingArguResp[] dmlTrackArguments(DmlBean bean) throws SQLException;

}
