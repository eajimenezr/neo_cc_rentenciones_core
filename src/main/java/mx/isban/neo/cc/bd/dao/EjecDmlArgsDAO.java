/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * EjecucionDmlDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2019-06-25                               Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc.bd.dao;
import java.sql.SQLException;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mx.isban.neo.cc.arguments.service.IArgumentsService;
import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.CatalogoResp;
import mx.isban.neo.cc.model.response.FusionResp;
import mx.isban.neo.cc.model.response.MessageResponse;
import mx.isban.neo.cc.model.response.MotivosCancelaResp;
import mx.isban.neo.cc.model.response.SeguimientoResp;
import mx.isban.neo.cc.model.response.TraceArgumentosResp;
import mx.isban.neo.cc.model.response.TrackingArguResp;
import mx.isban.neo.cc.model.response.updArgResp;
import oracle.jdbc.OracleTypes;

/**
 * Clase tipo data access object para ejecutar consultas de forma generica a una bd con el Prepared Statement Setter.<br>
 * Contiene los metodos siguientes:<br>
 * <ul>
 * <li>accessDml</li>
 * <li>setValuesPS</li>
 * </ul>
 * Variables globales:<br>
 * <ul>
 * <li>jdbcTemplate</li>
 * <li>codError</li>
 * </ul>
 * @author Isban / iscontreras
 * Class EjecucionDmlDAO.java 
 */
@Repository
public class EjecDmlArgsDAO implements IEjecDmlArgsDAO {
	/**
	 * Constante para trazar la aplicacion.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(EjecDmlArgsDAO.class);
	/**
	 *  JdbcTemplate jdbcTemplate: inyeccion del bean jdbcTemplate
	 */
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private IArgumentsService argumentsService;
	
	/**
	 *  Integer dbTimeOut : tiempo de expiracion en la conexin a la BD
	 */
	@Value("${bd.values.schema}")
	private String schema;
	
	@Value("${bd.values.typecursor}")
	private String cursor;
	
	private static final String functionTraceMotivtoCall = "FNC_CONSULTA_MOTIVOSCANCEL";
	
	
	private static final String functionTraceArguemntos = "FNC_CONSULTA_ARGUMENTOS";


	@Override
	public MessageResponse[] dmlArguments(DmlBean bean) throws SQLException {
		MessageResponse[] SYS_REFCURSOR = null;
		
			Map<String, String> paramEnt = bean.getParamsIn();
			 
			String 	cve_folio ="pa_cve_folio";
			String 	num_Tarjeta ="pa_num_Tarjeta"; 
			String 	id_argument ="pa_ide_argumento";
			String 	id_itera ="pa_iteracion";
			String 	validacion ="pa_resultadoValidacion";
			String 	ids_motivosCancel ="pa_ides_motivosCanelacion";
			
			 SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		        		.withSchemaName(schema)
		                .withFunctionName(bean.getFuncion())
		                .declareParameters(new SqlOutParameter(cursor, OracleTypes.CURSOR))
		                .withNamedBinding();
			    
				MapSqlParameterSource in = new MapSqlParameterSource();
				in.addValue(cve_folio, paramEnt.get(cve_folio),OracleTypes.VARCHAR);
				in.addValue(num_Tarjeta, paramEnt.get(num_Tarjeta),OracleTypes.VARCHAR);
				in.addValue(id_argument, paramEnt.get(id_argument),OracleTypes.VARCHAR);
				in.addValue(id_itera, paramEnt.get(id_itera),OracleTypes.NUMBER);
				in.addValue(validacion, paramEnt.get(validacion),OracleTypes.NUMBER);
				in.addValue(ids_motivosCancel, paramEnt.get(ids_motivosCancel),OracleTypes.VARCHAR);


		        	Map<String, Object> out = simpleJdbcCall.execute(in);
		        	Object resp = out.get(cursor);
		        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
		        	
		        	final GsonBuilder gsonBuilder = new GsonBuilder();
		        	final Gson gson = gsonBuilder.create();

		        	MessageResponse[] response = gson.fromJson(JsonFolio, MessageResponse[].class);
		        	SYS_REFCURSOR= response;
			
			
			
			return SYS_REFCURSOR;
			
		
	}

	
	@Override
	public updArgResp[] dmlUpdStatArguments(DmlBean bean) throws SQLException {
		
		updArgResp[] SYS_REFCURSOR = null;
		Map<String, String> paramEnt = bean.getParamsIn();
		if(bean.getFuncion().equals("FNC_RET_GUARDA_ADICIONALES")) {

		String 	cve_folio ="pa_cve_folio";
		String 	num_Tarjeta ="pa_num_Tarjeta"; 
		String 	id_argument ="pa_ide_argumento";
		String 	captura ="pa_captura_adicional";
		String 	folioAdicional ="pa_folio_adicional";

		 SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	        		.withSchemaName(schema)
	                .withFunctionName(bean.getFuncion())
	                .declareParameters(new SqlOutParameter(cursor, OracleTypes.CURSOR))
	                .withNamedBinding();
		    
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue(cve_folio, paramEnt.get(cve_folio),OracleTypes.VARCHAR);
			in.addValue(num_Tarjeta, paramEnt.get(num_Tarjeta),OracleTypes.VARCHAR);
			in.addValue(id_argument, paramEnt.get(id_argument),OracleTypes.NUMBER);
			in.addValue(captura, paramEnt.get(captura),OracleTypes.VARCHAR);
			in.addValue(folioAdicional, paramEnt.get(folioAdicional),OracleTypes.VARCHAR);

	        	Map<String, Object> out = simpleJdbcCall.execute(in);
	        	Object resp = out.get(cursor);
	        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
	        	
	        	final GsonBuilder gsonBuilder = new GsonBuilder();
	        	final Gson gson = gsonBuilder.create();
	            SYS_REFCURSOR = gson.fromJson(JsonFolio, updArgResp[].class);
	            
		}else if(bean.getFuncion().equals("FNC_RET_ACTUALIZA_STATARGU")) {
			String 	cve_folio ="pa_cve_folio";
			String 	num_Tarjeta ="pa_num_Tarjeta"; 
			String 	id_argument ="pa_ide_argumento";
			String 	status_argumento ="pa_status_argumento";
			String 	ids_motivosCancel ="pa_ides_motivosCanelacion";

			
			 SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		        		.withSchemaName(schema)
		                .withFunctionName(bean.getFuncion())
		                .declareParameters(new SqlOutParameter(cursor, OracleTypes.CURSOR))
		                .withNamedBinding();
			    
				MapSqlParameterSource in = new MapSqlParameterSource();
				in.addValue(cve_folio, paramEnt.get(cve_folio),OracleTypes.VARCHAR);
				in.addValue(num_Tarjeta, paramEnt.get(num_Tarjeta),OracleTypes.VARCHAR);
				in.addValue(id_argument, paramEnt.get(id_argument),OracleTypes.NUMBER);
				in.addValue(status_argumento, paramEnt.get(status_argumento),OracleTypes.NUMBER);
				in.addValue(ids_motivosCancel, paramEnt.get(ids_motivosCancel),OracleTypes.VARCHAR);



		        	Map<String, Object> out = simpleJdbcCall.execute(in);
		        	Object resp = out.get(cursor);
		        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
		        	
		        	final GsonBuilder gsonBuilder = new GsonBuilder();
		        	final Gson gson = gsonBuilder.create();
		            SYS_REFCURSOR = gson.fromJson(JsonFolio, updArgResp[].class);
			
		}else {
			String 	cve_folio ="pa_cve_folio";
			String 	num_Tarjeta ="pa_num_Tarjeta"; 
			String 	statusRtdc ="pa_statusRetencion";

			
			 SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		        		.withSchemaName(schema)
		                .withFunctionName(bean.getFuncion())
		                .declareParameters(new SqlOutParameter(cursor, OracleTypes.CURSOR))
		                .withNamedBinding();
			    
				MapSqlParameterSource in = new MapSqlParameterSource();
				in.addValue(cve_folio, paramEnt.get(cve_folio),OracleTypes.VARCHAR);
				in.addValue(num_Tarjeta, paramEnt.get(num_Tarjeta),OracleTypes.VARCHAR);
				in.addValue(statusRtdc, paramEnt.get(statusRtdc),OracleTypes.NUMBER);


		        	Map<String, Object> out = simpleJdbcCall.execute(in);
		        	Object resp = out.get(cursor);
		        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
		        	
		        	final GsonBuilder gsonBuilder = new GsonBuilder();
		        	final Gson gson = gsonBuilder.create();
		            SYS_REFCURSOR = gson.fromJson(JsonFolio, updArgResp[].class);
			
		}
		
		return SYS_REFCURSOR;
	}


	@Override
	public CatalogoResp[] dmlCatalogos(DmlBean bean) throws SQLException {
		CatalogoResp[] SYS_REFCURSOR = null;
		Map<String, String> paramEnt = bean.getParamsIn();		
		
		Object pa1 = paramEnt.keySet().toArray()[0];
		Object pa2 = paramEnt.keySet().toArray()[1];
		
		 SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	        		.withSchemaName(schema)
	                .withFunctionName(bean.getFuncion())
	                .declareParameters(new SqlOutParameter(cursor, OracleTypes.CURSOR))
	                .withNamedBinding();
		    
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue(pa1.toString(), paramEnt.get(pa1.toString()),OracleTypes.NUMBER);
			in.addValue(pa2.toString(), paramEnt.get(pa2.toString()),OracleTypes.NUMBER);



	        	Map<String, Object> out = simpleJdbcCall.execute(in);
	        	Object resp = out.get(cursor);
	        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
	        	
	        	final GsonBuilder gsonBuilder = new GsonBuilder();
	        	final Gson gson = gsonBuilder.create();
	            SYS_REFCURSOR = gson.fromJson(JsonFolio, CatalogoResp[].class);
		
		
		return SYS_REFCURSOR;
	}


	@Override
	public FusionResp[] dmlFusion(DmlBean bean) throws SQLException {
		FusionResp[] SYS_REFCURSOR = null;
		
		Map<String, String> paramEnt = bean.getParamsIn();
		 
		String 	cve_folio ="pa_cve_folio";
		String 	num_Tarjeta ="pa_num_Tarjeta"; 
		String 	id_validacion ="pa_idValidacion";
		String 	id_respuesta ="pa_ide_respuesta";
		
		
		 SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	        		.withSchemaName(schema)
	                .withFunctionName(bean.getFuncion())
	                .declareParameters(new SqlOutParameter(cursor, OracleTypes.CURSOR))
	                .withNamedBinding();
		    
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue(cve_folio, paramEnt.get(cve_folio),OracleTypes.VARCHAR);
			in.addValue(num_Tarjeta, paramEnt.get(num_Tarjeta),OracleTypes.VARCHAR);
			in.addValue(id_validacion, paramEnt.get(id_validacion),OracleTypes.VARCHAR);
			in.addValue(id_respuesta, paramEnt.get(id_respuesta),OracleTypes.NUMBER);
			


	        	Map<String, Object> out = simpleJdbcCall.execute(in);
	        	Object resp = out.get(cursor);
	        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
	        	
	        	final GsonBuilder gsonBuilder = new GsonBuilder();
	        	final Gson gson = gsonBuilder.create();

	        	FusionResp[] response = gson.fromJson(JsonFolio, FusionResp[].class);
	        	
	        	if(response.length > 1) {
	        		
	        		response=argumentsService.validaResponse(response);
	        	}
	        	
	        	SYS_REFCURSOR= response;
		
		
		
		return SYS_REFCURSOR;
	}


	@Override
	public SeguimientoResp[] dmlSeguimiento(DmlBean bean) throws SQLException {
		SeguimientoResp[] SYS_REFCURSOR = null;
		
		Map<String, String> paramEnt = bean.getParamsIn();
		 
		String 	cve_folio ="pa_cve_folio";
		String 	num_Tarjeta ="pa_num_Tarjeta"; 

		
		 SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	        		.withSchemaName(schema)
	        		.withCatalogName(bean.getPkg())
	                .withFunctionName(bean.getFuncion())
	                .declareParameters(new SqlOutParameter(cursor, OracleTypes.CURSOR))
	                .withNamedBinding();
		    
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue(cve_folio, paramEnt.get(cve_folio),OracleTypes.VARCHAR);
			in.addValue(num_Tarjeta, paramEnt.get(num_Tarjeta),OracleTypes.NUMBER);

	        	Map<String, Object> out = simpleJdbcCall.execute(in);
	        	Object resp = out.get(cursor);
	        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
	        	
	        	final GsonBuilder gsonBuilder = new GsonBuilder();
	        	final Gson gson = gsonBuilder.create();

	        	SeguimientoResp[] response = gson.fromJson(JsonFolio, SeguimientoResp[].class);
	        	
	        	///se manda a consultar el motivos de cancelacion relacionado al folio o num_TDC
	        	DmlBean beanMotivos = bean;
	        	beanMotivos.setFuncion(functionTraceMotivtoCall);
	        	///se manda a consultar el motivos de cancelacion relacionado al folio o num_TDC
	        	MotivosCancelaResp[] motivosCancel= this.dmlSegMotivos(beanMotivos);
	        	
	        	if (motivosCancel != null) {
	        		response[0].setMotivos_cancelacion(motivosCancel);
				}	        	
	        	///se manda a consultar los argumentos relacionados al folio o num_TDC
	        	DmlBean beanArgumentos = bean;
	        	beanMotivos.setFuncion(functionTraceArguemntos);
	        	///se manda a consultar el motivos de cancelacion relacionado al folio o num_TDC
	        	TraceArgumentosResp[] argumentos= this.dmlSegArguments(beanArgumentos);
	        	
	        	if (argumentos != null) {
	        		response[0].setArgumentos(argumentos);
				}
	        	
	        	
	        	
	        	SYS_REFCURSOR= response;
		
		
		
		return SYS_REFCURSOR;	
		}


	@Override
	public MotivosCancelaResp[] dmlSegMotivos(DmlBean bean) throws SQLException {
		MotivosCancelaResp[] SYS_REFCURSOR = null;
		
		Map<String, String> paramEnt = bean.getParamsIn();
		 
		String 	cve_folio ="pa_cve_folio";
		String 	num_Tarjeta ="pa_num_Tarjeta"; 

		
		 SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	        		.withSchemaName(schema)
	        		.withCatalogName(bean.getPkg())
	                .withFunctionName(bean.getFuncion())
	                .declareParameters(new SqlOutParameter(cursor, OracleTypes.CURSOR))
	                .withNamedBinding();
		    
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue(cve_folio, paramEnt.get(cve_folio),OracleTypes.VARCHAR);
			in.addValue(num_Tarjeta, paramEnt.get(num_Tarjeta),OracleTypes.NUMBER);

	        	Map<String, Object> out = simpleJdbcCall.execute(in);
	        	Object resp = out.get(cursor);
	        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
	        	
	        	final GsonBuilder gsonBuilder = new GsonBuilder();
	        	final Gson gson = gsonBuilder.create();

	        	MotivosCancelaResp[] response = gson.fromJson(JsonFolio, MotivosCancelaResp[].class);
	        	

	        	SYS_REFCURSOR= response;
		
		
		
		return SYS_REFCURSOR;
	        	
	}


	@Override
	public TraceArgumentosResp[] dmlSegArguments(DmlBean bean) throws SQLException {
		TraceArgumentosResp[] SYS_REFCURSOR = null;
		
		Map<String, String> paramEnt = bean.getParamsIn();
		 
		String 	cve_folio ="pa_cve_folio";
		String 	num_Tarjeta ="pa_num_Tarjeta"; 

		
		 SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	        		.withSchemaName(schema)
	        		.withCatalogName(bean.getPkg())
	                .withFunctionName(bean.getFuncion())
	                .declareParameters(new SqlOutParameter(cursor, OracleTypes.CURSOR))
	                .withNamedBinding();
		    
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue(cve_folio, paramEnt.get(cve_folio),OracleTypes.VARCHAR);
			in.addValue(num_Tarjeta, paramEnt.get(num_Tarjeta),OracleTypes.NUMBER);

	        	Map<String, Object> out = simpleJdbcCall.execute(in);
	        	Object resp = out.get(cursor);
	        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
	        	
	        	final GsonBuilder gsonBuilder = new GsonBuilder();
	        	final Gson gson = gsonBuilder.create();

	        	TraceArgumentosResp[] response = gson.fromJson(JsonFolio, TraceArgumentosResp[].class);
	        	

	        	SYS_REFCURSOR= response;
		
		
		
		return SYS_REFCURSOR;
	}


	@Override
	public TrackingArguResp[] dmlTrackArguments(DmlBean bean) throws SQLException {
		TrackingArguResp[] SYS_REFCURSOR = null;
		
		String 	cve_folio ="pa_cve_folio";
		String 	num_Tarjeta ="pa_num_Tarjeta"; 
		String 	txt_coment ="pa_txt_comentario";
		String 	id_argumento ="pa_ide_argumento";
		
		if(bean.getFuncion().equals("FNC_CONSULTA_PROCESO")) {
		
		Map<String, String> paramEnt = bean.getParamsIn();

		 SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	        		.withSchemaName(schema)
	        		.withCatalogName(bean.getPkg())
	                .withFunctionName(bean.getFuncion())
	                .declareParameters(new SqlOutParameter(cursor, OracleTypes.CURSOR))
	                .withNamedBinding();
		    
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue(cve_folio, paramEnt.get(cve_folio),OracleTypes.VARCHAR);
			in.addValue(num_Tarjeta, paramEnt.get(num_Tarjeta),OracleTypes.NUMBER);

	        	Map<String, Object> out = simpleJdbcCall.execute(in);
	        	Object resp = out.get(cursor);
	        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
	        	
	        	final GsonBuilder gsonBuilder = new GsonBuilder();
	        	final Gson gson = gsonBuilder.create();

	        	TrackingArguResp[] response = gson.fromJson(JsonFolio, TrackingArguResp[].class);
	        	

	        	SYS_REFCURSOR= response;
		
		}else {
			

			Map<String, String> paramEnt = bean.getParamsIn();
			
			 SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		        		.withSchemaName(schema)
		        		.withCatalogName(bean.getPkg())
		                .withFunctionName(bean.getFuncion())
		                .declareParameters(new SqlOutParameter(cursor, OracleTypes.CURSOR))
		                .withNamedBinding();
			    
				MapSqlParameterSource in = new MapSqlParameterSource();
				in.addValue(cve_folio, paramEnt.get(cve_folio),OracleTypes.VARCHAR);
				in.addValue(num_Tarjeta, paramEnt.get(num_Tarjeta),OracleTypes.NUMBER);
				in.addValue(txt_coment, paramEnt.get(txt_coment),OracleTypes.VARCHAR);
				in.addValue(id_argumento, paramEnt.get(id_argumento),OracleTypes.NUMBER);

		        	Map<String, Object> out = simpleJdbcCall.execute(in);
		        	Object resp = out.get(cursor);
		        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
		        	
		        	final GsonBuilder gsonBuilder = new GsonBuilder();
		        	final Gson gson = gsonBuilder.create();

		        	TrackingArguResp[] response = gson.fromJson(JsonFolio, TrackingArguResp[].class);
		        	

		        	SYS_REFCURSOR= response;
			
		}
		
		return SYS_REFCURSOR;
	}




}


