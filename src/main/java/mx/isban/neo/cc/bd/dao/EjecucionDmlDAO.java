/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * EjecucionDmlDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2019-06-25                               Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc.bd.dao;
import java.sql.SQLException;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.BenefitsResp;
import mx.isban.neo.cc.model.response.CodesResponse;
import mx.isban.neo.cc.model.response.FolioResponse;
import mx.isban.neo.cc.model.response.MessageResp;
import mx.isban.neo.cc.model.response.MotivosCancelResp;
import mx.isban.neo.cc.model.response.TrazaResp;
import oracle.jdbc.OracleTypes;

/**
 * Clase tipo data access object para ejecutar consultas de forma generica a una bd con el Prepared Statement Setter.<br>
 * Contiene los metodos siguientes:<br>
 * <ul>
 * <li>accessDml</li>
 * <li>setValuesPS</li>
 * </ul>
 * Variables globales:<br>
 * <ul>
 * <li>jdbcTemplate</li>
 * <li>codError</li>
 * </ul>
 * @author Isban / iscontreras
 * Class EjecucionDmlDAO.java 
 */
@Repository
public class EjecucionDmlDAO implements IEjecucionDmlDAO {
	/**
	 * Constante para trazar la aplicacion.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(EjecucionDmlDAO.class);
	/**
	 *  JdbcTemplate jdbcTemplate: inyeccion del bean jdbcTemplate
	 */
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	/**
	 *  Integer dbTimeOut : tiempo de expiracion en la conexin a la BD
	 */
	@Value("${bd.values.schema}")
	private String schema;
	
	@Value("${bd.values.functionFolio}")
	private String funcionFolio;
	
	@Value("${bd.values.functionCodes}")
	private String codesFunction;
	
	@Value("${bd.values.paqueteBloqueo}")
	private String pkgbloqueo;
	
	private static final String Cursor= "SYS_REFCURSOR";
	
	private static final String paramIn= "PA_CODIGOS";
	
	private static final String pInStatus = "PA_IND_ESTATUS_MOTIVO";
	
	private static final String pInPrio= "PA_NUM_PRIORIDAD";
	
	private static final String pInDesc= "PA_DES_MOTIVO";
	
	private static final String pINFolio= "pa_cve_folio";
	
	private static final String pInFchTraza= "pa_fec_trn";
	
	private static final String pInBuc= "pa_cve_buc";
	
	private static final String pInCve_ejecutivo= "pa_cve_ejecutivo";
	
	private static final String pInNameClient= "pa_desc_nombre_cliente";

	
	/**
	 * Method dmlUpdate
	 * Method objective: ___ .<br>
	 * @return List<Map<String,Object>> listado de mapa de datos
	 */
	   @Override
	    public FolioResponse[] getNewFolio(DmlBean bean) throws SQLException {
		   
		   Map<String, String> paramEnt = bean.getParamsIn();
		   
		    SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	        		.withSchemaName(schema)
	                .withFunctionName(funcionFolio)
	                .declareParameters(new SqlOutParameter(Cursor, OracleTypes.CURSOR))
	                .declareParameters(
	                		new SqlParameter(pInBuc,OracleTypes.VARCHAR))
	                .declareParameters(
	                		new SqlParameter(pInCve_ejecutivo,OracleTypes.VARCHAR))
	                .declareParameters(
	                		new SqlParameter(pInNameClient,OracleTypes.VARCHAR))
	                .withNamedBinding();
		    
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue(pInBuc, paramEnt.get(pInBuc),OracleTypes.VARCHAR);
			in.addValue(pInCve_ejecutivo, paramEnt.get(pInCve_ejecutivo),OracleTypes.VARCHAR);
			in.addValue(pInNameClient, paramEnt.get(pInNameClient),OracleTypes.VARCHAR);

	        	FolioResponse[] SYS_REFCURSOR = null;
	        	Map<String, Object> out = simpleJdbcCall.execute(in);
	        	Object resp = out.get(Cursor);
	        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
	        	
	        	final GsonBuilder gsonBuilder = new GsonBuilder();
	        	final Gson gson = gsonBuilder.create();

	        	FolioResponse[] testCase = gson.fromJson(JsonFolio, FolioResponse[].class);
	        	SYS_REFCURSOR= testCase;
	        	LOGGER.error("Objeto serializado: " +SYS_REFCURSOR);
	        return SYS_REFCURSOR;
	    }


	@Override
	public CodesResponse[] getCodigosBloqueo(String code) throws SQLException {
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName(schema)
		.withCatalogName(pkgbloqueo)
        .withFunctionName(codesFunction)
        .withReturnValue()
        .withoutProcedureColumnMetaDataAccess()
        .declareParameters(
        		new SqlOutParameter(Cursor, OracleTypes.CURSOR))
        .declareParameters(
        		new SqlParameter(paramIn,OracleTypes.VARCHAR))
        .withNamedBinding();
		
	     SqlParameterSource in = new MapSqlParameterSource().addValue(paramIn, code, OracleTypes.VARCHAR);

		
		if (LOGGER.isDebugEnabled()) {
		    LOGGER.debug(" getcodes CALL FUNCTION ");
		}
		
		CodesResponse[] SYS_REFCURSOR ;
		Map<String, Object> out = simpleJdbcCall.execute(in);
		Object resp = out.get("SYS_REFCURSOR");
    	String JsonFolio = new com.google.gson.Gson().toJson(resp);
    	
    	final GsonBuilder gsonBuilder = new GsonBuilder();
    	final Gson gson = gsonBuilder.create();

    	CodesResponse[] json = gson.fromJson(JsonFolio, CodesResponse[].class);
    	SYS_REFCURSOR= json;
    	LOGGER.error("Objeto serializado: " +SYS_REFCURSOR);
		
		
		return SYS_REFCURSOR;		

	}


	@Override
	public MotivosCancelResp[] getCatalog(String function, String pkg, Map<String, String> map,Map<String, String> map2)
			throws SQLException {
		MotivosCancelResp[] SYS_REFCURSOR = null;
		for (String i : map.keySet()) {
		     LOGGER.error("key: " + i + " value: " + map.get(i));
		    }
		
		
		if(map2 != null) {
			String status =map2.get("status");
			String priority =map2.get("prioridad");
			String descripcion =map2.get("desc");
			
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	        		.withSchemaName(schema)
	        		.withCatalogName(pkg)
	                .withFunctionName(function)
	                .declareParameters(new SqlOutParameter(Cursor, OracleTypes.CURSOR))
	                .declareParameters(
	                		new SqlParameter(pInStatus,OracleTypes.INTEGER))
	                .declareParameters(
	                		new SqlParameter(pInPrio,OracleTypes.INTEGER))
	                .declareParameters(
	                		new SqlParameter(pInDesc,OracleTypes.VARCHAR))
	                .withNamedBinding();
			
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue(pInStatus, status,OracleTypes.INTEGER);
			in.addValue(pInPrio, priority,OracleTypes.INTEGER);
			in.addValue(pInDesc, descripcion,OracleTypes.VARCHAR);
			
			

	        if (LOGGER.isDebugEnabled()) {
	            LOGGER.debug(" getCatalog CALL FUNCTION ");
	        }
	        	Map<String, Object> out = simpleJdbcCall.execute(in);
	        	Object resp = out.get(Cursor);
	        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
	        	
	        	final GsonBuilder gsonBuilder = new GsonBuilder();
	        	final Gson gson = gsonBuilder.create();

	        	MotivosCancelResp[] testCase = gson.fromJson(JsonFolio, MotivosCancelResp[].class);
	        	SYS_REFCURSOR= testCase;
	        	LOGGER.error("Objeto serializado: " +SYS_REFCURSOR);
		  
		}else {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	        		.withSchemaName(schema)
	        		.withCatalogName(pkg)
	                .withFunctionName(function)
	                .declareParameters(new SqlOutParameter(Cursor, OracleTypes.CURSOR))
	                .withNamedBinding();

	        if (LOGGER.isDebugEnabled()) {
	            LOGGER.debug(" getCatalog CALL FUNCTION ");
	        }
	        
	        	Map<String, Object> out = simpleJdbcCall.execute();
	        	Object resp = out.get(Cursor);
	        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
	        	
	        	final GsonBuilder gsonBuilder = new GsonBuilder();
	        	final Gson gson = gsonBuilder.create();

	        	MotivosCancelResp[] testCase = gson.fromJson(JsonFolio, MotivosCancelResp[].class);
	        	SYS_REFCURSOR= testCase;
	        	LOGGER.error("Objeto serializado: " +SYS_REFCURSOR);
		}
	        	
	        	return SYS_REFCURSOR;
		
	}


	@Override
	public TrazaResp[] dmlTraza(String function, String pkg, Map<String, String> mapOut, Map<String, String> mapIn)
			throws SQLException {
				
		TrazaResp[] SYS_REFCURSOR = null;
		
		if(function.equals("FNC_CONSULTA_TRAZA") ) {
			String pa_folio =mapIn.get("pa_cve_folio");
			String pa_fecha =mapIn.get("pa_fec_trn");
			
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	        		.withSchemaName(schema)
	        		.withCatalogName(pkg)
	                .withFunctionName(function)
	                .declareParameters(new SqlOutParameter(Cursor, OracleTypes.CURSOR))
	                .declareParameters(
	                		new SqlParameter(pINFolio,OracleTypes.VARCHAR))
	                .declareParameters(
	                		new SqlParameter(pInFchTraza,OracleTypes.VARCHAR))
	                .withNamedBinding();
			
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue(pINFolio, pa_folio,OracleTypes.VARCHAR);
			in.addValue(pInFchTraza, pa_fecha,OracleTypes.VARCHAR);
			
	        	Map<String, Object> out = simpleJdbcCall.execute(in);
	        	Object resp = out.get(Cursor);
	        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
	        	
	        	final GsonBuilder gsonBuilder = new GsonBuilder();
	        	final Gson gson = gsonBuilder.create();

	        	TrazaResp[] testCase = gson.fromJson(JsonFolio, TrazaResp[].class);
	        	SYS_REFCURSOR= testCase;
	        	LOGGER.error("Objeto serializado: " +SYS_REFCURSOR);
		  
		}else if ((function.equals("FNC_GUARDA_TRAZA") )) {
			
			String canal="pa_ide_canal";    
			String modulo="pa_ide_modulo";
			String operacion="pa_ide_tipo_operacion";
			String conId="pa_conn_id";
			String cliente="pa_txt_ip_cliente";
			String ejecutivo="pa_cve_ejecutivo";
			String usuario="pa_cve_usuario"; 
			String user="pa_cve_usr_local";
			String status="pa_ide_status";  
			String folioRtdc="pa_cve_folio";
			String numclie="pa_cve_buc";

			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	        		.withSchemaName(schema)
	        		.withCatalogName(pkg)
	                .withFunctionName(function)
	                .declareParameters(new SqlOutParameter(Cursor, OracleTypes.CURSOR))
	                .declareParameters(new SqlParameter(canal,OracleTypes.INTEGER))
	                .declareParameters(new SqlParameter(modulo,OracleTypes.INTEGER))
	                .declareParameters(new SqlParameter(operacion,OracleTypes.INTEGER))
	                .declareParameters(new SqlParameter(conId,OracleTypes.VARCHAR))
	                .declareParameters(new SqlParameter(cliente,OracleTypes.VARCHAR))
	                .declareParameters(new SqlParameter(ejecutivo,OracleTypes.VARCHAR))
	                .declareParameters(new SqlParameter(usuario,OracleTypes.VARCHAR))
	                .declareParameters(new SqlParameter(user,OracleTypes.VARCHAR))
	                .declareParameters(new SqlParameter(status,OracleTypes.INTEGER))
	                .declareParameters(new SqlParameter(folioRtdc,OracleTypes.VARCHAR))
	                .declareParameters(new SqlParameter(numclie,OracleTypes.VARCHAR))
	                .withNamedBinding();
			
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue(canal, mapIn.get("IDE_CANAL"),OracleTypes.INTEGER);
			in.addValue(modulo, mapIn.get("IDE_MODULO" ),OracleTypes.INTEGER);
			in.addValue(operacion, mapIn.get("IDE_TIPO_OPERACION"),OracleTypes.INTEGER);
			in.addValue(conId, mapIn.get("CONN_ID"),OracleTypes.VARCHAR);
			in.addValue(cliente, mapIn.get("TXT_IP_CLIENTE"),OracleTypes.VARCHAR);
			in.addValue(ejecutivo, mapIn.get("CVE_EJECUTIVO"),OracleTypes.VARCHAR);
			in.addValue(usuario, mapIn.get("CVE_USUARIO"),OracleTypes.VARCHAR);
			in.addValue(user, mapIn.get("CVE_USR_LOCAL"),OracleTypes.VARCHAR);
			in.addValue(status, mapIn.get("IDE_STATUS"),OracleTypes.INTEGER);
			in.addValue(folioRtdc, mapIn.get("CVE_FOLIO"),OracleTypes.VARCHAR);
			in.addValue(numclie, mapIn.get("CVE_BUC"),OracleTypes.VARCHAR);

			Map<String, Object> out = simpleJdbcCall.execute(in);
	        	Object resp = out.get(Cursor);
	        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
	        	
	        	final GsonBuilder gsonBuilder = new GsonBuilder();
	        	final Gson gson = gsonBuilder.create();

	        	TrazaResp[] trazaresp = gson.fromJson(JsonFolio, TrazaResp[].class);
	        	SYS_REFCURSOR= trazaresp;
	        	LOGGER.error("Objeto serializado: " +SYS_REFCURSOR);
	        	
		}
	        	
	        	return SYS_REFCURSOR;
	
	}


	@Override
	public TrazaResp[] dmlTrazaUpd(String function, String pkg, Map<String, String> mapOut, Map<String, String> mapIn)throws SQLException {
		TrazaResp[] SYS_REFCURSOR = null;

		
		if ((function.equals("FNC_ACTUALIZA_TRAZA") )) {
			String traza="pa_ide_traza"; 
			String canal="pa_ide_canal";    
			String modulo="pa_ide_modulo";
			String operacion="pa_ide_tipo_operacion";
			String conId="pa_conn_id";
			String fec_tran="pa_fec_trn";
			String cliente="pa_txt_ip_cliente";
			String ejecutivo="pa_cve_ejecutivo";
			String usuario="pa_cve_usuario"; 
			String user="pa_cve_usr_local";
			String status="pa_ide_status";  
			String folioRtdc="pa_cve_folio";
			String numclie="pa_cve_buc";

			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	        		.withSchemaName(schema)
	        		.withCatalogName(pkg)
	                .withFunctionName(function)
	                .declareParameters(new SqlOutParameter(Cursor, OracleTypes.CURSOR))
	                .declareParameters(new SqlParameter(traza,OracleTypes.INTEGER))
	                .declareParameters(new SqlParameter(canal,OracleTypes.INTEGER))
	                .declareParameters(new SqlParameter(modulo,OracleTypes.INTEGER))
	                .declareParameters(new SqlParameter(operacion,OracleTypes.INTEGER))
	                .declareParameters(new SqlParameter(conId,OracleTypes.VARCHAR))
	                .declareParameters(new SqlParameter(fec_tran,OracleTypes.VARCHAR))
	                .declareParameters(new SqlParameter(cliente,OracleTypes.VARCHAR))
	                .declareParameters(new SqlParameter(ejecutivo,OracleTypes.VARCHAR))
	                .declareParameters(new SqlParameter(usuario,OracleTypes.VARCHAR))
	                .declareParameters(new SqlParameter(user,OracleTypes.VARCHAR))
	                .declareParameters(new SqlParameter(status,OracleTypes.INTEGER))
	                .declareParameters(new SqlParameter(folioRtdc,OracleTypes.VARCHAR))
	                .declareParameters(new SqlParameter(numclie,OracleTypes.VARCHAR))
	                .withNamedBinding();
			
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue(traza, mapIn.get("IDE_TRAZA"),OracleTypes.INTEGER);
			in.addValue(canal, mapIn.get("IDE_CANAL"),OracleTypes.INTEGER);
			in.addValue(modulo, mapIn.get("IDE_MODULO" ),OracleTypes.INTEGER);
			in.addValue(operacion, mapIn.get("IDE_TIPO_OPERACION"),OracleTypes.INTEGER);
			in.addValue(conId, mapIn.get("CONN_ID"),OracleTypes.VARCHAR);
			in.addValue(fec_tran, mapIn.get("FEC_TRAN"),OracleTypes.VARCHAR);
			in.addValue(cliente, mapIn.get("TXT_IP_CLIENTE"),OracleTypes.VARCHAR);
			in.addValue(ejecutivo, mapIn.get("CVE_EJECUTIVO"),OracleTypes.VARCHAR);
			in.addValue(usuario, mapIn.get("CVE_USUARIO"),OracleTypes.VARCHAR);
			in.addValue(user, mapIn.get("CVE_USR_LOCAL"),OracleTypes.VARCHAR);
			in.addValue(status, mapIn.get("IDE_STATUS"),OracleTypes.INTEGER);
			in.addValue(folioRtdc, mapIn.get("CVE_FOLIO"),OracleTypes.VARCHAR);
			in.addValue(numclie, mapIn.get("CVE_BUC"),OracleTypes.VARCHAR);

			Map<String, Object> out = simpleJdbcCall.execute(in);
	        	Object resp = out.get(Cursor);
	        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
	        	
	        	final GsonBuilder gsonBuilder = new GsonBuilder();
	        	final Gson gson = gsonBuilder.create();

	        	TrazaResp[] trazaresp = gson.fromJson(JsonFolio, TrazaResp[].class);
	        	SYS_REFCURSOR= trazaresp;
	        	LOGGER.error("Objeto serializado: " +SYS_REFCURSOR);
			
		}
		return SYS_REFCURSOR;
	}


	@Override
	public BenefitsResp[] dmlBenefits(String function, String pkg, Map<String, String> mapOut,	Map<String, String> mapIn) throws SQLException{
		BenefitsResp[] SYS_REFCURSOR = null;
		if ((function.equals("FNC_GUARDA_BENEFICIO") )) {
			String producto="PA_IDE_PRODUCTO"; 
			String subprod="PA_IDE_SUB_PRODUCTO";    
			String des_prod="PA_DES_PRODUCTO";
			String des_prod_red="PA_DES_PRODUCTO_RED";
			String desc_benefit="PA_DES_BENEFICIOS";

			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	        		.withSchemaName(schema)
	        		.withCatalogName(pkg)
	                .withFunctionName(function)
	                .declareParameters(new SqlOutParameter(Cursor, OracleTypes.CURSOR))
	                .declareParameters(new SqlParameter(producto,OracleTypes.INTEGER))
	                .declareParameters(new SqlParameter(subprod,OracleTypes.INTEGER))
	                .declareParameters(new SqlParameter(des_prod,OracleTypes.VARCHAR))
	                .declareParameters(new SqlParameter(des_prod_red,OracleTypes.VARCHAR))
	                .declareParameters(new SqlParameter(desc_benefit,OracleTypes.VARCHAR))
	                .withNamedBinding();
			
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue(producto, mapIn.get("id_prod"),OracleTypes.INTEGER);
			in.addValue(subprod, mapIn.get("id_subprod"),OracleTypes.INTEGER);
			in.addValue(des_prod, mapIn.get("desc_prod" ),OracleTypes.VARCHAR);
			in.addValue(des_prod_red, mapIn.get("des_prod_red"),OracleTypes.VARCHAR);
			in.addValue(desc_benefit, mapIn.get("desc_benefit"),OracleTypes.VARCHAR);

			Map<String, Object> out = simpleJdbcCall.execute(in);
	        	Object resp = out.get(Cursor);
	        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
	        	
	        	final GsonBuilder gsonBuilder = new GsonBuilder();
	        	final Gson gson = gsonBuilder.create();

	        	BenefitsResp[] trazaresp = gson.fromJson(JsonFolio, BenefitsResp[].class);
	        	SYS_REFCURSOR= trazaresp;
	        	LOGGER.error("Objeto serializado: " +SYS_REFCURSOR);
			
		}else if((function.equals("FNC_CONSULTA_BENEFICIOS"))) {
			String producto="pa_ide_producto";    
			String subprod="pa_ide_sub_producto";

			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	        		.withSchemaName(schema)
	        		.withCatalogName(pkg)
	                .withFunctionName(function)
	                .declareParameters(new SqlOutParameter(Cursor, OracleTypes.CURSOR))
	                .declareParameters(new SqlParameter(producto,OracleTypes.INTEGER))
	                .declareParameters(new SqlParameter(subprod,OracleTypes.INTEGER))
	                .withNamedBinding();
			
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue(producto, mapIn.get("pa_ide_producto"),OracleTypes.INTEGER);
			in.addValue(subprod, mapIn.get("pa_ide_sub_producto" ),OracleTypes.INTEGER);

			Map<String, Object> out = simpleJdbcCall.execute(in);
	        	Object resp = out.get(Cursor);
	        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
	        	final GsonBuilder gsonBuilder = new GsonBuilder();
	        	final Gson gson = gsonBuilder.create();
	        	BenefitsResp[] benefitReq = gson.fromJson(JsonFolio, BenefitsResp[].class);
	        	SYS_REFCURSOR= benefitReq;
	        	LOGGER.error("Objeto serializado: " +SYS_REFCURSOR);		
		}
		
		return SYS_REFCURSOR;
	}


	@Override
	public BenefitsResp[] dmlBenefitsUpd(String function, String pkg, Map<String, String> mapOut,Map<String, String> mapIn)throws SQLException {
		BenefitsResp[] SYS_REFCURSOR = null;
		if ((function.equals("FNC_ACTUALIZA_BENEFICIO") )) {
			String producto="PA_IDE_PRODUCTO"; 
			String subprod="PA_IDE_SUB_PRODUCTO";    
			String des_prod="PA_DES_PRODUCTO";
			String des_prod_red="PA_DES_PRODUCTO_RED";
			String desc_benefit="PA_DES_BENEFICIOS";

			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	        		.withSchemaName(schema)
	        		.withCatalogName(pkg)
	                .withFunctionName(function)
	                .declareParameters(new SqlOutParameter(Cursor, OracleTypes.CURSOR))
	                .declareParameters(new SqlParameter(producto,OracleTypes.INTEGER))
	                .declareParameters(new SqlParameter(subprod,OracleTypes.INTEGER))
	                .declareParameters(new SqlParameter(des_prod,OracleTypes.VARCHAR))
	                .declareParameters(new SqlParameter(des_prod_red,OracleTypes.VARCHAR))
	                .declareParameters(new SqlParameter(desc_benefit,OracleTypes.VARCHAR))
	                .withNamedBinding();
			
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue(producto, mapIn.get("id_prod"),OracleTypes.INTEGER);
			in.addValue(subprod, mapIn.get("id_subprod"),OracleTypes.INTEGER);
			in.addValue(des_prod, mapIn.get("desc_prod" ),OracleTypes.VARCHAR);
			in.addValue(des_prod_red, mapIn.get("des_prod_red"),OracleTypes.VARCHAR);
			in.addValue(desc_benefit, mapIn.get("desc_benefit"),OracleTypes.VARCHAR);

			Map<String, Object> out = simpleJdbcCall.execute(in);
        	Object resp = out.get(Cursor);
        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
        	final GsonBuilder gsonBuilder = new GsonBuilder();
        	final Gson gson = gsonBuilder.create();
        	BenefitsResp[] trazaresp = gson.fromJson(JsonFolio, BenefitsResp[].class);
        	SYS_REFCURSOR= trazaresp;
        	LOGGER.error("Objeto serializado: " +SYS_REFCURSOR);
			
		}else if((function.equals("FNC_ELIMINA_BENEFICIO"))) {
			String producto="PA_IDE_PRODUCTO";    
			String subprod="PA_IDE_SUB_PRODUCTO";

			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	        		.withSchemaName(schema)
	        		.withCatalogName(pkg)
	                .withFunctionName(function)
	                .declareParameters(new SqlOutParameter(Cursor, OracleTypes.CURSOR))
	                .declareParameters(new SqlParameter(producto,OracleTypes.INTEGER))
	                .declareParameters(new SqlParameter(subprod,OracleTypes.INTEGER))
	                .withNamedBinding();
			
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue(producto, mapIn.get("PA_IDE_PRODUCTO"),OracleTypes.INTEGER);
			in.addValue(subprod, mapIn.get("PA_IDE_SUB_PRODUCTO" ),OracleTypes.INTEGER);

			Map<String, Object> out = simpleJdbcCall.execute(in);
	        	Object resp = out.get(Cursor);
	        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
	        	final GsonBuilder gsonBuilder = new GsonBuilder();
	        	final Gson gson = gsonBuilder.create();
	        	BenefitsResp[] benefitReq = gson.fromJson(JsonFolio, BenefitsResp[].class);
	        	SYS_REFCURSOR= benefitReq;
	        	LOGGER.error("Objeto serializado: " +SYS_REFCURSOR);		
		}
		
		return SYS_REFCURSOR;
	}


	@Override
	public MessageResp[] motivoToTdc(DmlBean bean) throws SQLException {
		MessageResp[] SYS_REFCURSOR = null;
		if(bean.getFuncion().equals("FNC_RET_GUARDA_TDC")) {
			 Map<String, String> paramEnt = bean.getParamsIn();
			 
			 String folio ="pa_cve_folio";
			 String num_tdc ="pa_num_tarjeta";
			 String saldo ="pa_imp_saldo";
			 String limite_credito ="pa_imp_limite_credito";
			 String fec_vigencia="pa_fec_vigencia";
			 String fec_antiguedad="pa_fec_antiguedad";
			 String status="pa_ind_status_tarjeta";
			 String tipo_tarjeta="pa_ind_tipo_tarjeta";
			 String id_producto="pa_ide_producto";
			 String id_subproducto="pa_ide_sub_producto";
			 String id_cod_bloqueo="pa_ide_codigo_bloqueo";
			 String id_mot_llamada="pa_ide_motLlamada";
			 String ind_status_retencion="pa_ind_status_retencion";
			 String mto_recibido="pa_mto_recibido";
			 String fec_alta_tdc="pa_fecha_alta_TDC";
			 String fec_limite_credito="pa_fecha_limite_pago";
			 String num_contrato="pa_numero_contrato";
			 String tdc_origen="pa_num_tarjeta_origen";
			 
			   
			    SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		        		.withSchemaName(schema)
		                .withFunctionName(bean.getFuncion())
		                .declareParameters(new SqlOutParameter(Cursor, OracleTypes.CURSOR))
		                .withNamedBinding();
			    
				MapSqlParameterSource in = new MapSqlParameterSource();
				in.addValue(folio, paramEnt.get(folio),OracleTypes.VARCHAR);
				in.addValue(num_tdc, paramEnt.get(num_tdc),OracleTypes.VARCHAR);
				in.addValue(saldo, paramEnt.get(saldo),OracleTypes.DOUBLE);
				in.addValue(limite_credito, paramEnt.get(limite_credito),OracleTypes.DOUBLE);
				in.addValue(fec_vigencia, paramEnt.get(fec_vigencia),OracleTypes.VARCHAR);
				in.addValue(fec_antiguedad, paramEnt.get(fec_antiguedad),OracleTypes.VARCHAR);
				in.addValue(status, paramEnt.get(status),OracleTypes.NUMBER);
				in.addValue(tipo_tarjeta, paramEnt.get(tipo_tarjeta),OracleTypes.NUMBER);
				in.addValue(id_producto, paramEnt.get(id_producto),OracleTypes.NUMBER);
				in.addValue(id_subproducto, paramEnt.get(id_subproducto),OracleTypes.NUMBER);
				in.addValue(id_cod_bloqueo, paramEnt.get(id_cod_bloqueo),OracleTypes.NUMBER);
				in.addValue(id_mot_llamada, paramEnt.get(id_mot_llamada),OracleTypes.NUMBER);
				in.addValue(ind_status_retencion, paramEnt.get(ind_status_retencion),OracleTypes.NUMBER);
				in.addValue(mto_recibido, paramEnt.get(mto_recibido),OracleTypes.DOUBLE);
				in.addValue(fec_alta_tdc, paramEnt.get(fec_alta_tdc),OracleTypes.VARCHAR);
				in.addValue(fec_limite_credito, paramEnt.get(fec_limite_credito),OracleTypes.VARCHAR);
				in.addValue(num_contrato, paramEnt.get(num_contrato),OracleTypes.VARCHAR);
				in.addValue(tdc_origen, paramEnt.get(tdc_origen),OracleTypes.NUMBER);


		        	Map<String, Object> out = simpleJdbcCall.execute(in);
		        	Object resp = out.get(Cursor);
		        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
		        	
		        	final GsonBuilder gsonBuilder = new GsonBuilder();
		        	final Gson gson = gsonBuilder.create();

		        	MessageResp[] request = gson.fromJson(JsonFolio, MessageResp[].class);
		        	SYS_REFCURSOR= request;
		        	LOGGER.error("Objeto serializado: " +SYS_REFCURSOR);
		        
		}else if(bean.getFuncion().equals("FNC_RET_AGREGA_MOTIVOS")) {
			
			Map<String, String> paramEnt = bean.getParamsIn();
			 
			 Object pa1 = paramEnt.keySet().toArray()[0];
			 Object pa2 = paramEnt.keySet().toArray()[1];
			 Object pa3 = paramEnt.keySet().toArray()[2];
			 Object pa4 = paramEnt.keySet().toArray()[3];	 
			   
			    SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		        		.withSchemaName(schema)
		                .withFunctionName(bean.getFuncion())
		                .declareParameters(new SqlOutParameter(Cursor, OracleTypes.CURSOR))
		                .declareParameters(
		                		new SqlParameter(pa1.toString(),OracleTypes.VARCHAR))
		                .declareParameters(
		                		new SqlParameter(pa2.toString(),OracleTypes.VARCHAR))
		                .declareParameters(
		                		new SqlParameter(pa3.toString(),OracleTypes.VARCHAR))
		                .declareParameters(
		                		new SqlParameter(pa4.toString(),OracleTypes.VARCHAR))
		                .withNamedBinding();
			    
				MapSqlParameterSource in = new MapSqlParameterSource();
				in.addValue(pa1.toString(), paramEnt.get(pa1.toString()),OracleTypes.VARCHAR);
				in.addValue(pa2.toString(), paramEnt.get(pa2.toString()),OracleTypes.VARCHAR);
				in.addValue(pa3.toString(), paramEnt.get(pa3.toString()),OracleTypes.VARCHAR);
				in.addValue(pa4.toString(), paramEnt.get(pa4.toString()),OracleTypes.VARCHAR);

		        	
		        	Map<String, Object> out = simpleJdbcCall.execute(in);
		        	Object resp = out.get(Cursor);
		        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
		        	
		        	final GsonBuilder gsonBuilder = new GsonBuilder();
		        	final Gson gson = gsonBuilder.create();

		        	MessageResp[] request = gson.fromJson(JsonFolio, MessageResp[].class);
		        	SYS_REFCURSOR= request;
		        	LOGGER.error("Objeto serializado: " +SYS_REFCURSOR);
		}
		
		return SYS_REFCURSOR;
	}


	@Override
	public MessageResp[] dmlScripts(DmlBean bean) throws SQLException {
		   
		MessageResp[] SYS_REFCURSOR = null;
		Map<String, String> paramEnt = bean.getParamsIn();
		
		
				String 	cve_folio ="pa_cve_folio";
				String 	nombre_Ejecutivo ="pa_cve_ejecutivo";
				String 	nombre_Cliente ="pa_nombre_Cliente";
				String 	num_Tarjeta ="pa_num_Tarjeta";
				String 	vigencia_Tarjeta ="pa_vigencia_Tarjeta";
				String 	script ="pa_script";
		   
		    SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	        		.withSchemaName(schema)
	                .withFunctionName(bean.getFuncion())
	                .declareParameters(new SqlOutParameter(Cursor, OracleTypes.CURSOR))
	                .declareParameters(new SqlParameter(cve_folio,OracleTypes.VARCHAR))
	                .declareParameters(new SqlParameter(nombre_Ejecutivo,OracleTypes.VARCHAR))
	                .declareParameters(new SqlParameter(nombre_Cliente,OracleTypes.VARCHAR))
	                .declareParameters(new SqlParameter(num_Tarjeta,OracleTypes.VARCHAR))
	                .declareParameters(new SqlParameter(vigencia_Tarjeta,OracleTypes.VARCHAR))
	                .declareParameters(new SqlParameter(script,OracleTypes.NUMBER))
	                .withNamedBinding();
		    
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue(cve_folio, paramEnt.get("cve_folio"),OracleTypes.VARCHAR);
			in.addValue(nombre_Ejecutivo, paramEnt.get("cve_Ejecutivo"),OracleTypes.VARCHAR);
			in.addValue(nombre_Cliente, paramEnt.get("nombre_Cliente"),OracleTypes.VARCHAR);
			in.addValue(num_Tarjeta, paramEnt.get("num_Tarjeta"),OracleTypes.VARCHAR);
			in.addValue(vigencia_Tarjeta, paramEnt.get("vigencia_Tarjeta"),OracleTypes.VARCHAR);
			in.addValue(script, paramEnt.get("script"),OracleTypes.VARCHAR);

	        	Map<String, Object> out = simpleJdbcCall.execute(in);
	        	Object resp = out.get(Cursor);
	        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
	        	
	        	final GsonBuilder gsonBuilder = new GsonBuilder();
	        	final Gson gson = gsonBuilder.create();

	        	MessageResp[] response = gson.fromJson(JsonFolio, MessageResp[].class);
	        	SYS_REFCURSOR= response;
	        return SYS_REFCURSOR;
	}
	
	
	public MessageResp[] dmlMotivosCall(DmlBean bean) throws SQLException {
		MessageResp[] SYS_REFCURSOR = null;
		
		 Map<String, String> paramEnt = bean.getParamsIn();
		 Object pa1 = paramEnt.keySet().toArray()[0];
		   
		    SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	        		.withSchemaName(schema)
	                .withFunctionName(bean.getFuncion())
	                .declareParameters(new SqlOutParameter(Cursor, OracleTypes.CURSOR))
	                .declareParameters(new SqlParameter(pa1.toString(),OracleTypes.NUMBER))

	                .withNamedBinding();
		    
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue(pa1.toString(), paramEnt.get(pa1.toString()),OracleTypes.VARCHAR);


	        	Map<String, Object> out = simpleJdbcCall.execute(in);
	        	Object resp = out.get(Cursor);
	        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
	        	
	        	final GsonBuilder gsonBuilder = new GsonBuilder();
	        	final Gson gson = gsonBuilder.create();

	        	MessageResp[] response = gson.fromJson(JsonFolio, MessageResp[].class);
	        	SYS_REFCURSOR= response;		
		return SYS_REFCURSOR;
		
	}
	
	
}


