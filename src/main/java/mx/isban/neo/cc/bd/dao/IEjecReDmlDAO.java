/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * IEjecucionDmlDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2019-06-25                               Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc.bd.dao;


import java.sql.SQLException;

import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.AdicionalResp;
import mx.isban.neo.cc.model.response.CodesPendingResp;
import mx.isban.neo.cc.model.response.MessageRespCancel;
import mx.isban.neo.cc.tenencias.model.LimiteResp;


/**
 * Type class Interfaz .<br>
 * Main objective: Definir la listado de los metodos de la clase de tipo DAO de EjecucionDmlDAO <br>
 * Contains methods:
 * <ul>
 * <li>accessDml</li>
 * </ul>
 * @author Isban / 
 * IEjecucionDmlDAO.java by Z266957
 *
 */
public interface IEjecReDmlDAO {

	/**
	 * Method dmlUpdate
	 * Method objective: ___ .<br>
	 * @param nameQuery query a ejecutar
	 * @param paramsQuery parametros de entrada para el insert, update o delete
	 * @return List<Map<String,Object>> listado de mapa de datos
	 */
    
    public CodesPendingResp[] dmlSegArguments(DmlBean bean) throws SQLException;
    
    /**
	 * Method dmlUpdate
	 * Method objective: ___ .<br>
	 * @param nameQuery query a ejecutar
	 * @param paramsQuery parametros de entrada para el insert, update o delete
	 * @return List<Map<String,Object>> listado de mapa de datos
	 */
    
    public LimiteResp[] dmlLimite(DmlBean bean) throws SQLException;
    
    /**
	 * Method dmlUpdate
	 * Method objective: ___ .<br>
	 * @param nameQuery query a ejecutar
	 * @param paramsQuery parametros de entrada para el insert, update o delete
	 * @return List<Map<String,Object>> listado de mapa de datos
	 */
    public MessageRespCancel[] dmlCancel(DmlBean bean) throws SQLException;

    
    /**
	 * Method dmlUpdate
	 * Method objective: ___ .<br>
	 * @param nameQuery query a ejecutar
	 * @param paramsQuery parametros de entrada para el insert, update o delete
	 * @return List<Map<String,Object>> listado de mapa de datos
	 */
    public AdicionalResp[] dmlExeAdic(DmlBean bean) throws SQLException;


    /**
	 * Method dmlUpdate
	 * Method objective: ___ .<br>
	 * @param nameQuery query a ejecutar
	 * @param paramsQuery parametros de entrada para el insert, update o delete
	 * @return List<Map<String,Object>> listado de mapa de datos
	 */
    public AdicionalResp[] dmlCelEmail(DmlBean bean) throws SQLException;

    

    
}
