/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * IEjecucionDmlDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2019-06-25                               Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc.bd.dao;


import java.sql.SQLException;

import java.util.Map;


import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.BenefitsResp;
import mx.isban.neo.cc.model.response.CodesResponse;
import mx.isban.neo.cc.model.response.FolioResponse;
import mx.isban.neo.cc.model.response.MessageResp;
import mx.isban.neo.cc.model.response.MotivosCancelResp;
import mx.isban.neo.cc.model.response.TrazaResp;



/**
 * Type class Interfaz .<br>
 * Main objective: Definir la listado de los metodos de la clase de tipo DAO de EjecucionDmlDAO <br>
 * Contains methods:
 * <ul>
 * <li>accessDml</li>
 * </ul>
 * @author Isban / iscontreras
 * IEjecucionDmlDAO.java by Z055749
 *
 */
 public interface IEjecucionDmlDAO {

	/**
	 * Method dmlUpdate
	 * Method objective: ___ .<br>
	 * @param nameQuery query a ejecutar
	 * @param paramsQuery parametros de entrada para el insert, update o delete
	 * @return List<Map<String,Object>> listado de mapa de datos
	 */
	 FolioResponse[] getNewFolio(DmlBean bean) throws SQLException;
	
	 CodesResponse[] getCodigosBloqueo(String code) throws SQLException;
	
	 MotivosCancelResp[] getCatalog(String function, String pkg, Map<String, String> map,Map<String, String> map2)throws SQLException;
	
	 TrazaResp[] dmlTraza(String function, String pkg, Map<String, String> map,Map<String, String> map2)throws SQLException;
	
	 TrazaResp[] dmlTrazaUpd(String function, String pkg, Map<String, String> mapOut, Map<String, String> mapIn)throws SQLException;
	
	 BenefitsResp[] dmlBenefits(String function, String pkg, Map<String, String> mapOut, Map<String, String> mapIn)throws SQLException;
	
	 BenefitsResp[] dmlBenefitsUpd(String function, String pkg, Map<String, String> mapOut, Map<String, String> mapIn)throws SQLException;
	
     MessageResp[] motivoToTdc(DmlBean bean) throws SQLException;
    
     MessageResp[] dmlScripts(DmlBean bean) throws SQLException;
    
     MessageResp[] dmlMotivosCall(DmlBean bean) throws SQLException;





}
