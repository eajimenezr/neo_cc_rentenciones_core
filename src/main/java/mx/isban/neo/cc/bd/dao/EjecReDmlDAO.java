/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * EjecucionDmlDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2019-06-25                               Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc.bd.dao;
import java.sql.SQLException;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mx.isban.neo.cc.cancel.service.ICancelService;
import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.AdicionalResp;
import mx.isban.neo.cc.model.response.CancelAdicResp;
import mx.isban.neo.cc.model.response.CancelTitularResp;
import mx.isban.neo.cc.model.response.CodesPendingResp;
import mx.isban.neo.cc.model.response.MessageRespCancel;
import mx.isban.neo.cc.model.response.TdcCanceladasResp;
import mx.isban.neo.cc.notificaciones.service.IPendingService;
import mx.isban.neo.cc.tenencias.model.LimiteResp;
import oracle.jdbc.OracleTypes;

/**
 * Clase tipo data access object para ejecutar consultas de forma generica a una bd con el Prepared Statement Setter.<br>
 * Contiene los metodos siguientes:<br>
 * <ul>
 * <li>accessDml</li>
 * <li>setValuesPS</li>
 * </ul>
 * Variables globales:<br>
 * <ul>
 * <li>jdbcTemplate</li>
 * <li>codError</li>
 * </ul>
 * @author Isban / iscontreras
 * Class EjecucionDmlDAO.java 
 */
@Lazy
@Repository
public class EjecReDmlDAO implements IEjecReDmlDAO {
	/**
	 * Constante para trazar la aplicacion.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(EjecReDmlDAO.class);
	/**
	 *  JdbcTemplate jdbcTemplate: inyeccion del bean jdbcTemplate
	 */
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	/**
	 *  instancia para usar servicio
	 */
	@Autowired
	private ICancelService cancelService;
	
	
	/**
	 *   jdbcTemplate: inyeccion de clase PendingService
	 */
	@Autowired
	private IPendingService pendingService;
	
	/**
	 *  Integer dbTimeOut : tiempo de expiracion en la conexin a la BD
	 */
	@Value("${bd.values.schema}")
	private String schema;
	
	@Value("${bd.values.typecursor}")
	private String cursor;
	

	@Override
	public LimiteResp[] dmlLimite(DmlBean bean) throws SQLException {
		LimiteResp[] SYS_REFCURSOR = null;
		
		 Map<String, String> paramEnt = bean.getParamsIn();
		 Object pa1 = paramEnt.keySet().toArray()[0];
		   
		    SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	        		.withSchemaName(schema)
	                .withFunctionName(bean.getFuncion())
	                .declareParameters(new SqlOutParameter(cursor, OracleTypes.CURSOR))
	                .withNamedBinding();
		    
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue(pa1.toString(), paramEnt.get(pa1.toString()),OracleTypes.VARCHAR);
	
	
	        	Map<String, Object> out = simpleJdbcCall.execute(in);
	        	Object resp = out.get(cursor);
	        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
	        	
	        	final GsonBuilder gsonBuilder = new GsonBuilder();
	        	final Gson gson = gsonBuilder.create();
	
	        	LimiteResp[] response = gson.fromJson(JsonFolio, LimiteResp[].class);
	        	SYS_REFCURSOR= response;		
		return SYS_REFCURSOR;
	}

	@Override
	public CodesPendingResp[] dmlSegArguments(DmlBean bean) throws SQLException {
		CodesPendingResp[] SYS_REFCURSOR = null;
		
		Map<String, String> paramEnt = bean.getParamsIn();
		 
		String 	cve_folio ="pa_ide_tarjeta";
		String 	accion ="pa_accion"; 
	
		
		 SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	        		.withSchemaName(schema)
	        		.withCatalogName(bean.getPkg())
	                .withFunctionName(bean.getFuncion())
	                .declareParameters(new SqlOutParameter(cursor, OracleTypes.CURSOR))
	                .withNamedBinding();
		    
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue(cve_folio, paramEnt.get(cve_folio),OracleTypes.VARCHAR);
			in.addValue(accion, paramEnt.get(accion),OracleTypes.NUMBER);
	
	        	Map<String, Object> out = simpleJdbcCall.execute(in);
	        	Object resp = out.get(cursor);
	        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
	        	
	        	final GsonBuilder gsonBuilder = new GsonBuilder();
	        	final Gson gson = gsonBuilder.create();
	
	        	CodesPendingResp[] response = gson.fromJson(JsonFolio, CodesPendingResp[].class);
	    
	        	pendingService.codesPending(response);
	
	        	SYS_REFCURSOR= response;
		
		
		
		return SYS_REFCURSOR;
	}
	
	@Override
	public MessageRespCancel[] dmlCancel(DmlBean bean) throws SQLException {
		MessageRespCancel[] SYS_REFCURSOR = null;
		
			Map<String, String> paramEnt = bean.getParamsIn();
			 
			String 	cve_folio ="pa_cve_folio";
			String 	num_Tarjeta ="pa_num_tarjeta"; 
			String 	id_valida ="pa_idValidacion";
			String 	id_resp ="pa_ide_respuesta";
			String 	folio1 ="pa_txt_folioCapturado";
			String 	folio2 ="pa_txt_folioCapturado2";
			String 	operacion ="pa_tipoOperacion";
			
			 SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		        		.withSchemaName(schema)
		                .withFunctionName(bean.getFuncion())
		                .declareParameters(new SqlOutParameter(cursor, OracleTypes.CURSOR))
		                .withNamedBinding();
			    
				MapSqlParameterSource in = new MapSqlParameterSource();
				in.addValue(cve_folio, paramEnt.get(cve_folio),OracleTypes.VARCHAR);
				in.addValue(num_Tarjeta, paramEnt.get(num_Tarjeta),OracleTypes.VARCHAR);
				in.addValue(id_valida, paramEnt.get(id_valida),OracleTypes.NUMBER);
				in.addValue(id_resp, paramEnt.get(id_resp),OracleTypes.NUMBER);
				in.addValue(folio1, paramEnt.get(folio1),OracleTypes.VARCHAR);
				in.addValue(folio2, paramEnt.get(folio2),OracleTypes.VARCHAR);
				in.addValue(operacion, paramEnt.get(operacion),OracleTypes.NUMBER);


		        	Map<String, Object> out = simpleJdbcCall.execute(in);
		        	Object resp = out.get(cursor);
		        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
		        	
		        	final GsonBuilder gsonBuilder = new GsonBuilder();
		        	final Gson gson = gsonBuilder.create();

		        	MessageRespCancel[] response = gson.fromJson(JsonFolio, MessageRespCancel[].class);
		        	
		        	//validar si el codigo de bloqueo es diferente de -1 para poder ejecutar el bloqueo de la TDC 
		        	if(response[0].getCODIGOBLOQUEO() != -1 /**&& response[0].getIDMENSAJE() != 0**/) {
		        		//se manda a cancelar la TDC titular
		        		        		
		        		//crear un objeto que junte a los 2 titual y adicional
		        		  CancelTitularResp tdcTitularCancel = cancelService.cancelaTdcTitular(paramEnt.get(num_Tarjeta),String.valueOf(response[0].getCODIGOBLOQUEO()),paramEnt.get(cve_folio));
		        		//se consultan y mandan a cancelar las adicionales 
		        		 CancelAdicResp adicionales = cancelService.consultaAdicional(paramEnt.get(cve_folio),paramEnt.get(num_Tarjeta));
		        		
		        		 TdcCanceladasResp tdcCancel= new TdcCanceladasResp(); 
		        		 tdcCancel.setTitular(tdcTitularCancel);
		        		 tdcCancel.setAdicionales(adicionales);
		        		 
			        	response[0].setTdc_cancelada(tdcCancel);
		        	}
		        	///validamos si en el response vienen mas de un objeto para mapearlo correctamente
		        	if(response.length > 1) {
		        		response = cancelService.validaResponse(response);
		        		
		        	}
		        	SYS_REFCURSOR= response;
			
			
			
			return SYS_REFCURSOR;
	}



	@Override
	public AdicionalResp[] dmlExeAdic(DmlBean bean) throws SQLException {
		AdicionalResp[] SYS_REFCURSOR = null;
		
		Map<String, String> paramEnt = bean.getParamsIn();
		 
		String 	cve_folio ="pa_cve_folio";
		String 	num_Tarjeta ="pa_num_tarjeta";
		String 	accion ="pa_accion";
		String 	codigoAplicado ="pa_codigoAplicado";

		
		 SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	        		.withSchemaName(schema)
	                .withFunctionName(bean.getFuncion())
	                .declareParameters(new SqlOutParameter(cursor, OracleTypes.CURSOR))
	                .withNamedBinding();
		    
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue(cve_folio, paramEnt.get(cve_folio),OracleTypes.VARCHAR);
			in.addValue(num_Tarjeta, paramEnt.get(num_Tarjeta),OracleTypes.VARCHAR);
			in.addValue(accion, paramEnt.get(accion),OracleTypes.NUMBER);
			in.addValue(codigoAplicado, paramEnt.get(codigoAplicado),OracleTypes.NUMBER);



	        	Map<String, Object> out = simpleJdbcCall.execute(in);
	        	Object resp = out.get(cursor);
	        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
	        	
	        	final GsonBuilder gsonBuilder = new GsonBuilder();
	        	final Gson gson = gsonBuilder.create();

	        	AdicionalResp[] response = gson.fromJson(JsonFolio, AdicionalResp[].class);
	        	
	        	SYS_REFCURSOR= response;
		
		
		
		return SYS_REFCURSOR;
	}



	@Override
	public AdicionalResp[] dmlCelEmail(DmlBean bean) throws SQLException {
		AdicionalResp[] SYS_REFCURSOR = null;
		
		Map<String, String> paramEnt = bean.getParamsIn();
		 
		String 	cve_folio ="pa_cve_folio";
		String 	num_Tarjeta ="pa_num_tarjeta";
		String 	cel ="pa_cel";
		String 	email ="pa_mail";

		
		 SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	        		.withSchemaName(schema)
	                .withFunctionName(bean.getFuncion())
	                .declareParameters(new SqlOutParameter(cursor, OracleTypes.CURSOR))
	                .withNamedBinding();
		    
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue(cve_folio, paramEnt.get(cve_folio),OracleTypes.VARCHAR);
			in.addValue(num_Tarjeta, paramEnt.get(num_Tarjeta),OracleTypes.VARCHAR);
			in.addValue(cel, paramEnt.get(cel),OracleTypes.NUMBER);
			in.addValue(email, paramEnt.get(email),OracleTypes.VARCHAR);



	        	Map<String, Object> out = simpleJdbcCall.execute(in);
	        	Object resp = out.get(cursor);
	        	String JsonFolio = new com.google.gson.Gson().toJson(resp);
	        	
	        	final GsonBuilder gsonBuilder = new GsonBuilder();
	        	final Gson gson = gsonBuilder.create();

	        	AdicionalResp[] response = gson.fromJson(JsonFolio, AdicionalResp[].class);
	        	
	        	SYS_REFCURSOR= response;
		
		
		
		return SYS_REFCURSOR;
	}

	
	
}


