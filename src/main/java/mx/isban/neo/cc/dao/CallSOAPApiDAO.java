/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * CallSOAPApiDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2018-08-15                               Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc.dao;


import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import mx.isban.neo.cc.utils.ConstastCommon;
import mx.isban.neo.cc.utils.Utils;

/**
 * Type class: @Repository .<br>
 * Main objective: Clase de tipo DAO donde se definen los metodos para la gestion de peticiones a servicos web de tipo soap externos y el tratamiento de sus urls .<br>
 * Contains methods:
 * <ul>
 * <li>callWebService</li>
 * <li>encodeURL</li>
 * </ul>
 * Global constants or variables:
 * <ul>
 * <li></li>
 * </ul>
 * @author Isban / iscontreras
 * CallSOAPApiDAO.java by Z055749
 *
 */
@Repository
public class CallSOAPApiDAO extends WebServiceGatewaySupport implements ICallSoapApiDAO{
	/**
	 * log para mostrar informacion en consola
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(CallSOAPApiDAO.class);

	/**
	 * Metodo para enviar informacion a un ws de tipo soap y regresar sus datos
	 * @param url 
	 * 		url del servicio endpoint
	 * @param request
	 * 		mapa de entrada que se enviara al servicio web
	 * @return
	 * 		informacion que retorna la consulta al web service
	 */
	/* (non-Javadoc)
	 * @see mx.isban.neo.cc.dao.ICallSoapApiDAO#callWebService(java.lang.String, java.lang.Object)
	 */
	public Object callWebService(String url, Object request){
		System.setProperty("javax.net.ssl.trustStore", "C:\\certificados\\neoservnegocio_mx_corp.jks");
		System.setProperty("javax.net.ssl.trustStorePassword", "123456");
		LOGGER.trace("ENVIAR marshalSendAndReceive");
		LOGGER.trace("url:"+url);
		LOGGER.trace("request:"+request);
		LOGGER.info("UtilTenencia:"+Utils.getSingletonInstance().objectToJsonString(request));
		 // se ejecuta el request soap
	        return getWebServiceTemplate().marshalSendAndReceive(encodeURL(url) , request);
	  }
	
	/**
	 * Metodo para darle codificacion a una url en foramto UTF8
	 * Methos encodeURL: return type String 
	 * @param url direccion a codificar
	 * @return urlencode 
	 */
	private String encodeURL(String url) {
		String urlencode = ConstastCommon.EMPTY_STRING;
		LOGGER.info("url:"+url);
		try {
			url = url.replace(" ","");
			urlencode = URLDecoder.decode(url, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("UnsupportedEncodingException:"+e.getMessage(),e);
			LOGGER.info("UnsupportedEncodingException:",e);
			urlencode = url;
		}
		LOGGER.info("urlencode:"+urlencode);
	    return urlencode;
	}
}
