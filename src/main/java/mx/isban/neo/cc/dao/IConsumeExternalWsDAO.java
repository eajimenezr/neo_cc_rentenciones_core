/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * IConsumeExternalServices.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2019-10-25       Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc.dao;

import java.util.Map;

import org.springframework.http.HttpMethod;

/**
 * Type class: ____ .<br>
 * Main objective: ___ .<br>
 * Contains gets / sets methods:
 * <ul>
 * <li></li>
 * </ul>
 * Global constants or variables:
 * <ul>
 * <li></li>
 * </ul>
 * @author Isban / iscontreras
 * IConsumeExternalServices.java by Z055749
 *
 */
public interface IConsumeExternalWsDAO {

	/**
	 * Method consumeWs
	 * Method objective: ___ .<br>
	 * @param url : end point del servicio que se consumira
	 * @param requestType : objeto json que recibe
	 * @param pathsVariable : mapa de variables para la url
	 * @param authorization : token de seguridad si es que requiere el servicio
	 * @param httpMethod : tipo de metodo que ejecutiva ( POST,GET, PUT, DELETE )
	 * @return String resultado del cuerpo
	 */
	String consumeWs(String url, Object requestType, Map<String, String> pathsVariable, String authorization,
			HttpMethod httpMethod);

}
