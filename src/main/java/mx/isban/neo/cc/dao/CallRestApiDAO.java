/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * CallRestApiDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2018-08-15                               Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc.dao;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
/**
 * Clase de tipo DAO define los metodos para ejecutar los llamados a otros 
 * servicios rest externos.<br>
 * Contiene los metodos:<br>
 * <Strong>callGet</Strong>
 * <Strong>callPost</Strong>
 * @author Isban / Z046293
 * Class CallRestApiDAO.java 
 */
@Repository
public class CallRestApiDAO implements ICallRestApiDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(CallRestApiDAO.class);
	
	/**
	 *  RestTemplate restTemplate: inyeccion del bean restTemplate
	 */
	@Autowired
	@Qualifier("restTemplateP")
	private RestTemplate restTemplateP;
	
	/**
	 *  RestTemplate restTemplate: inyeccion del bean restTemplate
	 */
	@Autowired
	@Qualifier("restTemplateS")
	private RestTemplate restTemplateS;
	/**
	 * implementacion para invocar un servicio rest pasando solo la url y el tipo de
	 * clase de respuesta
	 * 
	 * @param uri
	 *            url a ejecutar url a ejecutar
	 * @param responseType
	 *            clase para parsear la respuesta clase para parsear la respuesta
	 * 
	 * @return RestTemplate respuesta generica
	 */

	@Override
	public RestTemplate callGet(String uri, Class responseType) {

		return null;
	}

	/**
	 * implementacion para invocar un servicio rest pasando la url ,el tipo de clase
	 * y parametros de consulta de respuesta
	 * 
	 * @param uri
	 *            url a ejecutar
	 * @param responseType
	 *            clase para parsear la respuesta
	 * @param pathsVariable
	 *            path variables a usar en la invocacion
	 * @param queryParams
	 *            parametros para ejecutar la url
	 * 
	 * @return Object respuesta generica
	 */

	@Override
	public Object callGet(String uri, Class responseType, Map<String, String> pathsVariable,
			MultiValueMap<String, String> queryParams) {
		LOGGER.info("INCIAI callGet 4 params",getClass());
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);
		builder.queryParams(queryParams);
	
		LOGGER.debug("ENVIAR restTemplate.getForObject",getClass());
		LOGGER.debug("uri:"+uri,getClass());
		LOGGER.debug("pathsVariable:"+pathsVariable,getClass());
		return restTemplateP.getForObject(builder.buildAndExpand(pathsVariable).toUriString(), responseType);
	}

	/**
	 * implementacion para invocar un servicio rest pasando la url ,el tipo de clase
	 * , los paths variables y parametros de consulta de respuesta
	 * 
	 * @param uri
	 *            url a ejecutar
	 * @param responseType
	 *            clase para parsear la respuesta
	 * @param queryParams
	 *            parametros para ejecutar la url
	 * 
	 * @return Object respuesta generica
	 */
	@Override
	public Object callGet(String uri, Class responseType, MultiValueMap<String, String> queryParams) {
		LOGGER.info("INCIAI callGet 3 params",getClass());
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);
		builder.queryParams(queryParams);

		LOGGER.debug("ENVIAR restTemplate.getForObject",getClass());
		LOGGER.debug("uri:"+uri,getClass());
		LOGGER.debug("pathsVariable:"+queryParams,getClass());
		return restTemplateS.getForObject(builder.toUriString(), responseType);
	}

}
