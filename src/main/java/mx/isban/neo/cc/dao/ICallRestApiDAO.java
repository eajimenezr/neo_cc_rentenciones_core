package mx.isban.neo.cc.dao;

import java.util.Map;

import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * Interface para definir los metodo a la clase de dao call rest api.
 * Contiene los metodos:<br>
 * <Strong>callGet</Strong>
 * <Strong>callPost</Strong>
 * @author Isban / Z046293
 * Class ICallRestApiDAO.java 
 */
public interface ICallRestApiDAO {

	/**
	 * template para invocar un servicio rest pasando solo la url y el tipo de clase
	 * de respuesta
	 * 
	 * @param uri
	 *            url a invocar
	 * @param responseType
	 *            clase con la que se parseara la respuesta
	 * @return RestTemplate respuesta generica
	 */
	RestTemplate callGet(String uri, Class responseType);

	/**
	 * template para invocar un servicio rest pasando la url ,el tipo de clase y
	 * parametros de consulta de respuesta
	 * 
	 * @param uri
	 *            url a ejecutar
	 * @param responseType
	 *            clase con que se parseara la respuesta
	 * @param queryParams
	 *            parametros de consulta
	 * @return Object objeto respuesta
	 * @throws InstantiationException
	 *             excepcion replicada
	 * @throws IllegalAccessException
	 *             excepcion replicada
	 */
	Object callGet(String uri, Class responseType, MultiValueMap<String, String> queryParams);

	/**
	 * template para invocar un servicio rest pasando la url ,el tipo de clase , los
	 * paths variables y parametros de consulta de respuesta
	 * 
	 * @param uri
	 *            url a ejecutar
	 * @param responseType
	 *            clase con que se parseara la respuesta
	 * @param pathsVariable
	 *            paths variables a cubrir
	 * @param queryParams
	 *            parametros de consulta
	 * @return Object objeto respuesta
	 * @throws InstantiationException
	 *             excepcion replicada
	 * @throws IllegalAccessException
	 *             excepcion replicada
	 */
	Object callGet(String uri, Class responseType, Map<String, String> pathsVariable,
			MultiValueMap<String, String> queryParams);

}
