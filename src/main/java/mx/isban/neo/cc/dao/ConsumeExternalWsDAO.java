/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ConsumeExternalServices.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2019-10-25       Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc.dao;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

//import mx.isban.neo.cc.cancel.service.disableSslVerification;
import mx.isban.neo.cc.utils.ConstastCommon;
import mx.isban.neo.cc.utils.Headers;
import mx.isban.neo.cc.utils.Utils;

/**
 * Type class: ____ .<br>
 * Main objective: ___ .<br>
 * Contains gets / sets methods:
 * <ul>
 * <li></li>
 * </ul>
 * Global constants or variables:
 * <ul>
 * <li></li>
 * </ul>
 * @author Isban / iscontreras
 * ConsumeExternalServices.java by Z055749
 *
 */
@Repository
public class ConsumeExternalWsDAO implements IConsumeExternalWsDAO {

	/**
	 * Variable para logeo en consola
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ConsumeExternalWsDAO.class);
	/**
	 *  RestTemplate: inyecta restTemplate
	 */
	@Autowired
	private RestTemplate restTemplate;
	
//	@Autowired
//	private disableSslVerification disabelSSL;
    
	/**
	 * Method consumeWs
	 * Method objective: ___ .<br>
	 * @param url : end point del servicio que se consumira
	 * @param requestType : objeto json que recibe
	 * @param pathsVariable : mapa de variables para la url
	 * @param authorization : token de seguridad si es que requiere el servicio
	 * @param httpMethod : tipo de metodo que ejecutiva ( POST,GET, PUT, DELETE )
	 * @return String resultado del cuerpo
	 */
    @Override
	public String consumeWs(String url,Object requestType,  Map<String, String> pathsVariable,String authorization,HttpMethod httpMethod) {
		LOG.info("INICIA.CONSUME_WEB_SERVICE");
		LOG.trace("::::::::::::::::");
		LOG.trace("url:"+url);
		LOG.trace("pathsVariable:"+pathsVariable);
		LOG.trace("HttpMethod:"+httpMethod);
		LOG.trace("authorization:"+authorization);
		LOG.trace("JSON:"+Utils.getSingletonInstance().objectToJsonString(requestType));
		LOG.trace("::::::::::::::::");
		String responseBody = ConstastCommon.EMPTY_STRING;
		//Inicializamos nuestro objeto
		ResponseEntity<String> responseentity  = null;
		/**Inicializamos nuestro encabezado***/ 
		HttpHeaders headers = new HttpHeaders();
		//Si es distinto de null y vacio authorization(token de seguridad) lo seteamos en la cabecera
		if(StringUtils.isNotBlank(authorization)) {
			headers.set(Headers.AUTHORIZATION.getName(), authorization);
		}
		
		// a la url se le agregan los parametros 
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);
		String finalUrl = ConstastCommon.EMPTY_STRING;
		if(pathsVariable!=null) {
			LOG.info("#Hay parametros para sustituir en la URI#");
			finalUrl = builder.buildAndExpand(pathsVariable).toUriString();
		}else {
			LOG.info("#No hay parametros para sustituir en la URI#");
			finalUrl = builder.buildAndExpand().toUriString();
		}
		url = null;
		// se agregan los headers y los json
		HttpEntity<?> requestEntity = new HttpEntity<>(requestType,headers);	
		//enviamos la peticion
//		disabelSSL.disableSslVerification();
		responseentity = restTemplate.exchange(finalUrl, httpMethod, requestEntity, String.class);
		responseBody = responseentity.getBody();
//		LOG.trace("responseBody:"+responseBody);
		//vaciamos las variables que ya no usamos
		responseentity = null;
		finalUrl = null;
		httpMethod = null; 
		requestEntity = null;
		LOG.info("TERMINA.CONSUME_WEB_SERVICE");
		return responseBody;
	}

}
