/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * ICallSoapApiDAO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2018-08-15                               Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc.dao;

/**
 * Type class: interface .<br>
 * Main objective: Interace para especificar las implementaciones de los metodos generados para consumir de la clase de dao call soap api(CallSOAPApiDAO) .<br>
 * Contains gets / sets methods:
 * <ul>
 * <li>callWebService</li>
 * </ul>
 * Global constants or variables:
 * <ul>
 * <li></li>
 * </ul>
 * @author Isban / iscontreras
 * ICallSoapApiDAO.java by Z055749
 *
 */
public interface ICallSoapApiDAO {
	/**
	 * Metodo para enviar informacion a un ws de tipo soap y regresar sus datos
	 * Methos callWebService: return type Object 
	 * @param url url del servicio o endpoint
	 * @param request mapa de entrada que se enviara al servicio web
	 * @return informacion que retorna la consulta al web service
	 */
	Object callWebService(String url, Object request);
}
