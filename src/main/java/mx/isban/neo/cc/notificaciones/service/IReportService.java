package mx.isban.neo.cc.notificaciones.service;

import mx.isban.neo.cc.model.request.CartaCancelReq;
import mx.isban.neo.cc.model.response.AdicionalResp;

public interface IReportService {
	
	public AdicionalResp[] coredmlReport(CartaCancelReq bean) ;


}
