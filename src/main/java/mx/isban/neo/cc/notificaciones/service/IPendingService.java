package mx.isban.neo.cc.notificaciones.service;

import mx.isban.neo.cc.model.response.CodesPendingResp;

public interface IPendingService {
	
	public String codesPending(CodesPendingResp[] codigos);
	
	
	public String getTrx(CodesPendingResp[] codigos, CodesPendingResp[] pendientesConsultar);


}
