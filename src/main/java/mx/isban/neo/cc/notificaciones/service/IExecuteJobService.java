package mx.isban.neo.cc.notificaciones.service;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public interface IExecuteJobService {

	void execute(JobExecutionContext context) throws JobExecutionException;

}