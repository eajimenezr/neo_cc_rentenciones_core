package mx.isban.neo.cc.notificaciones.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.activation.UnsupportedDataTypeException;
import javax.swing.WindowConstants;

import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import mx.isban.neo.cc.model.request.CartaCancelReq;
import mx.isban.neo.cc.model.response.AdicionalResp;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanArrayDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

@Service
public class ReportService implements IReportService{

	@Override
	public AdicionalResp[] coredmlReport(CartaCancelReq bean){
		String path = "C:\\Users\\Z266957\\Desktop"; 

		try {
			File file = ResourceUtils.getFile("classpath:CartaCancelacion.jrxml");
			JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
//			JRBeanArrayDataSource datasource= new JRBeanArrayDataSource(bean);
			Map<String,Object> parameters =new HashMap<>();
			parameters.put("Created by", "Santander");
			
	        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, getDataSource(bean));

//			JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters,getDataSource(bean));


			JasperExportManager.exportReportToPdfFile(jasperPrint, path+"cartaCancelacion.pdf");
		} catch (JRException | FileNotFoundException e) {
			// TODO: handle exception
		}
		
		return null;
	}
	
	private static JRDataSource getDataSource(CartaCancelReq bean) {
	    Collection<CartaCancelReq> coll = new ArrayList<CartaCancelReq>();
	    coll.add(bean);

	    return new JRBeanCollectionDataSource(coll);
	}

}
