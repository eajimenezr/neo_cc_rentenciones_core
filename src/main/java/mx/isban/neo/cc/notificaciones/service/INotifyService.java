package mx.isban.neo.cc.notificaciones.service;

import java.sql.SQLException;
import java.util.Map;

import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.request.CartaCancelReq;
import mx.isban.neo.cc.model.request.Mpa2Rq;
import mx.isban.neo.cc.model.response.AdicionalResp;
import mx.isban.neo.cc.model.response.CodesPendingResp;
import mx.isban.neo.cc.tenencias.trx.beans.lmwe.rs.TrxRs;

public interface INotifyService {
	
	public AdicionalResp[] coredmlCreateReport(CartaCancelReq bean) throws SQLException;
	
	public CodesPendingResp[] coredml(DmlBean bean )throws SQLException;
	
}