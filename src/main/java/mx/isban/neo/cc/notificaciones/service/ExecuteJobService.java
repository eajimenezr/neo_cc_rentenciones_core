package mx.isban.neo.cc.notificaciones.service;

import java.sql.SQLException;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.utils.UtilMappingSqlRe;

@Service
public class ExecuteJobService implements Job, IExecuteJobService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExecuteJobService.class);

	@Autowired
	private INotifyService cancelservice; 
	
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
//		LOGGER.error("se ejecuta proceso de consulta/actualizacion de codigos de bloqueo pendientes : "+ new Date());
//		DmlBean bean = UtilMappingSql.mapingQuery("TDC_codes", null, "1", null,null,null);
//    	try {
//			cancelservice.coredml(bean);
//		} catch (SQLException e) {
//			LOGGER.error("Ocurrio un error al ejecutar el job",e);
//
//		}
	}

}
