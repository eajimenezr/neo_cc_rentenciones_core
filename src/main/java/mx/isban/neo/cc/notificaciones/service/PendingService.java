package mx.isban.neo.cc.notificaciones.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import mx.isban.neo.cc.model.response.CodesPendingResp;
@Service
public class PendingService implements IPendingService {
	
	/**
	 * String baseUri : 
	 **/
	@Value("${endPoint.base}")
	private String baseUri;// 
	
	/**
	 * String uriTrxGenerica : 
	 **/
	@Value("${trx.tenencia}")
	private String uriTrxGenerica;
	
	@Override
	public String codesPending(CodesPendingResp[] codigos) {

		
		ArrayList<CodesPendingResp> subgrupoCodes = new ArrayList<CodesPendingResp>();//creamos el objeto lista
		ArrayList<CodesPendingResp> original = new ArrayList<>( Arrays.asList(codigos));

		
		
		subgrupoCodes.add(codigos[0]);
		String bucConsult=codigos[0].getBUC();
		for (int i = 0; i < codigos.length; i++) {

			if(codigos[0].getBUC().equals(codigos[i].getBUC())) {
				subgrupoCodes.add(codigos[i]);
			}
			
		}
		
		// Declaramos el Iterador y aliminamos los Elementos del ArrayList
		Iterator<CodesPendingResp> nombreIterator = original.iterator();
		while(nombreIterator.hasNext()){
			CodesPendingResp elemento = nombreIterator.next();
			if(elemento.getBUC().equals(bucConsult)) {
				nombreIterator.remove();
				
			}
			
		}
		
		
//		for (int j = 0; j < original.size(); j++) {
//			
//			CodesPendingResp obten = original.get(j);
//			
//			if(obten.getBUC().equals(bucConsult)) {
//				original.remove(0);
//				
//			}
//			
//		}
		
		
		subgrupoCodes.remove(1);
		CodesPendingResp[] subgrupo= subgrupoCodes.toArray(new CodesPendingResp[subgrupoCodes.size()]);
		CodesPendingResp[] subGrupPendiente= original.toArray(new CodesPendingResp[original.size()]);
		this.getTrx(subgrupo,subGrupPendiente);
		
		return "";
	}

	@Override
	public String getTrx(CodesPendingResp[] codigos, CodesPendingResp[] pendientesConsult) {
		
		StringBuilder endpointEjecutaTrx = new StringBuilder();
		endpointEjecutaTrx.append(baseUri);
		endpointEjecutaTrx.append(uriTrxGenerica);
		
		//consulta el subgrupo de TDCs por BUC en LMWE
		
		//compara cada TDC si su codigo en LMWE ya esta en 4, para mandar a actualizar en BD y mandar notificacion
		
		if(pendientesConsult.length>0 && pendientesConsult != null) {
			this.codesPending(pendientesConsult);
		}
		
		
		return null;
	}

}
