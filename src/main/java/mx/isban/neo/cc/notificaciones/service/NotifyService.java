package mx.isban.neo.cc.notificaciones.service;



import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.isban.neo.cc.builder.dml.service.IBuildReDMLService;
import mx.isban.neo.cc.dao.IConsumeExternalWsDAO;
import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.request.CartaCancelReq;
import mx.isban.neo.cc.model.response.AdicionalResp;
import mx.isban.neo.cc.model.response.CodesPendingResp;


@Service
public class NotifyService implements INotifyService {

	private static final Logger LOGGER = LoggerFactory.getLogger(NotifyService.class);
	
	@Autowired	
	public IBuildReDMLService dmlService;

	@Autowired
	public IReportService iRepService;
	
	@Autowired
	public IConsumeExternalWsDAO iConsumeExternalWsDAO;
	
	@Override
	public AdicionalResp[] coredmlCreateReport(CartaCancelReq dataSource) {
		return iRepService.coredmlReport(dataSource);
	}
	

	@Override
	public CodesPendingResp[] coredml(DmlBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return dmlService.buildDmlCodesPending(bean);
	}


}
