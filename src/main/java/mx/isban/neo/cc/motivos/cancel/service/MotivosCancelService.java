package mx.isban.neo.cc.motivos.cancel.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.isban.neo.cc.folio.service.IBuilderDMLService;
import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.MotivosCancelResp;

@Service
public class MotivosCancelService implements IMotivosCancelService {

	
	@Autowired
	private IBuilderDMLService buildService;
	
	@Override
	public MotivosCancelResp[] getCatalog(DmlBean bean) {
		return buildService.buildDml(bean);
	}

	@Override
	public MotivosCancelResp[] newMotivo(DmlBean bean) {
		return buildService.buildDml(bean);
	}

}
