package mx.isban.neo.cc.motivos.cancel.service;

import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.MotivosCancelResp;


public interface IMotivosCancelService {
	
	MotivosCancelResp[] getCatalog(DmlBean bean);

	MotivosCancelResp[] newMotivo(DmlBean bean);
	

}