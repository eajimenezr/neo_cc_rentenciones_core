package mx.isban.neo.cc.reutilizables.controller;

import java.sql.SQLException;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import io.swagger.annotations.ApiOperation;
import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.request.CartaCancelReq;
import mx.isban.neo.cc.model.response.AdicionalResp;
import mx.isban.neo.cc.model.response.CodesPendingResp;
import mx.isban.neo.cc.model.rs.RespBean;
import mx.isban.neo.cc.notificaciones.service.INotifyService;
import mx.isban.neo.cc.utils.ConstastCommon;
import mx.isban.neo.cc.utils.Headers;
import mx.isban.neo.cc.utils.UtilMappingSqlRe;
import mx.isban.neo.cc.utils.Utils;

@RestController
@RequestMapping("/notifications")
public class NotificacionesController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(NotificacionesController.class);

	
	@Autowired
	private INotifyService cancelservice; 
	
	

    /**
     * guarda email o celular de cliente para sus posterior notificacion del proceso RTDC
     *
     * @param SYS_REFCURSOR Response de la consulta(function) a BD 
     * @return FolioService
     * @throws SQLException 
     */
    @ApiOperation(value = "Construye la carta de cancelacion y manda el cifrado")
	@RequestMapping(value = "/cartaCancel", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> cartaCancel(@RequestBody CartaCancelReq dataSource,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser) throws SQLException  {
		LOGGER.info("INICIA creacion de carta de cancelacion");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean response = null;
		AdicionalResp[] result=null;
		
		
    		try {
//    			DmlBean bean = UtilMappingSql.mapingQueryObjCel("guarda_cel", req);
    	    	result = cancelservice.coredmlCreateReport(dataSource);
  
    	    	if(result != null && result[0].getIDMENSAJE() > 0) {
    	    		httpStatus = HttpStatus.OK;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_RGS+" de celular/email.", httpStatus));
    	    	}else {
    	        	httpStatus = HttpStatus.BAD_REQUEST;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en el registro de email/celular.", httpStatus));
        	    	    	    
    	    	}	

    		}catch(HttpClientErrorException ex) {
    				LOGGER.error("Ocurrio un error al guardar email/celular",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA creacion de carta de cancelacion");
			return new ResponseEntity<RespBean>(response, httpStatus);
			
	}
    
    /**
     * consulta la traza del proceso de RTDC
     *
     * @param SYS_REFCURSOR FolioService
     * @return FolioService
     */
    @ApiOperation(value = "consulta TDC por codigos de bloqueo pendientes")
	@RequestMapping(value = "/consulta", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> consultaCodigos(@RequestParam("id_tdc") String id_tdc,
    											@RequestParam("accion") String accion,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA consulta TDC por codeLock pendientes");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean response = null;
		CodesPendingResp[] result=null;
		
    		try {
    			DmlBean bean = UtilMappingSqlRe.mapingQuery("TDC_codes", id_tdc, accion, null,null,null);
    	    	result = cancelservice.coredml(bean);
    	    	if(result != null && result[0].getIDMENSAJE() > 0) {

	    	    	httpStatus = HttpStatus.OK;
					response = new RespBean(); 
					response.setResultado(result);
					//setamos el estatus
					response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_MSG+" de TDC por codigos de bloqueo pendientes.", httpStatus));
    	    	}else {
    	    		httpStatus = HttpStatus.BAD_REQUEST;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en la consulta de TDC por codigos de bloqueo pendientes.", httpStatus));
        	    	  
    	    	}
    		}catch(HttpClientErrorException | SQLException ex) {
    				LOGGER.error("Ocurrio un error al consular el beneficio",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.consulta TDC por codeLock pendientes");
			return new ResponseEntity<RespBean>(response, httpStatus);
			
	}
    
    
    
}

