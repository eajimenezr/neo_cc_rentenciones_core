package mx.isban.neo.cc.reutilizables.controller;

import java.sql.SQLException;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import io.swagger.annotations.ApiOperation;
import mx.isban.neo.cc.cancel.service.ICancelService;
import mx.isban.neo.cc.excepcion.ValidationParamsException;
import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.request.CancelRq;
import mx.isban.neo.cc.model.request.CelEmailReq;
import mx.isban.neo.cc.model.request.Mpa2Rq;
import mx.isban.neo.cc.model.response.AdicionalResp;
import mx.isban.neo.cc.model.response.MessageRespCancel;
import mx.isban.neo.cc.model.rs.RespBean;
import mx.isban.neo.cc.nivel.autenticacion.beans.FilterRq;
import mx.isban.neo.cc.tenencias.trx.beans.lmwe.rs.Resultado;
import mx.isban.neo.cc.tenencias.trx.beans.lmwe.rs.TrxRs;
import mx.isban.neo.cc.utils.ConstastCommon;
import mx.isban.neo.cc.utils.Headers;
import mx.isban.neo.cc.utils.MappingUtilTnnc;
import mx.isban.neo.cc.utils.UtilMappingSqlRe;
import mx.isban.neo.cc.utils.Utils;

@RestController
@RequestMapping("/cancelTdc")
public class CancelTdcController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CancelTdcController.class);

	
	@Autowired
	private ICancelService cancelservice; 
	
	
	/**
     * consulta la traza del proceso de RTDC
     *
     * @param SYS_REFCURSOR Response de la consulta(function) a BD 
     * @return FolioService
     */
    @ApiOperation(value = "consulta argumentos iniciales del proceso RTDC")
	@RequestMapping(value = "/validaciones", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> getValidaciones(@RequestBody CancelRq req,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("Inicia.consulta de validaciones de cancelacion");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean response = null;
		MessageRespCancel[] result=null;
		String consulta= "valida_cancel";
		
		
    		try {
    			DmlBean bean = UtilMappingSqlRe.mapingQueryObj(consulta, req);
    	    	result = cancelservice.coredmlCancel(bean);
  
    	    	if(result != null && result[0].getIDMENSAJE() > 0) {
    	    		httpStatus = HttpStatus.OK;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_MSG+" de validaciones para cancelaciones.", httpStatus));
    	    	}else {
    	        	httpStatus = HttpStatus.BAD_REQUEST;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en la consulta de validaciones para cancelaciones.", httpStatus));
        	    	    	    
    	    	}	

    		}catch(HttpClientErrorException | SQLException ex) {
    				LOGGER.error("Ocurrio un error al consular los argumentos",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.consulta de validaciones de cancelacion");
			return new ResponseEntity<RespBean>(response, httpStatus);
			
	}
    
    /**
	 * Method bloqueoTDC
	 * Method objective: ___ .<br>
	 * @param buc numero de cliente para generar la busqueda
	 * @param dataUser encabezado que contiene la cookie nombrada dataUser; contiene datos como perfil y area del usuario
	 * @param filterRq mapa de datos que contienen informacion del ejecutivo
	 * @return ResponseEntity<RespBean> mapa de datos con la informacion recupera de posicion consolidad; Creditos,Nomina,Captacion
     * @throws ValidationParamsException 
	 * @throws Exception 
	 */
	@RequestMapping(value = "/lockTDC", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> cancelacionTDC(@RequestParam("pan") String pan,
    											   @RequestParam("codigo") String codigo,
			@RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser,
			@RequestBody(required=false) FilterRq filterRq) {
				
		LOGGER.info("INICIA.bloqueo de TDC");
			/**Inicializamos nuestros objetos**/
			HttpStatus httpStatus = null;
			RespBean response = null;
			
			try {
				Mpa2Rq resquest = MappingUtilTnnc.getSingletonInstance().mappingMpa2Rq(pan,codigo);
				TrxRs  trxRs = cancelservice.coreGetCreditCardTrx(resquest);
				Resultado result = trxRs.getResultado(); 
				//seteamos el estatus y el resultado
				
				if(trxRs != null && trxRs.getEstatus().getMensaje().equals("No existen datos")) {
						httpStatus = HttpStatus.BAD_REQUEST;
						response = new RespBean();
						response.setResultado(result);
						//setamos el estatus
						response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), trxRs.getEstatus().getMensaje(),"Ocurrio un error en el bloqueo de la TDC.", httpStatus));
				}else {
					httpStatus = HttpStatus.OK;
					response = new RespBean();
					response.setResultado(result);
					//setamos el estatus
					response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), trxRs.getEstatus().getMensaje(),ConstastCommon.SUCCESS_RGS+" del bloqueo de la TDC.", httpStatus));
				}
			
		}catch(HttpClientErrorException ex) {
			LOGGER.error("busquedaTenencia.ValidationParamsException",ex);
		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.bloqueo de TDC");
			return new ResponseEntity<RespBean>(response, httpStatus);
			
		}

	/**
     * consulta la traza del proceso de RTDC
     *
     * @param SYS_REFCURSOR Response de la consulta(function) a BD 
     * @return FolioService
     */
    @ApiOperation(value = "consulta argumentos iniciales del proceso RTDC")
	@RequestMapping(value = "/adicionales", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> getAdicionales(@RequestParam("cve_folio") String cve_folio,
												   @RequestParam("num_tdc") String num_tdc,
												   @RequestParam("accion") String accion,
												   @RequestParam("codigo_aplicado") String codigoAplicado,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("Inicia.consulta de validaciones de cancelacion");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean response = null;
		AdicionalResp[] result=null;
		String consulta= "consulta_adicionales";
		
		
    		try {
    			DmlBean bean = UtilMappingSqlRe.mapingQuery(consulta, cve_folio, num_tdc, accion, codigoAplicado, null);
    	    	result = cancelservice.coredmlAdicionales(bean);
  
    	    	response = new RespBean(); 
				response.setResultado(result);
    	    	
    	    	if(result != null && result[0].getIDMENSAJE() == 0) {
    	    		httpStatus = HttpStatus.BAD_REQUEST;
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"No existe el folio o TDC a consultar.", httpStatus));
    	    	}else if(result != null && result[0].getIDMENSAJE() == 2){
    	        	httpStatus = HttpStatus.BAD_REQUEST;
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"La TDC no tiene adicionales asociadas.", httpStatus));
    	    	}else {
    	    		httpStatus = HttpStatus.OK;
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_MSG+" de TDC adicionales.", httpStatus));

    				//setamos el estatus
    	    	}	

    		}catch(HttpClientErrorException | SQLException ex) {
    				LOGGER.error("Ocurrio un error al consular los argumentos",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.consulta de validaciones de cancelacion");
			return new ResponseEntity<RespBean>(response, httpStatus);
			
	} 

    /**
     * guarda email o celular de cliente para sus posterior notificacion del proceso RTDC
     *
     * @param SYS_REFCURSOR Response de la consulta(function) a BD 
     * @return FolioService
     */
    @ApiOperation(value = "consulta argumentos iniciales del proceso RTDC")
	@RequestMapping(value = "/saveContacts", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> saveContacto(@RequestBody CelEmailReq req,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA guardado de email/celular");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean response = null;
		AdicionalResp[] result=null;
		
		
    		try {
    			DmlBean bean = UtilMappingSqlRe.mapingQueryObjCel("guarda_cel", req);
    	    	result = cancelservice.coredmlAdicionales(bean);
  
    	    	if(result != null && result[0].getIDMENSAJE() > 0) {
    	    		httpStatus = HttpStatus.OK;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_RGS+" de celular/email.", httpStatus));
    	    	}else {
    	        	httpStatus = HttpStatus.BAD_REQUEST;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en el registro de email/celular.", httpStatus));
        	    	    	    
    	    	}	

    		}catch(HttpClientErrorException | SQLException ex) {
    				LOGGER.error("Ocurrio un error al guardar email/celular",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA guardado de email/celular");
			return new ResponseEntity<RespBean>(response, httpStatus);
			
	}
    
    
}

