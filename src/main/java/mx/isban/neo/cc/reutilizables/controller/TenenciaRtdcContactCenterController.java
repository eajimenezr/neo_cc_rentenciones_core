/**
 ******************************************************************************
 *
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * TenenciaContactCenterContoller.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour     By        Company          Description
 * ======= =========== ========= =============== ==============================
 * 1.0     2018-08-15                               Creacion
 *
 ******************************************************************************
 * */
package mx.isban.neo.cc.reutilizables.controller;


import java.sql.SQLException;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import io.swagger.annotations.ApiOperation;
import mx.isban.neo.cc.excepcion.ValidationParamsException;
import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.rs.RespBean;
import mx.isban.neo.cc.nivel.autenticacion.beans.FilterRq;
import mx.isban.neo.cc.tenencias.model.DatosLmweRs;
import mx.isban.neo.cc.tenencias.model.LimiteResp;
import mx.isban.neo.cc.tenencias.service.ITenenciaService;
import mx.isban.neo.cc.tenencias.trx.beans.lmwe.rq.LmweRq;
import mx.isban.neo.cc.utils.ConstastCommon;
import mx.isban.neo.cc.utils.Headers;
import mx.isban.neo.cc.utils.MappingUtilTnnc;
import mx.isban.neo.cc.utils.UtilMappingSqlRe;
import mx.isban.neo.cc.utils.Utils;




/**
 * Clase de tipo controller para hacer el llamado a los servicios de tenencias.
 * Contiene los metodos:<br>
 * @author Isban / Z266957
 * Class AlertasContactCenterController.java 
 */
@RestController()
@RequestMapping("/rtdc")
public class TenenciaRtdcContactCenterController {

	/**
	 * Logeo en consola
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(TenenciaRtdcContactCenterController.class);

	/**
	 * Se inyecta el service ITenenciaService
	 */
	@Autowired
	private ITenenciaService itenenciasService;

	
	/**
	 * Method busquedaTenencia
	 * Method objective: ___ .<br>
	 * @param buc numero de cliente para generar la busqueda
	 * @param dataUser encabezado que contiene la cookie nombrada dataUser; contiene datos como perfil y area del usuario
	 * @param filterRq mapa de datos que contienen informacion del ejecutivo
	 * @return ResponseEntity<RespBean> mapa de datos con la informacion recupera de posicion consolidad; Creditos,Nomina,Captacion
	 * @throws Exception 
	 */
	@RequestMapping(value = "/{buc}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON})
	public ResponseEntity<RespBean> busquedaTenencia(@PathVariable("buc") String buc,
			@RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser,
			@RequestBody(required=false) FilterRq filterRq) {
				
		LOGGER.info("INICIA.obtenerTarjetasDeCreditoPorClienteTRX");
			/**Inicializamos nuestros objetos**/
			HttpStatus httpStatus = null;
			RespBean response = null;
			
			try {
				LmweRq resquest = MappingUtilTnnc.getSingletonInstance().mappingLmweRq(buc);
				DatosLmweRs  trxRs = itenenciasService.coreGetCreditCardTrx(resquest);
				//seteamos el estatus y el resultado
						httpStatus = HttpStatus.OK;
						response = new RespBean();
						response.setResultado(trxRs);
						//setamos el estatus
						response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_MSG+" de las tdc en al trx.", httpStatus));
						/**Si es diferente  a null se setea el mensaje que se recupera**/
			
		}catch(ValidationParamsException ex) {
			LOGGER.error("busquedaTenencia.ValidationParamsException",ex);
		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.OBTENER_LISTA_DE_TARJETAS_POR_CLIENTE");
			return new ResponseEntity<RespBean>(response, httpStatus);
			
		}
	
	
	/**
     * consulta la traza del proceso de RTDC
     *
     * @param SYS_REFCURSOR Response de la consulta(function) a BD 
     * @return FolioService
     */
    @ApiOperation(value = "consulta limite de creditopor TDC")
	@RequestMapping(value = "/limCredit", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> getLimite(@RequestParam("num_Tdc") String num_Tdc,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA limite de credito por TDC");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean response = null;
		LimiteResp[] result=null;
		
		
    		try {
    			DmlBean bean = UtilMappingSqlRe.mapingQuery("consulta_limite", num_Tdc, null, null, null,null);
    	    	result = itenenciasService.coreDml(bean);
    	  
    	    	if(result != null && result[0].getIDMENSAJE() > 0) {
    	    		httpStatus = HttpStatus.OK;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_MSG+" de limite de credito por TDC.", httpStatus));
    	    	}else {
    	        	httpStatus = HttpStatus.BAD_REQUEST;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en la consulta de argumentos iniciales.", httpStatus));
        	    	    	    
    	    	}	

    		}catch(HttpClientErrorException | SQLException ex) {
    				LOGGER.error("Ocurrio un error al consular los argumentos",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.limite de credito por TDC");
			return new ResponseEntity<RespBean>(response, httpStatus);
			
	}	
    
    /**
     * consulta la traza del proceso de RTDC
     *
     * @param SYS_REFCURSOR Response de la consulta(function) a BD 
     * @return FolioService
     */
    @ApiOperation(value = "consulta limite de credito por TDC")
	@RequestMapping(value = "/rewardPoints", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON})
    public ResponseEntity<RespBean> getPuntoPremia(@RequestParam("num_Tdc") String num_Tdc,
    									   @RequestHeader(value = Headers.HEADER_SANTANDER_DATA_ID,required = false)String dataUser)  {
		LOGGER.info("INICIA consulta de puntos recompensa por TDC");
		/**Inicializamos nuestros objetos**/
		HttpStatus httpStatus = null;
		RespBean response = null;
		LimiteResp[] result=null;
		
		
    		try {
    			DmlBean bean = UtilMappingSqlRe.mapingQuery("consulta_puntos", num_Tdc, null, null, null,null);
    	    	result = itenenciasService.coreDml(bean);
    	  
    	    	if(result != null && result[0].getIDMENSAJE() > 0) {
    	    		httpStatus = HttpStatus.OK;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.SUCCESS,ConstastCommon.SUCCESS_MSG+" de puntos premia por TDC.", httpStatus));
    	    	}else {
    	        	httpStatus = HttpStatus.BAD_REQUEST;
    				response = new RespBean(); 
    				response.setResultado(result);
    				//setamos el estatus
    				response.setStatus(Utils.getSingletonInstance().setEstatus(httpStatus.value(), ConstastCommon.FAIL,"Ocurrio un error en la consulta de argumentos iniciales.", httpStatus));
        	    	    	    
    	    	}	

    		}catch(HttpClientErrorException | SQLException ex) {
    				LOGGER.error("Ocurrio un error al consular los argumentos",ex);
    		}
			//Regresamos el resultado y el estatus
			LOGGER.info("TERMINA.consulta de puntos recompensa por TDC");
			return new ResponseEntity<RespBean>(response, httpStatus);
			
	}	


}
