package mx.isban.neo.cc.folio.service;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.isban.neo.cc.bd.dao.IEjecucionDmlDAO;
import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.CodesResponse;
import mx.isban.neo.cc.model.response.FolioResponse;


@Service
public class FunctionsService implements IFunctionsService {
	/**
	 * Constante para trazar la aplicacion.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(FunctionsService.class);
	/**
	 * IEjecucionDmlDAO iEjecucionDmlDAO : 
	 **/
	@Autowired
	private  IEjecucionDmlDAO iEjecucionDmlDAO;
		
	/**
	 * Method extraerConfigTdcVirtual
	 * Method objective: ___ .<br>
	 * @param config configuracion que llega del front
	 * @return ConfigRq configuracion a la que se le setea lo que recuperamos de la BD
	 */
	@Override
	public FolioResponse[] newFolio(DmlBean bean) {
		LOG.info("inicia.getFolio");
		LOG.info("QUERY_TDC_VIRTUAL_COD>>");
		FolioResponse[] paramOut=null;
		
		try {
			paramOut = iEjecucionDmlDAO.getNewFolio(bean);
		} catch (SQLException e) {
			LOG.error("ocurrio un error al generar el folio");
		}
		
	    LOG.info("termina.extraerNoFolio");
	    //Regresamos los datos
	    return paramOut ;
	}

	@Override
	public CodesResponse[] getLockCodes(String codes) {
		LOG.info("inicia.getFolio");
		LOG.info("QUERY_TDC_VIRTUAL_COD>>");
		CodesResponse[] paramOut = null;
		try {
			paramOut = iEjecucionDmlDAO.getCodigosBloqueo(codes);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			LOG.error("ocurrio un error al generar el folio");
		}
		
	    LOG.info("termina.extraerNoFolio");
	    //Regresamos los datos
	    return paramOut ;
	}
}
