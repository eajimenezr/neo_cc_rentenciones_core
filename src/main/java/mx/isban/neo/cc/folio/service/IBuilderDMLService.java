package mx.isban.neo.cc.folio.service;

import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.BenefitsResp;
import mx.isban.neo.cc.model.response.MessageResp;
import mx.isban.neo.cc.model.response.MotivosCancelResp;
import mx.isban.neo.cc.model.response.TrazaResp;
public interface IBuilderDMLService {
	
	 MotivosCancelResp[] buildDml(DmlBean bean);
	
	 TrazaResp[] buildDmlT(DmlBean bean);
	
	 BenefitsResp[] buildDmlBnft(DmlBean bean);

	 MessageResp[] buildDmlMTdc(DmlBean bean);
	
	 MessageResp[] buildDmlScript(DmlBean bean) ;





}