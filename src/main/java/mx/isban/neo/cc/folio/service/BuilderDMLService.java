package mx.isban.neo.cc.folio.service;

import java.sql.SQLException;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import mx.isban.neo.cc.bd.dao.IEjecucionDmlDAO;

import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.BenefitsResp;
import mx.isban.neo.cc.model.response.MessageResp;
import mx.isban.neo.cc.model.response.MotivosCancelResp;
import mx.isban.neo.cc.model.response.TrazaResp;


@Service
public class BuilderDMLService implements IBuilderDMLService {
	
	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(BuilderDMLService.class);

	@Autowired
	private IEjecucionDmlDAO executesql;
	
	
	@Override
	public MotivosCancelResp[] buildDml(DmlBean bean) {
		
		MotivosCancelResp[] result= null;
		
		String funcion = bean.getFuncion();
		String paquete = bean.getPkg();
		Map<String, String> paramsIn = bean .getParamsIn();
		Map<String, String> paramsOut = bean .getParamsOut();

			try {
				result = executesql.getCatalog(funcion, paquete, paramsOut, paramsIn);
				//respbean se hara dependiendo al bean correspondiente
			} catch (SQLException e) {
				LOGGER.error("Ocurrio una excepcion en la ejecucion SQL"+e);
			}
		

		return result;
	}
	
	@Override
	public TrazaResp[] buildDmlT(DmlBean bean) {
		
		TrazaResp[] result= null;
		if(bean != null) {
			String funcion = bean.getFuncion();
			String paquete = bean.getPkg();
			Map<String, String> paramsIn = bean .getParamsIn();
			Map<String, String> paramsOut = bean .getParamsOut();
		
			try {
				if(funcion.equals("FNC_ACTUALIZA_TRAZA")){
					result = executesql.dmlTrazaUpd(funcion, paquete, paramsOut, paramsIn);
				}else {
					result = executesql.dmlTraza(funcion, paquete, paramsOut, paramsIn);
				}
				
				
				//respbean se hara dependiendo al bean correspondiente
			} catch (SQLException e) {
				LOGGER.error("Ocurrio una excepcion en la ejecucion SQL"+e);
			}
		}

		return result;
	}

	@Override
	public BenefitsResp[] buildDmlBnft(DmlBean bean) {
		BenefitsResp[] result= null;
		if(bean != null) {
			String funcion = bean.getFuncion();
			String paquete = bean.getPkg();
			Map<String, String> paramsIn = bean .getParamsIn();
			Map<String, String> paramsOut = bean .getParamsOut();
		
			try {
				
				if(funcion.equals("FNC_ACTUALIZA_BENEFICIO") || (funcion.equals("FNC_ELIMINA_BENEFICIO"))){
					result = executesql.dmlBenefitsUpd(funcion, paquete, paramsOut, paramsIn);
				}else {
					result = executesql.dmlBenefits(funcion, paquete, paramsOut, paramsIn);
				}
				//respbean se hara dependiendo al bean correspondiente
			} catch (SQLException e) {
				LOGGER.error("Ocurrio una excepcion en la ejecucion SQL"+e);
			}
		}

		return result;
	}

	@Override
	public MessageResp[] buildDmlMTdc(DmlBean bean) {
		MessageResp[] result= null;
		if(bean != null) {
	
			try {				
				result = executesql.motivoToTdc(bean);
				//respbean se hara dependiendo al bean correspondiente
			} catch (SQLException e) {
				LOGGER.error("Ocurrio una excepcion en la ejecucion SQL"+e);
			}
		}

		return result;
	}

	@Override
	public MessageResp[] buildDmlScript(DmlBean bean) {
		MessageResp[] result= null;
		if(bean != null) {
	
			try {
				if(bean.getFuncion().equals("FNC_RET_MOTIVOS_LLAMADA")) {
					result = executesql.dmlMotivosCall(bean);
				}else {
					result = executesql.dmlScripts(bean);
				}
				
				
				
				//respbean se hara dependiendo al bean correspondiente
			} catch (SQLException e) {
				LOGGER.error("Ocurrio una excepcion en la ejecucion SQL"+e);
			}
		}

		return result;
	}

}
 