package mx.isban.neo.cc.folio.service;

import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.model.response.CodesResponse;
import mx.isban.neo.cc.model.response.FolioResponse;
/**
 * @author Z266957
 * interface to class IFunctionsService
 */
public interface IFunctionsService {
	
	
	/**
	 * Metodo que retorna el numero de folio para el proceso de retencion TDC
	 */
	FolioResponse[] newFolio(DmlBean bean);
	
	/**
	 * @param codes cadena de codigos de bloque
	 * @return regresa los mensajes de codigo de bloqueo de las TDC
	 */
	CodesResponse[] getLockCodes(String codes);
	
}
