package mx.isban.neo.cc.tenencias.service;


import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import mx.isban.neo.cc.builder.dml.service.IBuildReDMLService;
import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.tenencias.model.DatosLmweRs;
import mx.isban.neo.cc.tenencias.model.LimiteResp;
import mx.isban.neo.cc.tenencias.trx.beans.lmwe.config.ConfigRq;
import mx.isban.neo.cc.tenencias.trx.beans.lmwe.rq.LmweRq;


@Service
public class TenenciaService implements ITenenciaService {
		
	/**
	 * ITrxService iTrxService : 
	 **/
	@Autowired
	private ITrxService iTrxService;
	/**
	 * ITarjetaCreditoService iTarjetaCreditoService : 
	 **/
	@Autowired
	private ITarjetaCreditoService iTarjetaCreditoService;
	
	@Autowired
	private IBuildReDMLService ibuilderService;

	/**
	 * Method coreGetCreditCardTrx
	 * Method objective: ___ .<br>
	 * @param resquest mapa de datos apra acceder a la TRX
	 * @return TrxRs informacion recuperada de la TRX LMWE
	 */
	@Override
	public DatosLmweRs coreGetCreditCardTrx(LmweRq resquest) {
		return iTrxService.consumirTrxLmwe(resquest);
	}
	
	/**
	 * Method coreExtraerConfigTdcVirtual
	 * Method objective: ___ .<br>
	 * @param config configuracion que llega del front
	 * @return ConfigRq configuracion a la que se le setea lo que recuperamos de la BD
	 */
	@Override
	public ConfigRq coreExtraerConfigTdcVirtual(ConfigRq config) {
		return iTarjetaCreditoService.extraerConfigTdcVirtual(config);
	}
	/**
	 * Method coreExtraerTrx
	 * Method objective: ___ .<br>
	 * @param endpoint endpoint del servicio
	 * @param json json que se envia
	 * @return String rresultado consultado
	 */
	@Override
	public 	String coreExtraerTrx(String endpoint, Object json) {
		return iTarjetaCreditoService.extraerTrx(endpoint, json);
	}

	@Override
	public LimiteResp[] coreDml(DmlBean bean) throws SQLException{
		return ibuilderService.buildDml(bean);
	}
}
