package mx.isban.neo.cc.tenencias.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class DatosLmwe implements Serializable {
	
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("indSituacionTarj")	
	private String indSituacionTarj;
	
	@JsonProperty("cuentaCheques")	
    private String cuentaCheques;
    
	@JsonProperty("calidadParticipacio")
    private String calidadParticipacio;
    
	@JsonProperty("tarjetaPaginacion")
    private String tarjetaPaginacion;

	@JsonProperty("fechaUltimoCorte")
    private String fechaUltimoCorte;

	@JsonProperty("modenaTarjeta")
    private String modenaTarjeta;

	@JsonProperty("centroAltaContrato")
    private String centroAltaContrato;

	@JsonProperty("importeSaldo")
    private String importeSaldo;

	@JsonProperty("indTipoTarj")
    private String indTipoTarj;

	@JsonProperty("descripcionProducto")
    private String descripcionProducto;

	@JsonProperty("marcaTarj")
    private String marcaTarj;

	@JsonProperty("producto")
    private String producto;

	@JsonProperty("cuentaContrato")
    private String cuentaContrato;

	@JsonProperty("condicionEspampacio")
    private String condicionEspampacio;

	@JsonProperty("periodoCaducidad")
    private String periodoCaducidad;

	@JsonProperty("indOperatividadBlq")
    private String indOperatividadBlq;

	@JsonProperty("subproducto")
    private String subproducto;

	@JsonProperty("indReversividadBlq")
    private String indReversividadBlq;

	@JsonProperty("entidadContrato")
    private String entidadContrato;

	@JsonProperty("numeroCliente")
    private String numeroCliente;

	@JsonProperty("pan")
    private String pan;

	@JsonProperty("nombreEstampacion")
    private String nombreEstampacion;

	@JsonProperty("limiteCredito")
    private String limiteCredito;

	@JsonProperty("fechaAlta")
    private String fechaAlta;

	@JsonProperty("numeroBeneficiarios")
    private String numeroBeneficiarios;

	@JsonProperty("codigoBloqueo")
    private String codigoBloqueo;

	@JsonProperty("indDebitoCredito")
    private String indDebitoCredito;

	@JsonProperty("SaldoDisponibleJupiter")
    private String SaldoDisponibleJupiter;

	@JsonProperty("SaldoPuntualJupiter")
    private String SaldoPuntualJupiter;

	public String getIndSituacionTarj() {
		return indSituacionTarj;
	}

	public void setIndSituacionTarj(String indSituacionTarj) {
		this.indSituacionTarj = indSituacionTarj;
	}

	public String getCuentaCheques() {
		return cuentaCheques;
	}

	public void setCuentaCheques(String cuentaCheques) {
		this.cuentaCheques = cuentaCheques;
	}

	public String getCalidadParticipacio() {
		return calidadParticipacio;
	}

	public void setCalidadParticipacio(String calidadParticipacio) {
		this.calidadParticipacio = calidadParticipacio;
	}

	public String getTarjetaPaginacion() {
		return tarjetaPaginacion;
	}

	public void setTarjetaPaginacion(String tarjetaPaginacion) {
		this.tarjetaPaginacion = tarjetaPaginacion;
	}

	public String getFechaUltimoCorte() {
		return fechaUltimoCorte;
	}

	public void setFechaUltimoCorte(String fechaUltimoCorte) {
		this.fechaUltimoCorte = fechaUltimoCorte;
	}

	public String getModenaTarjeta() {
		return modenaTarjeta;
	}

	public void setModenaTarjeta(String modenaTarjeta) {
		this.modenaTarjeta = modenaTarjeta;
	}

	public String getCentroAltaContrato() {
		return centroAltaContrato;
	}

	public void setCentroAltaContrato(String centroAltaContrato) {
		this.centroAltaContrato = centroAltaContrato;
	}

	public String getImporteSaldo() {
		return importeSaldo;
	}

	public void setImporteSaldo(String importeSaldo) {
		this.importeSaldo = importeSaldo;
	}

	public String getIndTipoTarj() {
		return indTipoTarj;
	}

	public void setIndTipoTarj(String indTipoTarj) {
		this.indTipoTarj = indTipoTarj;
	}

	public String getDescripcionProducto() {
		return descripcionProducto;
	}

	public void setDescripcionProducto(String descripcionProducto) {
		this.descripcionProducto = descripcionProducto;
	}

	public String getMarcaTarj() {
		return marcaTarj;
	}

	public void setMarcaTarj(String marcaTarj) {
		this.marcaTarj = marcaTarj;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getCuentaContrato() {
		return cuentaContrato;
	}

	public void setCuentaContrato(String cuentaContrato) {
		this.cuentaContrato = cuentaContrato;
	}

	public String getCondicionEspampacio() {
		return condicionEspampacio;
	}

	public void setCondicionEspampacio(String condicionEspampacio) {
		this.condicionEspampacio = condicionEspampacio;
	}

	public String getPeriodoCaducidad() {
		return periodoCaducidad;
	}

	public void setPeriodoCaducidad(String periodoCaducidad) {
		this.periodoCaducidad = periodoCaducidad;
	}

	public String getIndOperatividadBlq() {
		return indOperatividadBlq;
	}

	public void setIndOperatividadBlq(String indOperatividadBlq) {
		this.indOperatividadBlq = indOperatividadBlq;
	}

	public String getSubproducto() {
		return subproducto;
	}

	public void setSubproducto(String subproducto) {
		this.subproducto = subproducto;
	}

	public String getIndReversividadBlq() {
		return indReversividadBlq;
	}

	public void setIndReversividadBlq(String indReversividadBlq) {
		this.indReversividadBlq = indReversividadBlq;
	}

	public String getEntidadContrato() {
		return entidadContrato;
	}

	public void setEntidadContrato(String entidadContrato) {
		this.entidadContrato = entidadContrato;
	}

	public String getNumeroCliente() {
		return numeroCliente;
	}

	public void setNumeroCliente(String numeroCliente) {
		this.numeroCliente = numeroCliente;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getNombreEstampacion() {
		return nombreEstampacion;
	}

	public void setNombreEstampacion(String nombreEstampacion) {
		this.nombreEstampacion = nombreEstampacion;
	}

	public String getLimiteCredito() {
		return limiteCredito;
	}

	public void setLimiteCredito(String limiteCredito) {
		this.limiteCredito = limiteCredito;
	}

	public String getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getNumeroBeneficiarios() {
		return numeroBeneficiarios;
	}

	public void setNumeroBeneficiarios(String numeroBeneficiarios) {
		this.numeroBeneficiarios = numeroBeneficiarios;
	}

	public String getCodigoBloqueo() {
		return codigoBloqueo;
	}

	public void setCodigoBloqueo(String codigoBloqueo) {
		this.codigoBloqueo = codigoBloqueo;
	}

	public String getIndDebitoCredito() {
		return indDebitoCredito;
	}

	public void setIndDebitoCredito(String indDebitoCredito) {
		this.indDebitoCredito = indDebitoCredito;
	}

	public String getSaldoDisponibleJupiter() {
		return SaldoDisponibleJupiter;
	}

	public void setSaldoDisponibleJupiter(String saldoDisponibleJupiter) {
		SaldoDisponibleJupiter = saldoDisponibleJupiter;
	}

	public String getSaldoPuntualJupiter() {
		return SaldoPuntualJupiter;
	}

	public void setSaldoPuntualJupiter(String saldoPuntualJupiter) {
		SaldoPuntualJupiter = saldoPuntualJupiter;
	}

	
	
}
