package mx.isban.neo.cc.tenencias.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;


import mx.isban.neo.cc.tenencias.trx.beans.lmwe.config.ConfigRq;
import mx.isban.neo.cc.tenencias.trx.beans.lmwe.config.Virtual;
import mx.isban.neo.cc.utils.ConstastCommon;
import mx.isban.neo.cc.utils.MappingUtils;
import mx.isban.neo.cc.dao.IConsumeExternalWsDAO;

@Service
public class TarjetaCreditoService implements ITarjetaCreditoService{
	/**
	 * Constante para trazar la aplicacion.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(TarjetaCreditoService.class);
	/**
	 * IConsumeExternalWsDAO iConsumeExternalWsDAO : 
	 **/
	@Autowired
	private IConsumeExternalWsDAO iConsumeExternalWsDAO;
	/**
	 * String QUERY_TDC_VIRTUAL_COD : query para extraer las configuraciones para los codigos de tarjetas de credito virtuales
	 **/
	private static final String QUERY_TDC_VIRTUAL_COD = "SELECT IDE_TPO_TDC,COD_MARCA,DSC_TPO_TDC FROM NEO_CC_MX_CAT_TPOS_TDC";
	/**
	 * String IDE_TPO_TDC : 
	 **/
	private static final String IDE_TPO_TDC  ="IDE_TPO_TDC";
	/**
	 * String COD_MARCA : 
	 **/
	private static final String COD_MARCA    ="COD_MARCA";
	/**
	 * String DSC_TPO_TDC : 
	 **/
	private static final String DSC_TPO_TDC  ="DSC_TPO_TDC";
	
	
	/**
	 * Method extraerTrx
	 * Method objective: ___ .<br>
	 * @param endpoint endpoint del servicio
	 * @param json json que se envia
	 * @return String rresultado consultado
	 */
	@Override
	public String extraerTrx(String endpoint,Object json) {
		String result = ConstastCommon.EMPTY_STRING;
		try {
			//Se manda ejecutar el proceso
		 result = iConsumeExternalWsDAO.consumeWs( endpoint, json , null, null, HttpMethod.POST);
		}catch (RestClientException e) {
			/*RestClientException  **/
			LOG.error("consumirTrxLmwe.RestClientException",e);
		}
		
		return result;
	}
	
	/**
	 * Method extraerConfigTdcVirtual
	 * Method objective: ___ .<br>
	 * @param config configuracion que llega del front
	 * @return ConfigRq configuracion a la que se le setea lo que recuperamos de la BD
	 */
	@Override
	public ConfigRq extraerConfigTdcVirtual(ConfigRq config) {
		LOG.info("inicia.extraerConfigTdcVirtual");
		//Inicializamos los objetos
		List<Virtual> virtuales = new ArrayList<>();
		Virtual virtual = null;
		ConfigRq configuraciones = new ConfigRq();
		configuraciones = config;
		config = null;
		LOG.info("QUERY_TDC_VIRTUAL_COD>>");
		//Recorremos el resultado de BD
//	    for(Map<String,Object> out :paramOut) {
//	    	//Mapeamos los objetos
//	    	virtual = new Virtual();
//	    	virtual.setIdeTpoTdc(MappingUtils.getMValue(out,IDE_TPO_TDC, ConstastCommon.EMPTY_STRING));
//	    	virtual.setCodMarca(MappingUtils.getMValue(out,COD_MARCA, ConstastCommon.EMPTY_STRING));
//	    	virtual.setDscTpoTdc(MappingUtils.getMValue(out,DSC_TPO_TDC, ConstastCommon.EMPTY_STRING));
//	    	virtuales.add(virtual);
//	    	virtual = null;
//	    }
//	    paramOut = null;
	    //setamos la lista recuperada
	    configuraciones.getConfigLmwe().setVirtuales(virtuales);
	    virtuales = null;
	    LOG.info("termina.extraerConfigTdcVirtual");
	    //Regresamos los datos
	    return configuraciones;
	}
}
