package mx.isban.neo.cc.tenencias.model;

import java.io.Serializable;

public class SimpleDataLmwe implements Serializable{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DatosLmwe datosLMWE;
    
    public SimpleDataLmwe() {
        this.datosLMWE = new DatosLmwe();
    }
    
    public DatosLmwe getDatosLMWE() {
        return this.datosLMWE;
    }
    
    public void setDatosLMWE(final DatosLmwe datosLMWE) {
        this.datosLMWE = datosLMWE;
    }

}
