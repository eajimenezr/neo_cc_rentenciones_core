package mx.isban.neo.cc.tenencias.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LimiteResp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("id_mensaje")
	private int IDMENSAJE;
	
	@JsonProperty("mensaje")
	private String MENSAJE;
	
	@JsonProperty("num_pan")
	private String NUM_PAN;
	
	@JsonProperty("imp_limite_actual")
	private String IMP_LIMITE_ACTUAL;
	
	@JsonProperty("imp_limite_ofertado")
	private String IMP_LIMITE_OFERTADO;
	
	@JsonProperty("fec_vigencia")
	private String FEC_VIGENCIA;
	
	@JsonProperty("imp_limite_actual_p")
	private String IMP_LIMITE_ACTUAL_P;
	
	@JsonProperty("imp_limite_ofertado_p")
	private String IMP_LIMITE_OFERTADO_P;

	@JsonProperty("fechaShot")
	private String FECHA_SHOT;
	
	@JsonProperty("num_puntostotales")
	private String NUM_PUNTOSTOTALES;
	
	@JsonProperty("des_programa")
	private String DES_PROGRAMA;
	
	@JsonProperty("num_puntos_expirar_3m")
	private String NUM_PUNTOS_EXPIRAR_3M;
	
	
	@JsonProperty("ind_estatus")
	private String IND_ESTATUS;
	
	@JsonProperty("num_puntosxtdc")
	private String NUM_PUNTOSXTDC;
	
	
	
	public int getIDMENSAJE() {
		return IDMENSAJE;
	}

	public void setIDMENSAJE(int iDMENSAJE) {
		IDMENSAJE = iDMENSAJE;
	}
	
	
	

	

}
