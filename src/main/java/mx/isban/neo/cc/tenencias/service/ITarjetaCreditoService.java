package mx.isban.neo.cc.tenencias.service;

import mx.isban.neo.cc.tenencias.trx.beans.lmwe.config.ConfigRq;

public interface ITarjetaCreditoService {

	/**
	 * Method extraerConfigTdcVirtual
	 * Method objective: ___ .<br>
	 * @param config configuracion que llega del front
	 * @return ConfigRq configuracion a la que se le setea lo que recuperamos de la BD
	 */
	ConfigRq extraerConfigTdcVirtual(ConfigRq config);

	/**
	 * Method extraerTrx
	 * Method objective: ___ .<br>
	 * @param endpoint endpoint del servicio
	 * @param json json que se envia
	 * @return String rresultado consultado
	 */
	String extraerTrx(String endpoint, Object json);

}
