package mx.isban.neo.cc.tenencias.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import mx.isban.neo.cc.tenencias.model.DatosLmweRs;
import mx.isban.neo.cc.tenencias.trx.beans.lmwe.rq.LmweRq;
import mx.isban.neo.cc.tenencias.trx.beans.lmwe.rs.TrxRs;
import mx.isban.neo.cc.utils.MappingUtilTnnc;
import mx.isban.neo.cc.utils.Utils;
import mx.isban.neo.cc.utils.UtilsCommon;

@Service
public class TrxService  implements ITrxService{
	/**
	 * Constante para trazar la aplicacion.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(TrxService.class);
	
	/**
	 * ICoreTenenciasService iCoreTenenciasService : 
	 **/
	@Autowired
	private ITenenciaService iCoreTenenciasService;
	/**
	 * String baseUri : 
	 **/
	@Value("${endPoint.base}")
	private String baseUri;// 
	
	/**
	 * String uriTrxGenerica : 
	 **/
	@Value("${trx.generica}")
	private String uriTrxGenerica;
	
	@Value("${config.posicion.dot.decimal}")
	private Integer posicionDotDecimal;
	
	/**
	 * String endpoitTrxWallet : 
	 **/
	@Value("${end.point.trx.wallet}")
	private String endpoitTrxWallet;

	
	/**
	 * Method consumirTrxLmwe
	 * Method objective: ___ .<br>
	 * @param lmweRq mapa de datos que llena desde el front y que se setan los ids para su busqueda
	 * @param config configuracion requerida para validar el estatus de bloqueo, situacion de la tarjeta y codigos de bloqueo
	 * @return DatosLmweRs lista de tarjetas que cumplen con las validaciones
	 */
	@Override
	public DatosLmweRs consumirTrxLmwe(LmweRq lmweRq) {
		//Inicializamos nuestros objetos
		TrxRs trxRs = new TrxRs();
		DatosLmweRs datosLmweRs = new DatosLmweRs();
		//Seteamos los valores de la url que necesitamos
		StringBuilder endpointEjecutaTrx = new StringBuilder();
		endpointEjecutaTrx.append(baseUri);
		endpointEjecutaTrx.append(uriTrxGenerica);
		String result = iCoreTenenciasService.coreExtraerTrx(endpointEjecutaTrx.toString(), lmweRq.getLmwe());
		//Validamos el resultado
			if(StringUtils.isNotBlank(result)) {
				LOG.info("#consumirTrxLmwe.result#:"+Encode.forJava(result));
				trxRs = (TrxRs) Utils.getSingletonInstance().stringToObjectMapper(result, TrxRs.class);
				//Sacar el cast del resultado
				if(StringUtils.isNotBlank(trxRs.getResultado().getResponseJSON())) {
					 datosLmweRs = MappingUtilTnnc.getSingletonInstance().mappingDataLmweRs(trxRs);
					 //Validamos que contenga algo el array
						 if(!UtilsCommon.isEmptyArray(datosLmweRs.getDatosLMWE())) {
							 //Se manda ejecutar el proceso de tarjeta
//							 config = iCoreTenenciasService.coreExtraerConfigTdcVirtual(config);
							 
//							 datosLmweRs =  procesoValidacionTarjetas(datosLmweRs,config.getConfigLmwe());
							
//							 datosLmweRs =  iCoreTenenciasService.coreExtraccionDeDatosTdc(lmweRq,datosLmweRs);
//							 datosLmweRs =  iCoreTenenciasService.coreExtraccionDeDatosTdcRt( lmweRq, datosLmweRs);
						 }
						 LOG.info("#datosLmweRs#:"+Utils.getSingletonInstance().objectToJsonString(datosLmweRs));
						 //Enviamos a eliminar los datos cuando no se esta autenticado
//						 config.getFilterRq().setEndPoint(endpoitTrxWallet);
//						 datosLmweRs =  iDisplayService.displayDataTrxWallet(datosLmweRs, config.getFilterRq());
				}
				

			}
		
		
		//Regresamos el resultado
		return datosLmweRs;
	}

}
