package mx.isban.neo.cc.tenencias.service;

import mx.isban.neo.cc.tenencias.model.DatosLmweRs;
import mx.isban.neo.cc.tenencias.trx.beans.lmwe.rq.LmweRq;


public interface ITrxService {

	/**
	 * Method consumirTrxLmwe
	 * Method objective: ___ .<br>
	 * @param lmweRq mapa de datos apra acceder a la TRX
	 * @param config configuracion requerida para validar el estatus de bloqueo, situacion de la tarjeta y codigos de bloqueo
	 * @return DatosLmweRs   lista de tarjetas que cumplen con las validaciones
	 */
	DatosLmweRs consumirTrxLmwe(LmweRq lmweRq);


}
