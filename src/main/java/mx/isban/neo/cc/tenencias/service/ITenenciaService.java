package mx.isban.neo.cc.tenencias.service;



import java.sql.SQLException;

import mx.isban.neo.cc.model.beans.DmlBean;
import mx.isban.neo.cc.tenencias.model.DatosLmweRs;
import mx.isban.neo.cc.tenencias.model.LimiteResp;
import mx.isban.neo.cc.tenencias.trx.beans.lmwe.config.ConfigRq;
import mx.isban.neo.cc.tenencias.trx.beans.lmwe.rq.LmweRq;


public interface ITenenciaService {

//	/**
//	 * Method coreGetListTdd
//	 * Method objective: ___ .<br>
//	 * @param cardRq  mapa de entrada con la informacion del cliente para buscar sus tarjetas dependiendo del  TypesCards
//	 * @return  coreGetListTdd  mapa de salida con la informacion recuperada de la BD correspondiente al TypesCards que se haya enviado
//	 */
//	CardRs coreGetListCard(CardRq cardRq);


	/**
	 * Method coreGetCreditCardTrx
	 * Method objective: ___ .<br>
	 * @param resquest mapa de datos apra acceder a la TRX
	 * @param config configuracion requerida para validar el estatus de bloqueo, situacion de la tarjeta y codigos de bloqueo
	 * @return DatosLmweRs   lista de tarjetas que cumplen con las validaciones
	 *
	 */
	DatosLmweRs coreGetCreditCardTrx(LmweRq resquest);


//	/**
//	 * Method coreExtraccionDeDatosTdc
//	 * Method objective: ___ .<br>
//	 * @param lmweRq  mapa de datos que llena desde el front y que se setan los ids para su busqueda
//	 * @param response mapa de datos con el listado de tarjetas
//	 * @return DatosLmweRs mapa de datos con las tarjetas que cumplen la segunda condicion
//	 *
//	 */
//	DatosLmweRs coreExtraccionDeDatosTdc(LmweRq lmweRq, DatosLmweRs response);

//
//	/**
//	 * Method coreExtraccionDeDatosTdcRt
//	 * Method objective: ___ .<br>
//	 * @param lmweRq  mapa de datos que llena desde el front y que se setan los ids para su busqueda
//	 * @param response mapa de datos con el listado de tarjetas
//	 * @return DatosLmweRs lista de tarjetas que cumplen con las validaciones
//	 */
//	DatosLmweRs coreExtraccionDeDatosTdcRt(LmweRq lmweRq, DatosLmweRs response);
//

	/**
	 * Method coreExtraerConfigTdcVirtual
	 * Method objective: ___ .<br>
	 * @param config configuracion que llega del front
	 * @return ConfigRq configuracion a la que se le setea lo que recuperamos de la BD
	 */
	ConfigRq coreExtraerConfigTdcVirtual(ConfigRq config);


	/**
	 * Method coreExtraerTrx
	 * Method objective: ___ .<br>
	 * @param endpoint endpoint del servicio
	 * @param json json que se envia
	 * @return String rresultado consultado
	 */
	String coreExtraerTrx(String endpoint, Object json);
	
	
	public LimiteResp[] coreDml(DmlBean bean) throws SQLException;


	
}
