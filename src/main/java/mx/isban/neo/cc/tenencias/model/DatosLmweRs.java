package mx.isban.neo.cc.tenencias.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;


import java.math.BigDecimal;

public class DatosLmweRs implements Serializable{
	
	private static final long serialVersionUID = -1L;
	
	private BigDecimal autoCalculatedAmount;
    private List<DatosLmwe> datosLMWE;
    
    public DatosLmweRs() {
        this.datosLMWE = new ArrayList<DatosLmwe>();
        this.autoCalculatedAmount = BigDecimal.ZERO;
    }
    
    public BigDecimal getAutoCalculatedAmount() {
        return this.autoCalculatedAmount;
    }
    
    public void setAutoCalculatedAmount(final BigDecimal montoSuma, final int scale) {
        this.autoCalculatedAmount = montoSuma.setScale(scale, 5);
    }
    
    public List<DatosLmwe> getDatosLMWE() {
        return new ArrayList<DatosLmwe>(this.datosLMWE);
    }
    
    public void setDatosLMWE(final List<DatosLmwe> datosLMWE) {
        if (datosLMWE == null) {
            this.datosLMWE = new ArrayList<DatosLmwe>();
        }
        else {
            this.datosLMWE = new ArrayList<DatosLmwe>(datosLMWE);
        }
    }
	
    
}
